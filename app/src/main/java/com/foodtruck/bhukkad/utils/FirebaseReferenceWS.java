package com.foodtruck.bhukkad.utils;


import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by shubham on 13-09-2019
 */

public class FirebaseReferenceWS {


    public static final String REFERENCE_DRIVER_LOCATION = "drivers_location";
    private static final String REFERENCE_DRIVERS_DATA = "drivers_data";
    private static final String REFERENCE_ACCEPT = "accept";
    private static final String REFERENCE_REQUEST = "request";
    private static final String REFERENCE_STATUS = "status";
    private static final String REFERENCE_RIDER_LOCATION = "riders_location";
    private static final String REFERENCE_RIDER_LOCATION_DATA = "riders_location_data";
    private static final String REFERENCE_TRIP_DATA = "trips_data";
    private static final String REFERENCE_PAYMENT_TYPE = "Paymenttype";
    private static final String REFERENCE_LOCATION = "location";
    private static final String REFERENCE_TOTAL_PRICE = "Price";
    private static final String REFERENCE_TOTAL_DISTANCE = "Distance";
    private static final String REFERENCE_DRIVER_RATING = "driver_rating";
    private static final String REFERENCE_TOLL_FEE = "tollfee";
    private static final String REFERENCE_RIDER_DATA ="foodtruck-61ad7-default-rtdb" ;

    private static com.foodtruck.bhukkad.utils.FirebaseReferenceWS i;


    public DatabaseReference getFirebaseDatabaseReference() {
        return FirebaseDatabase.getInstance().getReference();
    }


    public static com.foodtruck.bhukkad.utils.FirebaseReferenceWS getI() {
        if (i != null) {
            return i;
        }
        i = new com.foodtruck.bhukkad.utils.FirebaseReferenceWS();
        return i;
    }

   /* public DatabaseReference getDriverLocationReference(String carCategory) {
        return getFirebaseDatabaseReference().child(REFERENCE_DRIVER_LOCATION).child(carCategory);
    }

    public DatabaseReference getDriverDataReference(String driverId) {
        return getFirebaseDatabaseReference().child(REFERENCE_DRIVERS_DATA).child(driverId);
    }

    public DatabaseReference getUserLocationReference(String driverId) {
        return getFirebaseDatabaseReference().child(REFERENCE_DRIVERS_DATA).child(driverId).child(REFERENCE_REQUEST);
    }

    public DatabaseReference getAcceptChildReference(String driverId) {
        return getFirebaseDatabaseReference().child(REFERENCE_DRIVERS_DATA).child(driverId).child(REFERENCE_ACCEPT);
    }

    public DatabaseReference getTripStatusReference(String driverId) {
        return getFirebaseDatabaseReference().child(REFERENCE_DRIVERS_DATA).child(driverId).child(REFERENCE_ACCEPT).child(REFERENCE_STATUS);
    }

    public DatabaseReference getTotalPriceReference(String tripId) {
        return getFirebaseDatabaseReference().child(REFERENCE_TRIP_DATA).child(tripId).child(REFERENCE_TOTAL_PRICE);
    }

    public DatabaseReference getTotalDistanceReference(String tripId) {
        return getFirebaseDatabaseReference().child(REFERENCE_TRIP_DATA).child(tripId).child(REFERENCE_TOTAL_DISTANCE);
    }

    public DatabaseReference getDriverRatingReference(String tripId) {
        return getFirebaseDatabaseReference().child(REFERENCE_TRIP_DATA).child(tripId).child(REFERENCE_DRIVER_RATING);
    }

    public DatabaseReference getUpdateRatingReference(String tripId) {
        return getFirebaseDatabaseReference().child(REFERENCE_TRIP_DATA).child(tripId);
    }

    public DatabaseReference getTollFeeReference(String driverId) {
        return getFirebaseDatabaseReference().child(REFERENCE_DRIVERS_DATA).child(driverId).child(REFERENCE_ACCEPT).child(REFERENCE_TOLL_FEE);
    }

    public DatabaseReference getPaymentTypeReference(String riderId) {
        return getFirebaseDatabaseReference().child(REFERENCE_RIDER_LOCATION).child(riderId).child(REFERENCE_PAYMENT_TYPE);
    }
    public DatabaseReference getUserLocation(String riderId) {
        return getFirebaseDatabaseReference().child(REFERENCE_RIDER_LOCATION_DATA).child(riderId).child(REFERENCE_LOCATION);
    }
public DatabaseReference trackDriverLocation(String driverId){
        return  getFirebaseDatabaseReference().child(REFERENCE_DRIVER_LOCATION).child(driverId);
}*/


    public DatabaseReference getTripStatusReference(String userId, String isType) {
       // return getFirebaseDatabaseReference().child(REFERENCE_RIDER_DATA);
        return getFirebaseDatabaseReference().child("orders").child(userId).child(isType);

    }
}
