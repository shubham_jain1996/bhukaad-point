package com.foodtruck.bhukkad.utils;

public interface Const {


    String BASE_URL = "http://3.17.125.65/";
    String ITEM_BASE_URL = "https://playandlike.in/uploads/";

    String IS_LOGIN = "is_login";
    String PREF_NAME = "com.playandlike.vilo";
    String GOOGLE_LOGIN = "google";
    String FACEBOOK_LOGIN = "facebook";
    String USER = "user";
    String USER_GOOGLE = "user_GOOGLE";
    String TOKEN = "token";
    String FAV = "fav";
    String TERMS_URL = "https://playandlike.in/tnc.html";
    String PRIVACY_URL = "https://playandlike.in/privacy-policy.html";
    String HELP_URL = "https://playandlike.in/help.html";
}
