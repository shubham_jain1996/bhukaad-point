package com.foodtruck.bhukkad.utils;

import android.app.ActivityManager;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.text.TextUtils;
import android.util.Patterns;

import com.foodtruck.bhukkad.model.order.ItemResopnse;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Pattern;

public  class Validations {


    private Validations() {
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static boolean isEmpty(String value) {
        return TextUtils.isEmpty(value);
    }


    public static boolean isValidMobile(String phone) {
        String pattern = "^(\\+91[\\-\\s]?)?[0]?(91)?[6789]\\d{9}$";
        Pattern sameDigits = Pattern.compile("(\\d)(\\1){3,}");
        return (phone.matches(pattern)) && (!sameDigits.matcher(phone).matches());

    }


    public static boolean isMockSettingsON(Context context) {
        final PackageManager pm = context.getPackageManager();

//////////// List of all installed apps.
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);

//////////// AppOpsManager
        AppOpsManager opsManager = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);

        boolean isMockEnabled = false;

        for (ApplicationInfo packageInfo : packages) {
            //////////// The package name.
            String packageName = packageInfo.packageName;

            //////////// I think this will get the UID
            int UID = packageInfo.uid;

            //////////// Check this app is selected as mock location app
            if (opsManager.checkOp(AppOpsManager.OPSTR_MOCK_LOCATION, UID, packageName) == AppOpsManager.MODE_ALLOWED) {
                isMockEnabled = true;
            }
        }
    return  isMockEnabled;
    }

    public static List<String> getListOfFakeLocationApps(Context context) {
        List<String> runningApps = getRunningApps(context);
        List<String> fakeApps = new ArrayList<>();
        for (String app : runningApps) {
            if(!isSystemPackage(context, app) && hasAppPermission(context, app, "android.permission.ACCESS_MOCK_LOCATION")){
                fakeApps.add(getApplicationName(context, app));
            }
        }
        return fakeApps;
    }



    public static List<String> getRunningApps(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        HashSet<String> runningApps = new HashSet<>();
        try {
            List<ActivityManager.RunningAppProcessInfo> runAppsList = activityManager.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runAppsList) {
                runningApps.addAll(Arrays.asList(processInfo.pkgList));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            //can throw securityException at api<18 (maybe need "android.permission.GET_TASKS")
            List<ActivityManager.RunningTaskInfo> runningTasks = activityManager.getRunningTasks(1000);
            for (ActivityManager.RunningTaskInfo taskInfo : runningTasks) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    runningApps.add(taskInfo.topActivity.getPackageName());
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            List<ActivityManager.RunningServiceInfo> runningServices = activityManager.getRunningServices(1000);
            for (ActivityManager.RunningServiceInfo serviceInfo : runningServices) {
                runningApps.add(serviceInfo.service.getPackageName());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return new ArrayList<>(runningApps);
    }

    public static boolean isSystemPackage(Context context, String app){
        PackageManager packageManager = context.getPackageManager();
        try {
            PackageInfo pkgInfo = packageManager.getPackageInfo(app, 0);
            return (pkgInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean hasAppPermission(Context context, String app, String permission){
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packageInfo;
        try {
            packageInfo = packageManager.getPackageInfo(app, PackageManager.GET_PERMISSIONS);
            if(packageInfo.requestedPermissions!= null){
                for (String requestedPermission : packageInfo.requestedPermissions) {
                    if (requestedPermission.equals(permission)) {
                        return true;
                    }
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String getApplicationName(Context context, String packageName) {
        String appName = packageName;
        PackageManager packageManager = context.getPackageManager();
        try {
            appName = packageManager.getApplicationLabel(packageManager.getApplicationInfo(packageName, PackageManager.GET_META_DATA)).toString();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return appName;
    }

    public static String stringPrice(int price) {

        return  "\u20B9 "+price;


    }

    public static String stringPrice(double price) {

        return  "\u20B9 "+price;


    }

    public static String stringPrice(String format) {
        return  "\u20B9 "+format;
    }
    public static String prettyCount(Number number) {

        try {
            char[] suffix = {' ', 'k', 'M', 'B', 'T', 'P', 'E'};
            long numValue = number.longValue();
            int value = (int) Math.floor(Math.log10(numValue));
            int base = value / 3;
            if (value >= 3 && base < suffix.length) {
                double value2 = numValue / Math.pow(10, base * 3);
                if (String.valueOf(value2).contains(".")) {
                    String num = String.valueOf(value2).split("\\.")[String.valueOf(value2).split("\\.").length - 1];
                    if (num.contains("0")) {
                        return new DecimalFormat("#0").format(value2) + suffix[base];
                    } else {
                        return new DecimalFormat("#0.0").format(value2) + suffix[base];
                    }
                } else {
                    return new DecimalFormat("#0").format(value2) + suffix[base];
                }
            } else {
                return new DecimalFormat("#,##0").format(numValue);
            }
        } catch (Exception e) {
            return String.valueOf(number);
        }
    }

    public static String getItemNames(List<ItemResopnse> items) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i =0; i<items.size();i++){
            if (i<(items.size()-1)){
                stringBuilder = stringBuilder.append(items.get(i).getProductCount()+" x "+items.get(i).getProductName()+", ");
            }else {
                stringBuilder = stringBuilder.append(items.get(i).getProductCount()+" x "+items.get(i).getProductName());
            }
        }
        return stringBuilder.toString();
    }
}
