package com.foodtruck.bhukkad.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.foodtruck.bhukkad.model.register.FirebaseKeys;
import com.foodtruck.bhukkad.model.register.RegisterResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class SessionManager {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    @SuppressLint("CommitPrefEdits")
    public SessionManager(Context context) {
        this.pref = context.getSharedPreferences(Const.PREF_NAME, MODE_PRIVATE);
        this.editor = this.pref.edit();
    }

    public void saveStringValue(String key, String value) {
        editor.putString(key, value);
        editor.apply();
    }

    public void saveTokenValue(String key, String value) {
        editor.putString(key, value);
        editor.apply();
    }

    public String getTokenValue(String key) {
        return pref.getString(key, "");
    }


    public String getStringValue(String key) {
        return pref.getString(key, "");
    }

    public void saveLongValue(String key, Long value) {
        editor.putLong(key, value);
        editor.apply();
    }

    public Long getLongValue(String key) {
        return pref.getLong(key, 0);
    }

    public void saveBooleanValue(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.apply();
    }

    public boolean getBooleanValue(String key) {
        return pref.getBoolean(key, false);
    }

    public void saveUser(RegisterResponse user) {
        editor.putString(Const.USER, new Gson().toJson(user));
        editor.apply();
    }

    public RegisterResponse getUser() {
        String userString = pref.getString(Const.USER, "");
        if (!userString.isEmpty()) {
            return new Gson().fromJson(userString, RegisterResponse.class);
        }
        return null;
    }

    public void saveGoogleUser(FirebaseKeys user) {
        editor.putString(Const.USER_GOOGLE, new Gson().toJson(user));
        editor.apply();
    }

    public FirebaseKeys getGoogleUser() {
        String userString = pref.getString(Const.USER_GOOGLE, "");
        if (!userString.isEmpty()) {
            return new Gson().fromJson(userString, FirebaseKeys.class);
        }
        return null;
    }

    public void saveFavouriteMusic(String id) {
        List<String> fav = getFavouriteMusic();
        if (fav != null) {
            if (fav.contains(id)) {
                fav.remove(id);

            } else {
                fav.add(id);

            }
        } else {
            fav = new ArrayList<>();
            fav.add(id);

        }
        editor.putString(Const.FAV, new Gson().toJson(fav));
        editor.apply();
    }

    public List<String> getFavouriteMusic() {
        String userString = pref.getString(Const.FAV, "");
        if (userString != null && !userString.isEmpty()) {
            return new Gson().fromJson(userString, new TypeToken<ArrayList<String>>() {
            }.getType());
        }
        return new ArrayList<>();
    }

    public void clear() {
        editor.clear().apply();
        saveBooleanValue(Const.IS_LOGIN, false);
        //Global.accessToken = "";
       // Global.userId = "";
    }

    public void saveProfileImage(String key, String value) {
        editor.putString(key, value);
        editor.apply();
    }

    public String getProfileImage(String key) {
        return pref.getString(key, "");
    }



}
