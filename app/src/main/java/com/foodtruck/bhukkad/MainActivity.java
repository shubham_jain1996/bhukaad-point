 package com.foodtruck.bhukkad;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.foodtruck.bhukkad.utils.SessionManager;
import com.foodtruck.bhukkad.view.HomeActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.ContentValues.TAG;

 public class MainActivity extends AppCompatActivity {
     private static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 5;
     private SessionManager session;
     public  static double latitude=0.0;
     public  static double longitude=0.0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        generateFirebaseToken();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkMultiplePermissions();
        } else {
            Handler handler = new Handler();
            handler.postDelayed(() -> splash(), 1000);
        }

    }

     private void generateFirebaseToken() {
         FirebaseMessaging.getInstance().getToken().addOnSuccessListener(token -> {
             if (!TextUtils.isEmpty(token)) {
                 saveTokenInSession(token);
             } else{
                 Log.w(TAG, "token should not be null...");
             }
         }).addOnFailureListener(e -> {
             //handle e
         }).addOnCanceledListener(() -> {
             //handle cancel
         }).addOnCompleteListener(task -> Log.v(TAG, "This is the token : "));
        /* FirebaseInstanceId.getInstance().getInstanceId()
                 .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                     @Override
                     public void onComplete(@NonNull Task<InstanceIdResult> task) {
                         if (!task.isSuccessful()) {
                             Log.w("", "getInstanceId failed", task.getException());
                             return;
                         }
                         String token = task.getResult().getToken();


                     }
                 });*/

     }


     private void saveTokenInSession(String s) {
         session= new SessionManager(this);
         session.saveTokenValue("Token",s);
     }

     private void splash() {
         startActivity(new Intent(MainActivity.this, HomeActivity.class));
         finish();

     }

     private void checkMultiplePermissions() {
         if (Build.VERSION.SDK_INT >= 23) {
             List<String> permissionsNeeded = new ArrayList<>();
             List<String> permissionsList = new ArrayList<>();


             if(!addPermission(permissionsList, Manifest.permission.CAMERA))
             {
                 permissionsNeeded.add("Camera");
             }
             if(!addPermission(permissionsList, Manifest.permission.ACCESS_NETWORK_STATE))
             {
                 permissionsNeeded.add("Access Network State");
             }

             if(!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
             {
                 permissionsNeeded.add("Access Coarse location");
             }

             if(!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
             {
                 permissionsNeeded.add("Access Fine location");
             }


             if(!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE))
             {
                 permissionsNeeded.add("Read calander");
             }
             if(!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
             {
                 permissionsNeeded.add("Read calander");
             }

             if (permissionsList.size() > 0)
             {
                 requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                         REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
             }
             else{
                 Handler handler = new Handler();
                 handler.postDelayed(() -> splash(), 3000);
             }
         }
     }
     private boolean addPermission(List<String> permissionsList, String permission) {
         if (Build.VERSION.SDK_INT >= 23)

             if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                 permissionsList.add(permission);

                 // Check for Rationale Option
                 if (!shouldShowRequestPermissionRationale(permission))
                     return false;
             }
         return true;
     }
     @Override
     public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
         switch (requestCode) {
             case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                 Map<String, Integer> perms = new HashMap<String, Integer>();
                 perms.put(Manifest.permission.ACCESS_NETWORK_STATE,PackageManager.PERMISSION_GRANTED);
                 perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE,PackageManager.PERMISSION_GRANTED);
                 perms.put(Manifest.permission.READ_EXTERNAL_STORAGE,PackageManager.PERMISSION_GRANTED);
                 perms.put(Manifest.permission.ACCESS_FINE_LOCATION,PackageManager.PERMISSION_GRANTED);
                 perms.put(Manifest.permission.ACCESS_COARSE_LOCATION,PackageManager.PERMISSION_GRANTED);
                 perms.put(Manifest.permission.CAMERA,PackageManager.PERMISSION_GRANTED);
                 // Fill with results
                 for (int i = 0; i < permissions.length; i++)
                     perms.put(permissions[i], grantResults[i]);
                 if ( perms.get(Manifest.permission.ACCESS_NETWORK_STATE)==PackageManager.PERMISSION_GRANTED
                         && perms.get(Manifest.permission.CAMERA)==PackageManager.PERMISSION_GRANTED
                         && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED
                         && perms.get(Manifest.permission.ACCESS_FINE_LOCATION)==PackageManager.PERMISSION_GRANTED
                         && perms.get(Manifest.permission.ACCESS_COARSE_LOCATION)==PackageManager.PERMISSION_GRANTED
                         && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED
                 ) {
                     // All Permissions Granted
                     splash();
                     return;
                 } else {
                     // Permission Denied
                     if (Build.VERSION.SDK_INT >= 23) {
                         Toast.makeText(getApplicationContext(), "Please permit all the permissions", Toast.LENGTH_LONG).show();
                         finish();
                     }
                 }
             }
             break;
             default:
                 super.onRequestPermissionsResult(requestCode, permissions, grantResults);
         }

     }
}