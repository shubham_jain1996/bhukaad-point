package com.foodtruck.bhukkad.helper;

import android.app.Activity;
import android.os.AsyncTask;

import com.foodtruck.bhukkad.view.address.AddAddres;
import com.google.android.gms.maps.model.LatLng;

public class SetDataInBackground extends AsyncTask<LatLng, String, String> {
    private DataLoadingInterfaceHome callBack;
    Activity ctx;
    public SetDataInBackground(DataLoadingInterfaceHome dataLoading, AddAddres mainActivity){
        this.callBack = dataLoading;
        this.ctx=mainActivity;
    }

    @Override
    protected String doInBackground(LatLng... LatLng)
    {
       com.google.android.gms.maps.model.LatLng coordinate= LatLng[0];
       String address= Utility.getCompleteAddressString(coordinate.latitude,coordinate.longitude,ctx);
        return address;
    }

    @Override
    protected void onPostExecute(String searchInventoryList) {

        callBack.postExecute(searchInventoryList);
    }


}
