package com.foodtruck.bhukkad.helper;

public interface DataLoadingInterfaceHome {
    void preExecute();
    void postExecute(String homeClasses);
}
