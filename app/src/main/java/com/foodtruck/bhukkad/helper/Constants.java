package com.foodtruck.bhukkad.helper;

public class Constants {
    public static final String success  = "Success";
    public static final String fail  = "Fail";
    public static final String PREF_NAME = "wegocity-user";
    public static final int ORIGIN_REQUEST_CODE_AUTOCOMPLETE =1 ;
    public static final int DEST_REQUEST_CODE_AUTOCOMPLETE =2 ;
    public static final int LOCATION_PICKER_PICKUP =1 ;
    public static final int LOCATION_PICKER_DESTINATION =2 ;
    public static final String FinalPickupLatLong = "finalPickupLatLong";
    public static final String FinalDestinationLatLong = "finalDestinationLatLong";
    public static final String TYPE_PICKUP_LOCATION = "pickuplocation";
    public static final String TYPE_DESTINATION_LOCATION = "destinationlocation";
    public static final String MY_API_KEY="AIzaSyDmjaUBuvFaxxjtVP9VaYJz_jq_vmD7Qik";
    public static final int REQUEST_CODE_CALLING_PERMISSION =5 ;
    public static final String RIDE_STATUS ="ride_status" ;
    public static final String RIDE_STATUS_CANCEL ="5" ;
    public static final String RIDE_STATUS_DRIVER_ARRIVING ="2" ;
    public static final String RIDE_STATUS_ON_TRIP ="3" ;
    public static final String RIDE_STATUS_END ="4" ;
    public static final String PAYMENT_TYPE_CARD ="stripe" ;
    public static final String PAYMENT_TYPE_CASH ="cash" ;
    public static final String FARE_TYPE_FIXED = "fixed";
    public static final String FARE_TYPE_CUSTOM = "custom";
    public static final String CATEGORY_LIVE_URL ="http://188.166.151.131/" ;
    public static final String PARAM_RIDER_ID ="ride_id" ;
    public static final String DRIVER_COMING_AT_LOCATION ="6" ;
    public static final String DRIVER_DATA ="drivers_data" ;
    public static final String ACCEPT ="accept" ;
    public static final String ENDTRIP_RIDER ="7" ;
    public static final String RIDE_REFERENCE_NODE ="ride_id" ;
    public static final String RIDE_PICK_ADDRESS ="pick_address" ;
    public static final String RIDE_DROP_ADDRESS ="drop_address" ;
    public static final String RIDE_PICK_LATITUED ="pick_latitude" ;
    public static final String RIDE_PICK_LONGITUDE ="pick_longitude" ;
    public static final String RIDE_DROP_LATITUDE ="drop_latitude" ;
    public static final String RIDE_STATUS_REQUEST_ACCEPTED ="Accepted" ;
    public static final String TRIP_ID ="tripId" ;
    public static final String CAR_TYPE ="car_type" ;
    public static final int LANGUAGE =9 ;
    public static final String LATER ="later" ;
    public static final String NOW ="now" ;
    public static final String PROFILEIMAGE ="profile_image" ;
    public static String rs=" Rs";
    public static String RIDE_dROP_LONGITUDE="drop_longitude";

    public static class Pref {
        public static final String userid = "driverid";


        public static String requestid="requestid";
        public static String requeststatus="requeststatus";
        public static String userkey="cardriverid";
        public static String tripId="tripId";
        public static String isSubscribed="subscribed";
        public static final String token="token";
        public static String email="email";
        public static String username="userName";
        public static String phoneNum="PhoneNo";
        public static String address="address";
        public static String password="password";
        public static String reference="refenceNo";
        public static String profileImage="profilePic";
        public static String driverid="drid";
        public static String pick_lat="pick_lat";
        public static String pick_lng="pick_lng";
        public static String drop_lat="drop_lat";
        public static String drop_lng="drop_lng";
        public static String pick_address="pick_address";
        public static String drop_address="drop_address";
        public static String isRideContinue="isRideContinue";
        public static String rideId="rideId";
        public static String Car_type="car_type";
        public static String driverName="driver_name";
        public static String amount="amount";
        public static String firebaseToken="firebaseToken";
        public static String language="language";
        //public static String driverid="driverid";
    }
}
