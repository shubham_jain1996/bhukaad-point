package com.foodtruck.bhukkad.customdailog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.ImageView;

import com.foodtruck.bhukkad.R;



public class LbmDialog extends Dialog  {
    private static final String DEFAULT_PATTERN = "%d%%";
    Context c;

    public LbmDialog(Context context) {
        super(context);
        this.c = context;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.lbm_dialog);
        // hide the progress
        //CircleProgressBar.setProgressFormatter(null);
    }


}