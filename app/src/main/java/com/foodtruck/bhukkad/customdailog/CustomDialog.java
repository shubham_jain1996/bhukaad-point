package com.foodtruck.bhukkad.customdailog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.adapter.ViewCouponAdapter;
import com.foodtruck.bhukkad.model.coupon.CouponResponse;
import com.foodtruck.bhukkad.model.coupon.Offer;
import com.foodtruck.bhukkad.network.DataService;
import com.foodtruck.bhukkad.utils.SessionManager;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.firebase.database.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.ButterKnife;


public class CustomDialog extends BottomSheetDialogFragment {


    private RecyclerView rvListCoupon;
    private ViewCouponAdapter viewCouponAdapter;
    SessionManager sessionManager;
    private List<Offer> couponData=new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root= inflater.inflate(R.layout.fragment_custom_dialog, container, false);
        sessionManager = new SessionManager(getActivity());
        rvListCoupon  = root.findViewById(R.id.rv_list_coupon);
        initRV();
        getDataForCoupon();
        return root;
    }
    private void getDataForCoupon() {
        DataService dataService = new DataService(getActivity());
        dataService.setOnServerListener(new DataService.ServerResponseListner<CouponResponse>() {
            @Override
            public void onDataSuccess(CouponResponse response, int code) {
                if (response!=null && response.getData()!=null && response.getData().getOffers().size()>0){
                    couponData.addAll(response.getData().getOffers());
                    viewCouponAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.callApiToGetCoupon("Token "+sessionManager.getUser().getData().getAuthToken());

    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(@NotNull final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.bottomsheetdialog_layout, null);
        ButterKnife.bind(this, contentView);
        dialog.setContentView(contentView);

        ((View) contentView.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);

    }


    private void initRV() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvListCoupon.setLayoutManager(linearLayoutManager);
       // viewCouponAdapter = new ViewCouponAdapter(getActivity(),couponData,this);
      //  rvListCoupon.setAdapter(viewCartAdapter);
    }
}