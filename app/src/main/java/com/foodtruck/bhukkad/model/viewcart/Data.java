package com.foodtruck.bhukkad.model.viewcart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {
    @SerializedName("cart_detail")
    @Expose
    private List<CartDetail> cartDetail = null;
    @SerializedName("costing")
    @Expose
    private Costing costing;

    public List<CartDetail> getCartDetail() {
        return cartDetail;
    }

    public void setCartDetail(List<CartDetail> cartDetail) {
        this.cartDetail = cartDetail;
    }

    public Costing getCosting() {
        return costing;
    }

    public void setCosting(Costing costing) {
        this.costing = costing;
    }
}
