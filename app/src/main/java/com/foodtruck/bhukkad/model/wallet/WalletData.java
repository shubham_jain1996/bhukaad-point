package com.foodtruck.bhukkad.model.wallet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WalletData {
    @SerializedName("balance")
    @Expose
    private Double balance;
    @SerializedName("pending_referrals")
    @Expose
    private List<String> pendingReferrals = null;

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public List<String> getPendingReferrals() {
        return pendingReferrals;
    }

    public void setPendingReferrals(List<String> pendingReferrals) {
        this.pendingReferrals = pendingReferrals;
    }
}
