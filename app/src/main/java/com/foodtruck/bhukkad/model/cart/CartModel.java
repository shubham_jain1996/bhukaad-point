package com.foodtruck.bhukkad.model.cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartModel {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status_code")
    @Expose
    private Integer statusCode;
    @SerializedName("data")
    @Expose
    private RemoveCartItemModel.ItemAdded.Data data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public RemoveCartItemModel.ItemAdded.Data getData() {
        return data;
    }

    public void setData(RemoveCartItemModel.ItemAdded.Data data) {
        this.data = data;
    }
    public class ItemAdded {

        @SerializedName("product")
        @Expose
        private Integer product;
        @SerializedName("price")
        @Expose
        private String price;
        @SerializedName("discount")
        @Expose
        private Double discount;
        @SerializedName("quantity")
        @Expose
        private String quantity;
        @SerializedName("active")
        @Expose
        private Boolean active;

        public Integer getProduct() {
            return product;
        }

        public void setProduct(Integer product) {
            this.product = product;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public Double getDiscount() {
            return discount;
        }

        public void setDiscount(Double discount) {
            this.discount = discount;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public Boolean getActive() {
            return active;
        }

        public void setActive(Boolean active) {
            this.active = active;
        }

        public class Data {

            @SerializedName("item_added")
            @Expose
            private RemoveCartItemModel.ItemAdded itemAdded;
            @SerializedName("cart_count")
            @Expose
            private Integer cartCount;

            public RemoveCartItemModel.ItemAdded getItemAdded() {
                return itemAdded;
            }

            public void setItemAdded(RemoveCartItemModel.ItemAdded itemAdded) {
                this.itemAdded = itemAdded;
            }

            public Integer getCartCount() {
                return cartCount;
            }

            public void setCartCount(Integer cartCount) {
                this.cartCount = cartCount;
            }

        }
    }
}
