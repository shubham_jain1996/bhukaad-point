package com.foodtruck.bhukkad.model.delivery;

public class MessageResponse {
    String message;
    boolean iselect;

    public MessageResponse(String message, boolean iselect) {
        this.message = message;
        this.iselect = iselect;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isIselect() {
        return iselect;
    }

    public void setIselect(boolean iselect) {
        this.iselect = iselect;
    }
}
