package com.foodtruck.bhukkad.model.register;

public class FirebaseKeys {
    String name;
    String email;
    String token;


    public FirebaseKeys(String name, String email, String token) {
        this.name = name;
        this.email = email;
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
