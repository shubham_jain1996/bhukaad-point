package com.foodtruck.bhukkad.model.discount;

import com.foodtruck.bhukkad.model.viewcart.Costing;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataDiscount {
    @SerializedName("costing")
    @Expose
    private CostingDiscount costing;

    public CostingDiscount getCosting() {
        return costing;
    }

    public void setCosting(CostingDiscount costing) {
        this.costing = costing;
    }
}
