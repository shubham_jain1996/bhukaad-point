package com.foodtruck.bhukkad.model.discount;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CostingDiscount {
    @SerializedName("tax")
    @Expose
    private Integer tax;
    @SerializedName("shipping")
    @Expose
    private Double shipping;
    @SerializedName("coupon_discount")
    @Expose
    private Integer couponDiscount;
    @SerializedName("item_discount")
    @Expose
    private Double itemDiscount;
    @SerializedName("coupon_status")
    @Expose
    private Boolean couponStatus;
    @SerializedName("sub_total")
    @Expose
    private Double subTotal;
    @SerializedName("our_price")
    @Expose
    private Double ourPrice;
    @SerializedName("distance")
    @Expose
    private Double distance;
    @SerializedName("referral_limit")
    @Expose
    private Double referralLimit;
    @SerializedName("total")
    @Expose
    private Double total;

    public Integer getTax() {
        return tax;
    }

    public void setTax(Integer tax) {
        this.tax = tax;
    }

    public Double getShipping() {
        return shipping;
    }

    public void setShipping(Double shipping) {
        this.shipping = shipping;
    }

    public Integer getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(Integer couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public Double getItemDiscount() {
        return itemDiscount;
    }

    public void setItemDiscount(Double itemDiscount) {
        this.itemDiscount = itemDiscount;
    }

    public Boolean getCouponStatus() {
        return couponStatus;
    }

    public void setCouponStatus(Boolean couponStatus) {
        this.couponStatus = couponStatus;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public Double getOurPrice() {
        return ourPrice;
    }

    public void setOurPrice(Double ourPrice) {
        this.ourPrice = ourPrice;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Double getReferralLimit() {
        return referralLimit;
    }

    public void setReferralLimit(Double referralLimit) {
        this.referralLimit = referralLimit;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
}
