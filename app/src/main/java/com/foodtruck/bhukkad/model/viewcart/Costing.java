package com.foodtruck.bhukkad.model.viewcart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Costing {
    @SerializedName("tax")
    @Expose
    private Integer tax;
    @SerializedName("shipping")
    @Expose
    private Double shipping;
    @SerializedName("sub_total")
    @Expose
    private Integer subTotal;
    @SerializedName("item_discount")
    @Expose
    private Integer itemDiscount;
    @SerializedName("distance")
    @Expose
    private Double distance;
    @SerializedName("total")
    @Expose
    private Double total;

    public Integer getTax() {
        return tax;
    }

    public void setTax(Integer tax) {
        this.tax = tax;
    }

    public Double getShipping() {
        return shipping;
    }

    public void setShipping(Double shipping) {
        this.shipping = shipping;
    }

    public Integer getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Integer subTotal) {
        this.subTotal = subTotal;
    }

    public Integer getItemDiscount() {
        return itemDiscount;
    }

    public void setItemDiscount(Integer itemDiscount) {
        this.itemDiscount = itemDiscount;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

}
