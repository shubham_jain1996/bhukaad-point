package com.foodtruck.bhukkad.model.delivery;

public class DeliveryResponse {
    String message;
    String status;
    String time;

    public DeliveryResponse() {
    }

    public DeliveryResponse(String message, String status, String time) {
        this.message = message;
        this.status = status;
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
