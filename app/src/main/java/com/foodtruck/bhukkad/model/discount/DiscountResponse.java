package com.foodtruck.bhukkad.model.discount;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DiscountResponse {
    @SerializedName("message")
    @Expose
    private Object message;
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status_code")
    @Expose
    private Integer statusCode;
    @SerializedName("data")
    @Expose
    private DataDiscount data;

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public DataDiscount getData() {
        return data;
    }

    public void setData(DataDiscount data) {
        this.data = data;
    }

}
