package com.foodtruck.bhukkad.model.addres;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataAddress {
    @SerializedName("address")
    @Expose
    private List<AddressData> address = null;

    public List<AddressData> getAddress() {
        return address;
    }

    public void setAddress(List<AddressData> address) {
        this.address = address;
    }
}
