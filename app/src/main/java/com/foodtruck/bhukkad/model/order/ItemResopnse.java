package com.foodtruck.bhukkad.model.order;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemResopnse implements Parcelable {
    @SerializedName("product__name")
    @Expose
    private String productName;
    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("product_count")
    @Expose
    private Integer productCount;

    protected ItemResopnse(Parcel in) {
        productName = in.readString();
        if (in.readByte() == 0) {
            orderId = null;
        } else {
            orderId = in.readInt();
        }
        if (in.readByte() == 0) {
            productCount = null;
        } else {
            productCount = in.readInt();
        }
    }

    public static final Creator<ItemResopnse> CREATOR = new Creator<ItemResopnse>() {
        @Override
        public ItemResopnse createFromParcel(Parcel in) {
            return new ItemResopnse(in);
        }

        @Override
        public ItemResopnse[] newArray(int size) {
            return new ItemResopnse[size];
        }
    };

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getProductCount() {
        return productCount;
    }

    public void setProductCount(Integer productCount) {
        this.productCount = productCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(productName);
        if (orderId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(orderId);
        }
        if (productCount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(productCount);
        }
    }
}
