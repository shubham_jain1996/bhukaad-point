package com.foodtruck.bhukkad.network;

import android.provider.ContactsContract;
import android.view.Menu;

import com.foodtruck.bhukkad.model.addres.AddressResponse;
import com.foodtruck.bhukkad.model.cart.CartModel;
import com.foodtruck.bhukkad.model.cart.RemoveCartItemModel;
import com.foodtruck.bhukkad.model.category.CategoryResponse;
import com.foodtruck.bhukkad.model.coupon.CouponResponse;
import com.foodtruck.bhukkad.model.discount.DiscountResponse;
import com.foodtruck.bhukkad.model.homeslider.SliderImagesResponse;
import com.foodtruck.bhukkad.model.menu.MenuResponse;
import com.foodtruck.bhukkad.model.order.MyOrderResponse;
import com.foodtruck.bhukkad.model.profile.ProfileResponse;
import com.foodtruck.bhukkad.model.register.RegisterResponse;
import com.foodtruck.bhukkad.model.review.ReviewResponse;
import com.foodtruck.bhukkad.model.viewcart.CartViewResponse;
import com.foodtruck.bhukkad.model.wallet.WalletResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ApiInterface {



   /* @POST("Users/GuestLogin")
    Call<LoginServerResponseWithMail> callApiToLogin(@Body LoginModel type);


    @POST("Items/HomePageImages")
    Call<HomeMainModel> callApiTogetHomeData(@Header("Authorization") String token);



    @POST("Items/GetGroupDataByItemTypeID")
    Call<GiftCardMainModel> callApiToGetGiftData(@Header("Authorization") String token, @Body GiftCardSeverInput type);


    @POST("Items/GetByTypeID")
    Call<BullionMainModel> callApiToBullionData(@Header("Authorization") String token, @Body GiftCardSeverInput type);


    @POST("Items/GetGroupDataByGroupID")
    Call<SubGiftCardModel> callApiToSubCardData(@Header("Authorization") String token, @Body SubGiftCardSeverInput type);




    @POST("Items/GetByItemID")
    Call<ImageDetailModel> callApiToGetImages(@Header("Authorization") String token, @Body DetailsSeverInput type);


    @POST("Favourite/Insert")
    Call<MainServerResponse> callApiToInsertFav(@Header("Authorization") String token, @Body InsertFavServer type);


    @POST("Favourite/GetByUserID")
    Call<FavModel> callApiToGetFavData(@Header("Authorization") String token, @Body FetchFavServer type);


    @POST("Favourite/Delete")
    Call<MainServerResponse> callApiTodeleteData(@Header("Authorization") String token, @Body InsertFavServer type);


    @POST("Wallet/GetByUserID")
    Call<WalletServerModel> callApiTogetWalletData(@Header("Authorization") String token, @Body FetchFavServer type);


    @POST("CartMaster/AddToCart")
    Call<MainServerResponse> callApiToAddItemInCart(@Header("Authorization") String token, @Body AddToCartModel type);



    @POST("Complaint/Insert")
    Call<MainServerResponse> callAPiToInsertQuery(@Header("Authorization") String token, @Body SupportInsert type);


    @POST("Complaint/GetComplaintCategory")
    Call<SupportModel> callApiTogetissueList(@Header("Authorization") String token);


    @POST("CustomizedCardMaster/GetByUserID")
    Call<CustomCardModel> callApiToGetCustomCard(@Header("Authorization") String token, @Body FetchFavServer type);


    @POST("Profile/AddAddress")
    Call<MainServerResponse> callApiToAddAddress(@Header("Authorization") String token, @Body AddAddressResponse type);


    @POST("Users/ChangePhoneNumber")
    Call<MainServerResponse> callApiToChangePhoneNumber(@Header("Authorization") String token, @Body ChangePhoneServer type);




    @POST("CartMaster/GetByUserID")
    Call<CartFetchModel> callAPiToFetchCartData(@Header("Authorization") String token, @Body FetchFavServer type);



    @POST("Profile/GetAddress")
    Call<GetAddressModel> callApiToGetAddress(@Header("Authorization") String token, @Body FetchFavServer type);



    @POST("CartMaster/Delete")
    Call<MainServerResponse> callApiToDeleteCart(@Header("Authorization") String token, @Body InsertFavServer type);



    @POST("Country/Get")
    Call<CountryModel> callApiToGetCountryData(@Header("Authorization") String token);



    @Multipart
    @POST("CustomizedCardMaster/Insert")
    Call<MainServerResponse> uploadCustomImageOnServer(@Header("Authorization") String token, @Part MultipartBody.Part back, @Part MultipartBody.Part front, @Part("UserId") RequestBody text);

    @POST("accountapi/login")
    Call<LoginServerResponseWithMail> callApiToLoginWithEmail(@Body LoginWithEmail type);

    @POST("accountapi/register")
    Call<NewRegisterModel> callApiToRegisterUser(@Body RegisterUserServer type);

    @POST("Profile/DeleteAddress")
    Call<MainServerResponse> callApiToCreateOrder(@Header("Authorization") String token, @Body DeleteAddress type);


    @POST("Profile/UpdateAddress")
    Call<MainServerResponse> callApiToEditAddress(@Header("Authorization") String token, @Body EditAddressServer type);

    @POST("CustomizedCardMaster/DeleteByCustomizedCardID")
    Call<MainServerResponse> callAPiToDeleteCard(@Header("Authorization") String token, @Body DeleteCustomCard type);

    @POST("Users/GetDeliveryCharge")
    Call<DeliveryChargeModel> callApiToGetDeliveryCharge(@Header("Authorization") String token, @Body DeliveryData type);

    @POST("accountapi/changepassword")
    Call<NewRegisterModel> callApiToCHangePassword(@Body ChangePassword type);

    @POST("accountapi/ForgotPassword")
    Call<NewRegisterModel> callApiToForgetPassword(@Body ForgotPasswordServer type);

    @POST("OrderMaster/BuyNow")
    Call<BuyNowResponseModel> callApiToGetOrderReferenceNo(@Body BuyNowServerModel type);

    @POST("OrderMaster/GetOrderByUser")
    Call<OrderModel> callApiToGetMyOrderList(@Body MyOrderResponse type);

    @POST("OrderMaster/UpdateOrderByReference")
    Call<MainServerResponse> callApiToChangePaymentStatus(@Body ChangePaymentStatusRsponse type);


    @POST("WalletPosting/SelectByUser")
    Call<WalletDetailModel> callApiToGetWalletDetails(@Body FetchFavServer type);

   *//* @POST("app/face_recog.php")
    Call<ServerResponse> callApiToSendFeedBack(@Body UploadImagesForFeedback type);*//*
*/

    @GET("menu/category/")
    Call<CategoryResponse> getCategory();

    @GET("menu/review/?")
    Call<ReviewResponse> getReating(@Header("Authorization") String token, @Query("product") String id );


    @GET("user/detail/")
    Call<ProfileResponse> getProfileData(@Header("Authorization") String token);


    @GET("menu/available/?")
    Call<MenuResponse> getMenuData(@Header("Authorization") String token, @Query("category") String id ,@Query("lat") String lat, @Query("lng") String lng, @Query("radius") String radius);


    @FormUrlEncoded
    @POST("menu/apply-coupon/")
    Call<DiscountResponse> callApiToGetCouponStatus( @Header("Authorization") String authToken,@Field("coupon") String token1, @Field("lat") String lat, @Field("lng") String lng);


    @FormUrlEncoded
    @POST("user/address/")
    Call<WalletResponse> setAddress( @Header("Authorization") String authToken,@Field("name") String name,@Field("mobile") String mobile
            ,@Field("line1") String line1,@Field("line2") String line2,@Field("postal_code") String postal_code, @Field("lat") String lat, @Field("lng") String lng);

    @FormUrlEncoded
    @PUT("user/address/{id}/")
    Call<WalletResponse> EditAddress( @Header("Authorization") String authToken,@Field("name") String name,@Field("mobile") String mobile
            ,@Field("line1") String line1,@Field("line2") String line2,@Field("postal_code") String postal_code, @Field("lat") String lat, @Field("lng") String lng
            ,@Path("id") String id,@Field("status") String status);



    @GET("menu/images")
    Call<SliderImagesResponse> getSliderImages();


    @GET("menu/user-order/")
    Call<MyOrderResponse> callApiToGetData(@Header("Authorization") String token);

    @GET("user/wallet/")
    Call<WalletResponse> callApiToGetWalletDetails(@Header("Authorization") String token);

    @GET("menu/coupon-list/")
    Call<CouponResponse> callApiToGetCoupon(@Header("Authorization") String token);
    @GET("menu/cart-detail/?")
    Call<CartViewResponse> getCartData(@Header("Authorization") String token, @Query("lat") String lat, @Query("lng") String lng);

    @FormUrlEncoded
    @POST("auth/login/")
    Call<RegisterResponse> callApiToLogin(@Field("username") String token1, @Field("password") String token
            , @Field("firebase_id") String fbtoken);




    @Multipart
    @PATCH("user/detail/")
    Call<ProfileResponse> updateProfile(@Header("Authorization") String token1, @PartMap Map<String, RequestBody> map);

    @FormUrlEncoded
    @POST("auth/google-login/")
    Call<RegisterResponse> callApiToGoogleLogin(@Field("email") String token1, @Field("display_name") String token
            , @Field("google_token") String fbtoken ,  @Field("firebase_id") String fbToken1);
    @FormUrlEncoded
    @POST("auth/register/")
    Call<RegisterResponse> registerUser(@Field("username") String token1, @Field("email") String apiKey, @Field("password") String token,
                                          @Field("password2") String coinAmount,
                                          @Field("mobile_number") String mobile, @Field("gender") String gender,
                                          @Field("display_name") String account,  @Field("isVerify") String isVerify
            ,  @Field("referral_code") String referral_code ,  @Field("firebase_id") String fbToken , @Field("google_token") String fbtoken);

    @FormUrlEncoded
    @POST("menu/add-to-cart/")
    Call<CartModel> addItem(@Field("quantity") String token1, @Field("product") String token, @Field("price") String price, @Header("Authorization") String authToken);

    @FormUrlEncoded
    @POST("menu/remove-from-cart/")
    Call<RemoveCartItemModel> deleteItem(@Field("quantity") String token1, @Field("product") String token, @Field("price") String price, @Header("Authorization") String authToken);

    @GET("user/address/?")
    Call<AddressResponse> callApiToGetAddress(@Header("Authorization") String token, @Query("lat") String lat, @Query("lng") String lng);
}
