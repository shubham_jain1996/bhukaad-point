package com.foodtruck.bhukkad.network;

import android.app.Application;
import android.content.Context;
import android.view.Menu;
import android.widget.Toast;


import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.customdailog.LbmDialog;
import com.foodtruck.bhukkad.helper.InternetConnection;
import com.foodtruck.bhukkad.model.addres.AddressResponse;
import com.foodtruck.bhukkad.model.cart.CartModel;
import com.foodtruck.bhukkad.model.cart.RemoveCartItemModel;
import com.foodtruck.bhukkad.model.category.CategoryResponse;
import com.foodtruck.bhukkad.model.coupon.CouponResponse;
import com.foodtruck.bhukkad.model.discount.DiscountResponse;
import com.foodtruck.bhukkad.model.homeslider.SliderImagesResponse;
import com.foodtruck.bhukkad.model.menu.MenuResponse;
import com.foodtruck.bhukkad.model.order.MyOrderResponse;
import com.foodtruck.bhukkad.model.profile.ProfileResponse;
import com.foodtruck.bhukkad.model.register.RegisterResponse;
import com.foodtruck.bhukkad.model.review.ReviewResponse;
import com.foodtruck.bhukkad.model.viewcart.CartViewResponse;
import com.foodtruck.bhukkad.model.wallet.WalletResponse;
import com.foodtruck.bhukkad.view.HomeActivity;

import java.io.IOException;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.foodtruck.bhukkad.MainActivity.latitude;
import static com.foodtruck.bhukkad.MainActivity.longitude;

public class DataService {

    private LbmDialog helpDialog;
    Context context;
    sendResponseToActivty sendResponseToActivty;



    public interface ServerResponseListner<T> {
        public void onDataSuccess(T response,int code);
        public void onDataFailiure();


    }
    public DataService(Context context)
    {
        helpDialog= new LbmDialog(context);
        helpDialog.setCancelable(false);
        this.context = context;
    }


    public DataService()
    {
    }
    private ServerResponseListner serverResponseListner;
    public void setOnServerListener(ServerResponseListner listener) {
        serverResponseListner = listener;
    }
    private void showLoader(String message){
   helpDialog.show();

    }


 /*   public void callApiToGetWalletDetails(FetchFavServer fetchFavServer) {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        showLoader(context.getResources().getString(R.string.please_wait));
        Call<WalletDetailModel> call = RetrofitInstance.getApiInstance().callApiToGetWalletDetails(fetchFavServer);
        performServerRequest(call);
    }*/



    public void getCategory() {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
       // showLoader(context.getResources().getString(R.string.please_wait));
        Call<CategoryResponse> call = RetrofitInstance.getApiInstance().getCategory();
        performServerRequest(call);


    }

    public void getReating(String authToken, String id) {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
      //  showLoader(context.getResources().getString(R.string.please_wait));
        Call<ReviewResponse> call = RetrofitInstance.getApiInstance().getReating(authToken,id);
        performServerRequest(call);
    }

    public void getProfileData(String token) {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        //  showLoader(context.getResources().getString(R.string.please_wait));
        Call<ProfileResponse> call = RetrofitInstance.getApiInstance().getProfileData(token);
        performServerRequest(call);
    }



    public void callApiToSetAddress(String token,String name, String phone, String address, String address1, String floor,double latitude,
                                    double longitude,String postal_code) {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        showLoader(context.getResources().getString(R.string.please_wait));
        Call<WalletResponse> call = RetrofitInstance.getApiInstance().setAddress(token,name,phone,address,address1,postal_code,String.valueOf(latitude),
                String.valueOf(longitude));
        performServerRequest(call);

    }
    public void callApiToUpdateAddress(String token,String name, String phone, String address, String address1, String floor,double latitude,
                                    double longitude,String postal_code,int addressId) {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        showLoader(context.getResources().getString(R.string.please_wait));
        Call<WalletResponse> call = RetrofitInstance.getApiInstance().EditAddress(token,name,phone,address,address1,postal_code,String.valueOf(latitude),
                String.valueOf(longitude),String.valueOf(addressId),"true");
        performServerRequest(call);

    }


    public void callApiToGetData(String token) {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        showLoader(context.getResources().getString(R.string.please_wait));
        Call<MyOrderResponse> call = RetrofitInstance.getApiInstance().callApiToGetData(token);
        performServerRequest(call);
    }

    public void callApiToGetAddress(String token, double latitude, double longitude) {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        showLoader(context.getResources().getString(R.string.please_wait));
        Call<AddressResponse> call = RetrofitInstance.getApiInstance().callApiToGetAddress(token,String.valueOf(latitude),String.valueOf(longitude));
        performServerRequest(call);
    }

    public void callApiToGetWalletDetails(String token) {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        showLoader(context.getResources().getString(R.string.please_wait));
        Call<WalletResponse> call = RetrofitInstance.getApiInstance().callApiToGetWalletDetails(token);
        performServerRequest(call);
    }


    public void callApiToGetCoupon(String token) {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        showLoader(context.getResources().getString(R.string.please_wait));
        Call<CouponResponse> call = RetrofitInstance.getApiInstance().callApiToGetCoupon(token);
        performServerRequest(call);
    }

    public void getMenuData(String token ,String id, String latitude, String longitude) {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
            showLoader(context.getResources().getString(R.string.please_wait));
        Call<MenuResponse> call = RetrofitInstance.getApiInstance().getMenuData(token,id,latitude,longitude,"200");
        performServerRequest(call);

    }
    public void callApiToGetCouponStatus(String token, String coupon) {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        showLoader(context.getResources().getString(R.string.please_wait));
        Call<DiscountResponse> call = RetrofitInstance.getApiInstance().callApiToGetCouponStatus(token,coupon, String.valueOf(latitude), String.valueOf(longitude));
        performServerRequest(call);

    }

    public void getSliderImages() {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
       // showLoader(context.getResources().getString(R.string.please_wait));
        Call<SliderImagesResponse> call = RetrofitInstance.getApiInstance().getSliderImages();
        performServerRequest(call);

    }


    public void getCartData(String latitude, String longitude, String authToken) {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
       // showLoader(context.getResources().getString(R.string.please_wait));
        Call<CartViewResponse> call = RetrofitInstance.getApiInstance().getCartData(authToken,latitude,longitude);
        performServerRequest(call);
    }


    public void callApiToLogin(String userName, String password,String token) {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
           showLoader(context.getResources().getString(R.string.please_wait));
        Call<RegisterResponse> call = RetrofitInstance.getApiInstance().callApiToLogin(userName,password,token);
        performServerRequest(call);
    }

    public void updateProfile(String token, Map<String, RequestBody> map) {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        showLoader(context.getResources().getString(R.string.please_wait));
        Call<ProfileResponse> call = RetrofitInstance.getApiInstance().updateProfile(token,map);
        performServerRequest(call);
    }

/*
    public void updateProfile(String token, Map<String, RequestBody> map) {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        showLoader(context.getResources().getString(R.string.please_wait));
        Call<ProfileResponse> call = RetrofitInstance.getApiInstance().updateProfile(token,map);
        performServerRequest(call);
    }
*/


    public void callApiToGoogleLogin(String userName, String password,String token,String firebaseId) {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        showLoader(context.getResources().getString(R.string.please_wait));
        Call<RegisterResponse> call = RetrofitInstance.getApiInstance().callApiToGoogleLogin(userName,password,token,firebaseId);
        performServerRequest(call);
    }
    public void callApiToRegister(String userName, String email, String Password, String password1, String mobile, String gender, String name, String isverify,String referral,String token,String google_token) {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        showLoader(context.getResources().getString(R.string.please_wait));
        Call<RegisterResponse> call = RetrofitInstance.getApiInstance().registerUser(userName,email,password1,Password,mobile,gender,name,isverify,referral,token,google_token);
        performServerRequest(call);
    }
    public void deleteItem(int qt, Integer id, String s, String authToken) {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        showLoader(context.getResources().getString(R.string.please_wait));
        Call<RemoveCartItemModel> call = RetrofitInstance.getApiInstance().deleteItem(String.valueOf(qt),String.valueOf(id),s,authToken);
        performServerRequest(call);
    }

    public void additem(int qt, Integer id, String s, String authToken) {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        showLoader(context.getResources().getString(R.string.please_wait));
        Call<CartModel> call = RetrofitInstance.getApiInstance().addItem(String.valueOf(qt),String.valueOf(id),s,authToken);
        performServerRequest(call);
    }




    private void performServerRequest(Call call)
    {
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call call, Response response) {

                if (helpDialog != null && helpDialog.isShowing()) {
                    helpDialog.dismiss();
                }

                if (serverResponseListner != null) {
                    serverResponseListner.onDataSuccess(response.body(),response.code());

                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {

                if (helpDialog != null) {
                    helpDialog.dismiss();
                }

                if (serverResponseListner != null) {
                    serverResponseListner.onDataFailiure();
                }
                if (context != null) {
                    if (t instanceof IOException) {
                        Toast.makeText(context, R.string.no_internet_connection+t.getMessage(), Toast.LENGTH_SHORT).show();
                        // logging probably not necessary
                    }
                    else {
                        Toast.makeText(context, "Parsing Error", Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });
    }

    public interface sendResponseToActivty{
        void removeProgressBar();
    }


}

