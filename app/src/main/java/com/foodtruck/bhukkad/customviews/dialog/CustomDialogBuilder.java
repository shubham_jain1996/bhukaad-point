package com.foodtruck.bhukkad.customviews.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import androidx.core.content.ContextCompat;

import com.baoyachi.stepview.VerticalStepView;
import com.baoyachi.stepview.bean.StepBean;
import com.foodtruck.bhukkad.R;

import java.util.ArrayList;
import java.util.List;


public class CustomDialogBuilder {
    private Context mContext;
    private Dialog mBuilder = null;

    public CustomDialogBuilder(Context context) {
        this.mContext = context;
        if (mContext != null) {
            mBuilder = new Dialog(mContext);
            mBuilder.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mBuilder.setCancelable(false);
            mBuilder.setCanceledOnTouchOutside(false);

            if (mBuilder.getWindow() != null) {
                mBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            }
        }
    }






    public void showDeliveryDialogue(int step,Context context) {
        if (mContext == null)
            this.mContext=context;
        mBuilder.setCancelable(true);
        mBuilder.setCanceledOnTouchOutside(true);
        View viewInflated = LayoutInflater.from(mContext).inflate(R.layout.dailog_delivery, null);
        VerticalStepView htScroll = viewInflated.findViewById(R.id.ht_scroll);
        mBuilder.setContentView(viewInflated);
        //binding.tvTerm.setOnClickListener(v -> mContext.startActivity(new Intent(mContext, WebViewActivity.class).putExtra("type", 0)));
        //  binding.tvPrivacy.setOnClickListener(v -> mContext.startActivity(new Intent(mContext, WebViewActivity.class).putExtra("type", 1)));
     //   HorizontalStepView setpview5 = (HorizontalStepView) mView.findViewById(R.id.step_view5);
        List<String> stepsBeanList = new ArrayList<>();
        StepBean stepBean0 = new StepBean("Processing",1);
        StepBean stepBean1 = new StepBean("Confirmed",1);
        StepBean stepBean2 = new StepBean("Dispacted",1);
        StepBean stepBean3 = new StepBean("Dileverd",0);
        StepBean stepBean4 = new StepBean("Dileverd",0);
        StepBean stepBean5 = new StepBean("Dileverd",0);

        stepsBeanList.add("Processing");
        stepsBeanList.add("Confirmed");
        stepsBeanList.add("Dispacted");
         stepsBeanList.add("Dileverd");
         stepsBeanList.add("Dileverd");
         stepsBeanList.add("Dileverd");



        htScroll.setStepsViewIndicatorComplectingPosition(stepsBeanList.size() - step)//设置完成的步数
                .reverseDraw(false)//default is true
                .setStepViewTexts(stepsBeanList)//总步骤
                .setLinePaddingProportion(0.85f)//设置indicator线与线间距的比例系数
                .setStepsViewIndicatorCompletedLineColor(ContextCompat.getColor(mContext, android.R.color.white))//设置StepsViewIndicator完成线的颜色
                .setStepsViewIndicatorUnCompletedLineColor(ContextCompat.getColor(mContext, R.color.uncompleted_text_color))//设置StepsViewIndicator未完成线的颜色
                .setStepViewComplectedTextColor(ContextCompat.getColor(mContext, android.R.color.white))//设置StepsView text完成线的颜色
                .setStepViewUnComplectedTextColor(ContextCompat.getColor(mContext, R.color.uncompleted_text_color))//设置StepsView text未完成线的颜色
                .setStepsViewIndicatorCompleteIcon(ContextCompat.getDrawable(mContext, R.drawable.complted))//设置StepsViewIndicator CompleteIcon
                .setStepsViewIndicatorDefaultIcon(ContextCompat.getDrawable(mContext, R.drawable.default_icon))//设置StepsViewIndicator DefaultIcon
                .setStepsViewIndicatorAttentionIcon(ContextCompat.getDrawable(mContext, R.drawable.attention));//设置StepsViewIndicator AttentionIcon

        mBuilder.show();
        mBuilder.setCancelable(false);
    }



    public void hideLoadingDialog() {
        try {
            mBuilder.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface OnResultButtonClick {
        void onButtonClick(boolean success);
    }

    public interface OnDismissListener {
        void onPositiveDismiss();

        void onNegativeDismiss();
    }

    public interface OnCoinDismissListener {
        void onCancelDismiss();

        void on5Dismiss();

        void on10Dismiss();

        void on20Dismiss();

    }

}