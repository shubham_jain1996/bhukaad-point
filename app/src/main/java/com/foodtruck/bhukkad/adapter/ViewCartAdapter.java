package com.foodtruck.bhukkad.adapter;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.model.cart.CartModel;
import com.foodtruck.bhukkad.model.cart.RemoveCartItemModel;
import com.foodtruck.bhukkad.model.menu.MenuResponse;
import com.foodtruck.bhukkad.model.viewcart.CartDetail;
import com.foodtruck.bhukkad.network.DataService;
import com.foodtruck.bhukkad.utils.Const;
import com.foodtruck.bhukkad.utils.SessionManager;
import com.foodtruck.bhukkad.utils.Validations;
import com.foodtruck.bhukkad.view.CartActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ViewCartAdapter extends RecyclerView.Adapter<ViewCartAdapter.ViewPolicyDetailHolder>  {
    Activity myActivity;
    private LayoutInflater mInflater;
  List<CartDetail> data = new ArrayList<>();
    private SessionManager sessionManager;
    private LocalBroadcastManager mgr;
    clickEvent clickEvent;
    public ViewCartAdapter(Activity myActivity, List<CartDetail> data, CartActivity cartActivity) {
        this.myActivity = myActivity;
        this.data=data;
        mInflater = LayoutInflater.from(myActivity);
        sessionManager= new SessionManager(myActivity);
        this.clickEvent=cartActivity;

 }


    @NonNull
    @Override
    public ViewPolicyDetailHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= mInflater.inflate(R.layout.cart_layout,parent,false);
        Log.d("Shubham", "again: ");

        return new ViewPolicyDetailHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewPolicyDetailHolder viewHolder, final int position) {
        viewHolder.tvTitle.setText(data.get(position).getProductName());
       // viewHolder.tvDesricption.setText(data.get(position).getProductDescription());
        viewHolder.tvQuantity.setText(String.valueOf(data.get(position).getQuantity()));
        viewHolder.tvPrice.setText(Validations.stringPrice(data.get(position).getPrice()));
        viewHolder.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
                    int qt= data.get(position).getQuantity();
                    qt++;
                    data.get(position).setQuantity(qt);
                    viewHolder.tvQuantity.setText(String.valueOf(data.get(position).getQuantity()));
                     clickEvent.clickAddButton(qt,data.get(position).getProduct(),data.get(position).getPrice());
                }else {
                    sendErrorMsgToActivity("open",0);

                }

            }
        });
        if (data.get(position).getValidStatus()){
            viewHolder.rltView.setVisibility(View.GONE);
        }else {
            viewHolder.rltView.setVisibility(View.VISIBLE);

        }
        viewHolder.btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
                    int qt= data.get(position).getQuantity();

                    if (qt>0){
                        qt--;
                        data.get(position).setQuantity(qt);
                        viewHolder.tvQuantity.setText(String.valueOf(data.get(position).getQuantity()));
                        clickEvent.clickDeleteButton(qt,data.get(position).getProduct(),data.get(position).getPrice());
                    }
                }else {
                    sendErrorMsgToActivity("open",0);
                }


            }
        });

        viewHolder.tvRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickEvent.clickDeleteButton(0,data.get(position).getProduct(),data.get(position).getPrice());
            }
        });

        if (data.get(position).getImage() != null) {
            Log.d("Sahil", "populateItemRows1: "+"http://3.17.125.65"+data.get(position).getImage());
            Picasso.get().load("http://3.17.125.65"+data.get(position).getImage()).into(viewHolder.dish_img);
        }


    }

 /*   private void callApiToDeleteQuantity(int qt, Integer id, String s) {
        DataService dataService = new DataService(myActivity);
        dataService.setOnServerListener(new DataService.ServerResponseListner<RemoveCartItemModel>() {
            @Override
            public void onDataSuccess(RemoveCartItemModel response, int code) {
                if (response!=null &&  response.getSuccess() && response.getData()!=null){
                    sendErrorMsgToActivity("cart_count",response.getData().getCartCount());
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.deleteItem(qt,id,s,"Token "+sessionManager.getUser().getData().getAuthToken());
    }

    private void callApiToAddQuantity(int qt, Integer id, String s) {
        DataService dataService = new DataService(myActivity);
        dataService.setOnServerListener(new DataService.ServerResponseListner<CartModel>() {
            @Override
            public void onDataSuccess(CartModel response, int code) {
                if (response!=null &&  response.getSuccess() && response.getData()!=null){
                    sendErrorMsgToActivity("cart_count",response.getData().getCartCount());
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.additem(qt,id,s,"Token "+sessionManager.getUser().getData().getAuthToken());
    }*/


    private void sendErrorMsgToActivity(String sessionExpire,int i) {
        Intent intent = new Intent("attend");
        intent.putExtra("session",sessionExpire);
        intent.putExtra("cartValue",String.valueOf(i));
        if (myActivity!=null) {
            mgr = LocalBroadcastManager.getInstance(myActivity);
        }
        mgr.sendBroadcast(intent);
    }


    @Override
    public int getItemCount() {


      return   data.size();

}



    public class ViewPolicyDetailHolder extends RecyclerView.ViewHolder {


        ImageView dish_img;
        TextView tvTitle,tvQuantity,tvPrice;
        private ImageView btnAdd,btnMinus;
        RelativeLayout rltView;
        TextView tvRemove;
        public ViewPolicyDetailHolder(@NonNull View itemView) {
            super(itemView);
            dish_img=itemView.findViewById(R.id.img_dish);
            tvTitle=itemView.findViewById(R.id.tv_title);

            btnAdd=itemView.findViewById(R.id.btn_add);
            btnMinus=itemView.findViewById(R.id.btn_minus);
            tvPrice=itemView.findViewById(R.id.tv_price);
            tvQuantity=itemView.findViewById(R.id.tv_quantity);
            tvRemove=itemView.findViewById(R.id.tvRemove);
            rltView=itemView.findViewById(R.id.rlt_view);
           // tvStatus=itemView.findViewById(R.id.tvStatus);
        }
    }


    public interface clickEvent {
        void clickAddButton(int qty, int position,int price);
        void clickDeleteButton(int qty, int position,int price);
    }






}

