package com.foodtruck.bhukkad.adapter;




import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.model.category.CategoryResponse;
import com.foodtruck.bhukkad.view.fragment.OtherTabFragment;

import java.util.ArrayList;
import java.util.List;


public  class ViewPagerAdapter extends FragmentPagerAdapter {
    public List<CategoryResponse.Datum> categoryname = new ArrayList<>();

    private FragmentManager mFragmentManager;
    private Fragment mFragmentAtPos0;

    public ViewPagerAdapter(FragmentManager manager, List<CategoryResponse.Datum> categoryname) {
        super(manager);
        this.categoryname= categoryname;
        mFragmentManager = manager;
    }
    @Override
    public int getItemPosition(Object object) {

            return POSITION_NONE;

    }
    @Override
    public Fragment getItem(int position) {

            return new OtherTabFragment().newInstance(categoryname.get(position).getName(),categoryname.get(position).getId());

    }

    @Override
    public int getCount() {
        return categoryname.size();
    }

   @Override
    public CharSequence getPageTitle(int position) {
        return categoryname.get(position).getName().toUpperCase();
    }





}
