package com.foodtruck.bhukkad.adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.foodtruck.bhukkad.MainActivity;
import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.model.addres.AddressData;
import com.foodtruck.bhukkad.utils.SessionManager;
import com.foodtruck.bhukkad.utils.Validations;
import com.foodtruck.bhukkad.view.address.AddAddres;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class ViewAddressAdapter extends RecyclerView.Adapter<ViewAddressAdapter.ViewPolicyDetailHolder>  {
    Activity myActivity;
    private LayoutInflater mInflater;
  List<AddressData> data = new ArrayList<>();
    clickResponse clickResponse;
    private SessionManager sessionManager;
    private LocalBroadcastManager mgr;
    public ViewAddressAdapter(Activity myActivity, List<AddressData> data) {
        this.myActivity = myActivity;
        this.data=data;
        mInflater = LayoutInflater.from(myActivity);
        sessionManager= new SessionManager(myActivity);


 }


    @NonNull
    @Override
    public ViewPolicyDetailHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= mInflater.inflate(R.layout.address_layout,parent,false);

        return new ViewPolicyDetailHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewPolicyDetailHolder viewHolder, final int position) {
        if (data.get(position).getValidStatus()){
            viewHolder.tvDeliver.setText("Deliver To");
            viewHolder.tvDeliver.setTextColor(myActivity.getResources().getColor(R.color.blue));
            viewHolder.imgSign.setImageDrawable(myActivity.getResources().getDrawable(R.drawable.ic_check));
        }else {
            viewHolder.tvDeliver.setText("Does not deliver to");
            viewHolder.imgSign.setImageDrawable(myActivity.getResources().getDrawable(R.drawable.ic_delete));
            viewHolder.tvDeliver.setTextColor(myActivity.getResources().getColor(R.color.quantum_googred));
        }
        viewHolder.tvAddress.setText(data.get(position).getLine1());
        viewHolder.imgDot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(myActivity, viewHolder.imgDot);
                //Inflating the Popup using xml file
                popup.getMenuInflater()
                        .inflate(R.menu.popup_menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        Intent i = new Intent(myActivity,AddAddres.class);
                        i.putExtra("id",data.get(position).getId());
                        i.putExtra("lat",String.valueOf(data.get(position).getLat()));
                        i.putExtra("long",String.valueOf(data.get(position).getLng()));
                        myActivity.startActivity(i);
                        return true;
                    }
                });

                popup.show();
            }
        });
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (data.get(position).getValidStatus()){
                    Intent i = new Intent();
                    i.putExtra("data",data.get(position).getLine1());
                    i.setData(Uri.parse(data.get(position).getLine1()));
                    myActivity.setResult(RESULT_OK, i);
                    myActivity.finish();
                }else {
                    Toast.makeText(myActivity, "No Deliver to this address", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    @Override
    public int getItemCount() {


      return   data.size();

}



    public class ViewPolicyDetailHolder extends RecyclerView.ViewHolder {


        TextView tvDeliver,tvAddress;
        ImageView imgSign, imgDot;
        public ViewPolicyDetailHolder(@NonNull View itemView) {
            super(itemView);
            tvDeliver=itemView.findViewById(R.id.tvDeliver);
            tvAddress=itemView.findViewById(R.id.tvAddress);
            imgDot=itemView.findViewById(R.id.imgDot);
            imgSign=itemView.findViewById(R.id.img_sign);

        }
    }


public interface clickResponse{
        void clickOnCoupon(String code);
}




}

