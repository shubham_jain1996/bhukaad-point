package com.foodtruck.bhukkad.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.model.cart.CartModel;
import com.foodtruck.bhukkad.model.cart.RemoveCartItemModel;
import com.foodtruck.bhukkad.model.menu.MenuResponse;
import com.foodtruck.bhukkad.model.order.MyOrderResponseList;
import com.foodtruck.bhukkad.network.DataService;
import com.foodtruck.bhukkad.utils.Const;
import com.foodtruck.bhukkad.utils.SessionManager;
import com.foodtruck.bhukkad.utils.Validations;
import com.foodtruck.bhukkad.view.Orderdetails;
import com.foodtruck.bhukkad.view.TrackOrder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RunningOrderAdapter extends RecyclerView.Adapter<RunningOrderAdapter.ViewPolicyDetailHolder>  {
    Activity myActivity;
    private LayoutInflater mInflater;
  ArrayList<MyOrderResponseList> data = new ArrayList<>();
    private SessionManager sessionManager;
    private LocalBroadcastManager mgr;
    public RunningOrderAdapter(Activity myActivity, ArrayList<MyOrderResponseList> data) {
        this.myActivity = myActivity;
        this.data= data;
        mInflater = LayoutInflater.from(myActivity);
        sessionManager= new SessionManager(myActivity);

 }


    @NonNull
    @Override
    public ViewPolicyDetailHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= mInflater.inflate(R.layout.running_order,parent,false);
        return new ViewPolicyDetailHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewPolicyDetailHolder viewHolder, final int position) {
        viewHolder.tvTitle.setText(""+data.get(position).getId());
        viewHolder.tvPrice.setText(Validations.stringPrice(data.get(position).getTotal()));
        viewHolder.tvOrderOn.setText(data.get(position).getCreatedAt());
        viewHolder.tvItems.setText(Validations.getItemNames(data.get(position).getItems()));
        viewHolder.btDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(myActivity, Orderdetails.class);
                i.putParcelableArrayListExtra("data",data.get(position).getItems());
                myActivity.startActivity(i);
            }
        });
        viewHolder.btTrack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(myActivity, TrackOrder.class);
               i.putExtra("data",data.get(position).getId());
                myActivity.startActivity(i);
            }
        });
     }



  @Override
    public int getItemCount() {


      return   data.size();

}



    public class ViewPolicyDetailHolder extends RecyclerView.ViewHolder {


        TextView tvTitle,tvPrice,tvItems,tvOrderOn;
        TextView btDetails, btTrack;
        public ViewPolicyDetailHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle=itemView.findViewById(R.id.tv_title);
            tvPrice=itemView.findViewById(R.id.tv_price);
            btDetails=itemView.findViewById(R.id.btDetails);
            btTrack=itemView.findViewById(R.id.btTrack);
            tvItems=itemView.findViewById(R.id.tv_items);
            tvOrderOn=itemView.findViewById(R.id.tv_order_on);

        }
    }








}

