package com.foodtruck.bhukkad.adapter;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.model.cart.CartModel;
import com.foodtruck.bhukkad.model.cart.RemoveCartItemModel;
import com.foodtruck.bhukkad.model.menu.MenuResponse;
import com.foodtruck.bhukkad.model.order.ItemResopnse;
import com.foodtruck.bhukkad.network.DataService;
import com.foodtruck.bhukkad.utils.Const;
import com.foodtruck.bhukkad.utils.SessionManager;
import com.foodtruck.bhukkad.utils.Validations;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ViewDetailAdapter extends RecyclerView.Adapter<ViewDetailAdapter.ViewPolicyDetailHolder>  {
    Activity myActivity;
    private LayoutInflater mInflater;
  List<ItemResopnse> data = new ArrayList<>();
    public ViewDetailAdapter(Activity myActivity, List<ItemResopnse> data) {
        this.myActivity = myActivity;
        this.data=data;
        mInflater = LayoutInflater.from(myActivity);

 }


    @NonNull
    @Override
    public ViewPolicyDetailHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= mInflater.inflate(R.layout.detail_layout,parent,false);
        Log.d("Shubham", "again: ");

        return new ViewPolicyDetailHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewPolicyDetailHolder viewHolder, final int position) {
        if (data.get(position).getProductName()!=null) {
            viewHolder.tvTitle.setText(data.get(position).getProductName().toUpperCase());
        }
        /*if (data.get(position).get()!=null) {
            viewHolder.tvPrice.setText(Validations.stringPrice(data.get(position).getPrice()));
        }

        if (data.get(position).getImage() != null) {
            Log.d("Sahil", "populateItemRows1: "+"http://3.17.125.65"+data.get(position).getImage());
            Picasso.get().load("http://3.17.125.65"+data.get(position).getImage()).into(viewHolder.dish_img);
        }*/


    }





    @Override
    public int getItemCount() {


      return   data.size();

}



    public class ViewPolicyDetailHolder extends RecyclerView.ViewHolder {


        ImageView dish_img;
        TextView tvTitle,tvPrice;
        public ViewPolicyDetailHolder(@NonNull View itemView) {
            super(itemView);
            dish_img=itemView.findViewById(R.id.img_dish);
            tvTitle=itemView.findViewById(R.id.tv_title);

            tvPrice=itemView.findViewById(R.id.tv_price);
        }
    }








}

