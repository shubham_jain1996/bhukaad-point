package com.foodtruck.bhukkad.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;


import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.model.cart.CartModel;
import com.foodtruck.bhukkad.model.cart.RemoveCartItemModel;
import com.foodtruck.bhukkad.model.menu.MenuResponse;
import com.foodtruck.bhukkad.network.DataService;
import com.foodtruck.bhukkad.utils.Const;
import com.foodtruck.bhukkad.utils.SessionManager;
import com.foodtruck.bhukkad.utils.Validations;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewMenuAdapter extends RecyclerView.Adapter<ViewMenuAdapter.ViewPolicyDetailHolder>  {
    Activity myActivity;
    private LayoutInflater mInflater;
  List<MenuResponse.Datum> data = new ArrayList<>();
    private SessionManager sessionManager;
    private LocalBroadcastManager mgr;
    clickEvent clickEvent;
    public ViewMenuAdapter(Activity myActivity, List<MenuResponse.Datum> data) {
        this.myActivity = myActivity;
        this.data=data;
        mInflater = LayoutInflater.from(myActivity);
        sessionManager= new SessionManager(myActivity);

 }


    @NonNull
    @Override
    public ViewPolicyDetailHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= mInflater.inflate(R.layout.double_layout,parent,false);
        Log.d("Shubham", "again: ");

        return new ViewPolicyDetailHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewPolicyDetailHolder viewHolder, final int position) {
        viewHolder.tvTitle.setText(data.get(position).getName().toUpperCase());
      //  viewHolder.tvdescription.setText(data.get(position).getDescription());

        viewHolder.tvQuantity.setText(String.valueOf(data.get(position).getQuantity()));
        viewHolder.tvPrice.setText(Validations.stringPrice(data.get(position).getPrice()));
        viewHolder.tvPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        viewHolder.tvDiscount.setText(data.get(position).getDiscount());
        viewHolder.tvPriceDiscont.setText(Validations.stringPrice(data.get(position).getOurPrice()));
        viewHolder.tvDescription.setText(data.get(position).getDescription());
        if (data.get(position).getTopCategory().equals("veg")){
            viewHolder.imgCategory.setImageDrawable(myActivity.getResources().getDrawable(R.drawable.veg_icon));
        }else if (data.get(position).getTopCategory().equals("non_veg")){
            viewHolder.imgCategory.setImageDrawable(myActivity.getResources().getDrawable(R.drawable.non_veg_icon));
        }else if (data.get(position).getTopCategory().equals("egg")){
            viewHolder.imgCategory.setImageDrawable(myActivity.getResources().getDrawable(R.drawable.egg_icon));
        }

        if (data.get(position).getAverageRating()!=null){
            viewHolder.tvRating.setText(data.get(position).getAverageRating());
        }else {
            viewHolder.tvRating.setText("5");
        }
        viewHolder.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
                    int qt= data.get(position).getQuantity();
                    qt++;
                    data.get(position).setQuantity(qt);
                    viewHolder.tvQuantity.setText(String.valueOf(data.get(position).getQuantity()));
                    callApiToAddQuantity(qt,data.get(position).getId(),String.valueOf(data.get(position).getPrice()));
                  //   clickEvent.clickAddButton(qt,data.get(position).getId(),String.valueOf(data.get(position).getPrice()));
                }else {
                    sendErrorMsgToActivity("open",0);

                }

            }
        });
        viewHolder.btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
                    int qt= data.get(position).getQuantity();

                    if (qt>0){
                        qt--;
                        data.get(position).setQuantity(qt);
                        viewHolder.tvQuantity.setText(String.valueOf(data.get(position).getQuantity()));
                        callApiToDeleteQuantity(qt,data.get(position).getId(),String.valueOf(data.get(position).getPrice()));
                       //   clickEvent.clickDeleteButton(qt,data.get(position).getId(),String.valueOf(data.get(position).getPrice()));
                    }
                }else {
                    sendErrorMsgToActivity("open",0);
                }


            }
        });
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (data.get(position).getOpenStatus()!=null && data.get(position).getOpenStatus()) {
                    openDetailsPage("details", data.get(position).getName(), data.get(position).getDescription(),
                            data.get(position).getImage(), data.get(position).getId(), data.get(position).getOurPrice(),data.get(position).getAverageRating());
                }else {
                    Toast.makeText(myActivity, "Restaurant closed", Toast.LENGTH_SHORT).show();
                }
            }
        });

        if (data.get(position).getImage() != null) {
            Log.d("Sahil", "populateItemRows1: "+"http://3.17.125.65"+data.get(position).getImage());
            Picasso.get().load("http://3.17.125.65"+data.get(position).getImage()).into(viewHolder.dish_img);
        }
        if (data.get(position).getOpenStatus()!=null && data.get(position).getOpenStatus()){
            viewHolder.mainCardView.setAlpha((float) 1);
           // viewHolder.tvClose.setVisibility(View.INVISIBLE);
            viewHolder.btnMinus.setEnabled(true);
            viewHolder.btnAdd.setEnabled(true);
        }else {
            viewHolder.mainCardView.setAlpha((float) 0.5);
          //  viewHolder.tvClose.setVisibility(View.VISIBLE);
            viewHolder.btnMinus.setEnabled(false);
            viewHolder.btnAdd.setEnabled(false);
        }


    }

    private void callApiToDeleteQuantity(int qt, Integer id, String s) {
        DataService dataService = new DataService(myActivity);
        dataService.setOnServerListener(new DataService.ServerResponseListner<RemoveCartItemModel>() {
            @Override
            public void onDataSuccess(RemoveCartItemModel response, int code) {
                if (response!=null &&  response.getSuccess() && response.getData()!=null){
                    sendErrorMsgToActivity("cart_count",response.getData().getCartCount());
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.deleteItem(qt,id,s,"Token "+sessionManager.getUser().getData().getAuthToken());
    }

    private void callApiToAddQuantity(int qt, Integer id, String s) {
        DataService dataService = new DataService(myActivity);
        dataService.setOnServerListener(new DataService.ServerResponseListner<CartModel>() {
            @Override
            public void onDataSuccess(CartModel response, int code) {
                if (response!=null &&  response.getSuccess() && response.getData()!=null){
                    sendErrorMsgToActivity("cart_count",response.getData().getCartCount());
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });

        dataService.additem(qt,id,s,"Token "+sessionManager.getUser().getData().getAuthToken());
    }


    private void sendErrorMsgToActivity(String sessionExpire,int i) {
        Intent intent = new Intent("attend");
        intent.putExtra("session",sessionExpire);
        intent.putExtra("cartValue",String.valueOf(i));
        if (myActivity!=null) {
            mgr = LocalBroadcastManager.getInstance(myActivity);
        }
        mgr.sendBroadcast(intent);
    }
    private void openDetailsPage(String type, String title, String description, String image,Integer id,Integer ourPrice, String rating) {
        Intent intent = new Intent("attend");
        intent.putExtra("session",type);
        intent.putExtra("title",title);
        intent.putExtra("description",description);
        intent.putExtra("image",image);
        intent.putExtra("price",ourPrice);
        intent.putExtra("rating",rating);
        intent.putExtra("id",String.valueOf(id));
        if (myActivity!=null) {
            mgr = LocalBroadcastManager.getInstance(myActivity);
        }
        mgr.sendBroadcast(intent);
    }

    @Override
    public int getItemCount() {


      return   data.size();

}



    public class ViewPolicyDetailHolder extends RecyclerView.ViewHolder {


        ImageView dish_img;
        TextView tvTitle,tvQuantity,tvPrice,tvPriceDiscont,tvDiscount,tvRating,tvDescription;
        private ImageView btnAdd,btnMinus;
        ImageView imgCategory;
        CardView mainCardView;
        public ViewPolicyDetailHolder(@NonNull View itemView) {
            super(itemView);
            dish_img=itemView.findViewById(R.id.img_dish);
            tvTitle=itemView.findViewById(R.id.tv_title);
          //  tvdescription=itemView.findViewById(R.id.tv_description);
            btnAdd=itemView.findViewById(R.id.btn_add);
            btnMinus=itemView.findViewById(R.id.btn_minus);
            tvPrice=itemView.findViewById(R.id.tv_price);
            tvQuantity=itemView.findViewById(R.id.tv_quantity);
            imgCategory=itemView.findViewById(R.id.img_category);
            mainCardView= itemView.findViewById(R.id.mainCardView);
            tvPriceDiscont= itemView.findViewById(R.id.tv_price_discount);
            tvDiscount= itemView.findViewById(R.id.tvDiscount);
            tvRating= itemView.findViewById(R.id.tvRating);
            tvDescription= itemView.findViewById(R.id.tvDescription);
        }
    }


    public interface clickEvent {
        void clickAddButton(int qty, int position,String price);
        void clickDeleteButton(int qty, int position,String price);
    }






}

