package com.foodtruck.bhukkad.adapter;




import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.foodtruck.bhukkad.model.category.CategoryResponse;
import com.foodtruck.bhukkad.view.fragment.OtherTabFragment;

import java.util.ArrayList;
import java.util.List;


public  class ViewPagerTrackAdapter extends FragmentPagerAdapter {
    public List<String> categoryname = new ArrayList<>();

    private FragmentManager mFragmentManager;
    private Fragment mFragmentAtPos0;

    public ViewPagerTrackAdapter(FragmentManager manager, List<String> categoryname) {
        super(manager);
        this.categoryname= categoryname;
        mFragmentManager = manager;
    }
    @Override
    public int getItemPosition(Object object) {

            return POSITION_NONE;

    }
    @Override
    public Fragment getItem(int position) {

            return new OtherTabFragment().newInstance(categoryname.get(position),1);

    }

    @Override
    public int getCount() {
        return categoryname.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return categoryname.get(position).toUpperCase();
    }


}
