package com.foodtruck.bhukkad.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.viewpager.widget.PagerAdapter;

import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.model.homeslider.SliderData;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class SlidingImageAdapter extends PagerAdapter {


    private List<SliderData.Image> bannerTextData=new ArrayList<>();

    private LayoutInflater inflater;
    private Context context;



    public SlidingImageAdapter(Context context, List<SliderData.Image> HomeSlideImageModel) {
        this.context = context;
        this.bannerTextData=HomeSlideImageModel;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return bannerTextData.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.slidingimage_layout_home, view, false);
        ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.img_banner);
        ProgressBar progressBar;
        progressBar=imageLayout.findViewById(R.id.progressBar);

        SliderData.Image homeClass = bannerTextData.get(position);

        if (homeClass!=null){
            Picasso.get().load("http://3.17.125.65"+homeClass.getImages()).into(imageView, new Callback() {
                @Override
                public void onSuccess() {
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError(Exception e) {
                    progressBar.setVisibility(View.GONE);
                }
            });
        }


        view.addView(imageLayout, 0);
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
  /*  public void downLoadImages(ImageView imageView, HomeClass data, ProgressBar progressBar ){
        DataService dataService = new DataService();
        dataService.setOnServerListener(new DataService.ServerResponseListner<Bannerdatamodel>() {
            @Override
            public void onDataSuccess(Bannerdatamodel response, int code) {
                if (response != null && code==200) {
                    try {
                        data.setMedia_url(response.getSourceUrl());
                        Picasso.get().load(data.getMedia_url()).into(imageView, new Callback() {
                            @Override
                            public void onSuccess() {
                                progressBar.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError(Exception e) {
                                progressBar.setVisibility(View.GONE);
                            }
                        });
                    }catch (Exception e){

                    }

                }

            }

            @Override
            public void onDataFailiure() {
            }
        });
        dataService.getBannerData(String.valueOf(data.getFeaturedMedia()));

    }*/
}
