package com.foodtruck.bhukkad.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.model.order.ItemResopnse;
import com.foodtruck.bhukkad.model.review.ReviewData;

import java.util.ArrayList;
import java.util.List;

public class ViewReviewAdapter extends RecyclerView.Adapter<ViewReviewAdapter.ViewPolicyDetailHolder>  {
    Activity myActivity;
    private LayoutInflater mInflater;
  List<ReviewData> data = new ArrayList<>();
    public ViewReviewAdapter(Activity myActivity, List<ReviewData> data) {
        this.myActivity = myActivity;
        this.data=data;
        mInflater = LayoutInflater.from(myActivity);

 }


    @NonNull
    @Override
    public ViewPolicyDetailHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= mInflater.inflate(R.layout.review_layout,parent,false);
        return new ViewPolicyDetailHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewPolicyDetailHolder viewHolder, final int position) {
        viewHolder.txt_name.setText(data.get(position).getCreatedByName());
        viewHolder.txt_rating.setText(String.valueOf(data.get(position).getRatings()));
        viewHolder.txt_review.setText(data.get(position).getDescription());

    }





    @Override
    public int getItemCount() {


      return   data.size();

}



    public class ViewPolicyDetailHolder extends RecyclerView.ViewHolder {


        ImageView img_user;
        TextView txt_name,txt_rating,txt_review;

        public ViewPolicyDetailHolder(@NonNull View itemView) {
            super(itemView);
            img_user=itemView.findViewById(R.id.img_user);
            txt_name=itemView.findViewById(R.id.txt_name);
            txt_rating=itemView.findViewById(R.id.txt_rating);
            txt_review=itemView.findViewById(R.id.txt_review);
        }
    }








}

