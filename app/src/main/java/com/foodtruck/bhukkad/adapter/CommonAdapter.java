package com.foodtruck.bhukkad.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.view.menu.MenuAdapter;
import androidx.cardview.widget.CardView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;


import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.model.cart.CartModel;
import com.foodtruck.bhukkad.model.cart.RemoveCartItemModel;
import com.foodtruck.bhukkad.model.menu.MenuResponse;
import com.foodtruck.bhukkad.network.DataService;
import com.foodtruck.bhukkad.utils.Const;
import com.foodtruck.bhukkad.utils.SessionManager;
import com.foodtruck.bhukkad.utils.Validations;
import com.foodtruck.bhukkad.view.CartActivity;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CommonAdapter extends RecyclerView.Adapter {


    private final int HEADER = 0;
    private final int FOOTER = 1;
    private final int VIEW_TYPE_ITEM = 2;

    Context context;
 //   List<HomeClass> allData = new ArrayList<>();
 List<List<MenuResponse.Datum>> data = new ArrayList<>();
    private SessionManager sessionManager;
    private LocalBroadcastManager mgr;


    public CommonAdapter(Context context, List<List<MenuResponse.Datum>> allData) {
        this.context = context;
        this.data = allData;
        sessionManager= new SessionManager(context);

    }



    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       if (viewType == HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header, parent, false);
            return new ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.simple_layout, parent, false);
            return new ItemViewHolder1(view);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof ItemViewHolder) {
            populateItemRows((ItemViewHolder) viewHolder, position);
        } else if (viewHolder instanceof ItemViewHolder1) {
            populateItemRows1((ItemViewHolder1) viewHolder, position);
        }
    }



    @Override
    public int getItemCount() {
        return data.size();
    }


    @Override
    public int getItemViewType(int position) {
       if (data.get(position).size() > 1) {
            return HEADER;
        } else  {
            return FOOTER;
        }
    }


    private class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView tvHeader;
        RecyclerView rvitem;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvHeader = itemView.findViewById(R.id.tvheader);
            rvitem= itemView.findViewById(R.id.rvItem);
            rvitem.setLayoutManager(new LinearLayoutManager(context,RecyclerView.HORIZONTAL,false){
                @Override
                public boolean checkLayoutParams(RecyclerView.LayoutParams lp) {
                       lp.width=getWidth()/2;
                    return true;
                }
            });
        }
    }

    private class ItemViewHolder1 extends RecyclerView.ViewHolder {
       ImageView dish_img;
       TextView tvTitle,tvPrice,tvQuantity,tvPriceDiscont,tvDiscount,tvRating,tvDescription;
        private ImageView btnAdd,btnMinus;
        TextView tvClose;
        ImageView imgCategory;
        CardView mainCardView;


        public ItemViewHolder1(@NonNull View itemView) {
            super(itemView);
            dish_img=itemView.findViewById(R.id.img_dish);
            tvTitle=itemView.findViewById(R.id.tv_title);
            btnAdd=itemView.findViewById(R.id.btn_add);
            btnMinus=itemView.findViewById(R.id.btn_minus);
            tvPrice=itemView.findViewById(R.id.tv_price);
            tvQuantity=itemView.findViewById(R.id.tv_quantity);
            tvClose=itemView.findViewById(R.id.tvClose);
            imgCategory=itemView.findViewById(R.id.img_category);
            mainCardView= itemView.findViewById(R.id.mainCardView);
            tvPriceDiscont= itemView.findViewById(R.id.tv_price_discount);
            tvDiscount= itemView.findViewById(R.id.tvDiscount);
            tvRating= itemView.findViewById(R.id.tvRating);
            tvDescription= itemView.findViewById(R.id.tvDescription);

        }
    }



    private void populateItemRows(ItemViewHolder viewHolder, int i) {
        viewHolder.tvHeader.setText(data.get(i).get(0).getName().toUpperCase());
        ViewMenuAdapter viewMenuAdapter = new ViewMenuAdapter((Activity) context,data.get(i));
        viewHolder.rvitem.setAdapter(viewMenuAdapter);

    }

    private void populateItemRows1(ItemViewHolder1 viewHolder, int position) {
        viewHolder.tvTitle.setText(data.get(position).get(0).getName().toUpperCase());
        //viewHolder.tvDescription.setText(data.get(position).get(0).getDescription());
        viewHolder.tvQuantity.setText(String.valueOf(data.get(position).get(0).getQuantity()));
        viewHolder.tvPrice.setText(Validations.stringPrice(data.get(position).get(0).getPrice()));
        viewHolder.tvPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        viewHolder.tvDiscount.setText(data.get(position).get(0).getDiscount());
        viewHolder.tvDescription.setText(data.get(position).get(0).getDescription());
        viewHolder.tvPriceDiscont.setText(Validations.stringPrice(data.get(position).get(0).getOurPrice()));
        if (data.get(position).get(0).getTopCategory().equals("veg")){
            viewHolder.imgCategory.setImageDrawable(context.getResources().getDrawable(R.drawable.veg_icon));
        }else if (data.get(position).get(0).getTopCategory().equals("non_veg")){
            viewHolder.imgCategory.setImageDrawable(context.getResources().getDrawable(R.drawable.non_veg_icon));
        }else if (data.get(position).get(0).getTopCategory().equals("egg")){
            viewHolder.imgCategory.setImageDrawable(context.getResources().getDrawable(R.drawable.egg_icon));
        }

        if (data.get(position).get(0).getAverageRating()!=null){
            viewHolder.tvRating.setText(data.get(position).get(0).getAverageRating());
            viewHolder.tvRating.setVisibility(View.VISIBLE);
        }else {
            viewHolder.tvRating.setText("5");
            viewHolder.tvRating.setVisibility(View.GONE);
        }

        viewHolder.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
                    int qt= data.get(position).get(0).getQuantity();
                    qt++;
                    data.get(position).get(0).setQuantity(qt);
                    viewHolder.tvQuantity.setText(String.valueOf(data.get(position).get(0).getQuantity()));
                    callApiToAddQuantity(qt,data.get(position).get(0).getId(),String.valueOf(data.get(position).get(0).getPrice()));

                    // clickEvent.clickAddButton(qt,menuData.get(position).getId(),menuData.get(position).getPrice());
                }else {
                    sendErrorMsgToActivity("open",0);

                }

            }
        });
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (data.get(position).get(0).getOpenStatus()!=null && data.get(position).get(0).getOpenStatus()){
                openDetailsPage("details",data.get(position).get(0).getName(),data.get(position).get(0).getDescription(),
                        data.get(position).get(0).getImage(),data.get(position).get(0).getId(),data.get(position).get(0).getOurPrice(),
                        data.get(position).get(0).getAverageRating());
            }else {
                    Toast.makeText(context, "Restaurant closed", Toast.LENGTH_SHORT).show();
                }

            }
        });
        viewHolder.btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
                    int qt= data.get(position).get(0).getQuantity();

                    if (qt>0){
                        qt--;
                        data.get(position).get(0).setQuantity(qt);
                        viewHolder.tvQuantity.setText(String.valueOf(data.get(position).get(0).getQuantity()));
                        callApiToDeleteQuantity(qt,data.get(position).get(0).getId(),String.valueOf(data.get(position).get(0).getPrice()));

                        //  clickEvent.clickDeleteButton(qt,menuData.get(position).getId(),menuData.get(position).getPrice());
                    }
                }else {
                   sendErrorMsgToActivity("open",0);
                }


            }
        });

       if (data.get(position).get(0).getImage() != null) {
            Picasso.get().load("http://3.17.125.65"+data.get(position).get(0).getImage()).into(viewHolder.dish_img);
        }

       if (data.get(position).get(0).getOpenStatus()!=null && data.get(position).get(0).getOpenStatus()){
            viewHolder.mainCardView.setAlpha((float) 1);
           viewHolder.tvClose.setVisibility(View.GONE);
           viewHolder.btnMinus.setEnabled(true);
           viewHolder.btnAdd.setEnabled(true);
       }else {
           viewHolder.mainCardView.setAlpha((float) 0.5);
           viewHolder.tvClose.setVisibility(View.VISIBLE);
           viewHolder.btnMinus.setEnabled(false);
           viewHolder.btnAdd.setEnabled(false);
       }


    }


    public interface ClickMoreButton {
        void clickMore();
    }
    private void sendErrorMsgToActivity(String sessionExpire, int i) {
        Intent intent = new Intent("attend");
        intent.putExtra("session",sessionExpire);
        intent.putExtra("cartValue",String.valueOf(i));
        if (context!=null) {
            mgr = LocalBroadcastManager.getInstance(context);
        }
        mgr.sendBroadcast(intent);
    }

    private void openDetailsPage(String type, String title, String description, String image, Integer id,Integer ourPrice, String rating) {
        Intent intent = new Intent("attend");
        intent.putExtra("session",type);
        intent.putExtra("title",title);
        intent.putExtra("description",description);
        intent.putExtra("image",image);
        intent.putExtra("price",ourPrice);
        intent.putExtra("rating",rating);
        intent.putExtra("id",String.valueOf(id));
        if (context!=null) {
            mgr = LocalBroadcastManager.getInstance(context);
        }
        mgr.sendBroadcast(intent);
    }

    private void callApiToDeleteQuantity(int qt, Integer id, String s) {
        DataService dataService = new DataService(context);
        dataService.setOnServerListener(new DataService.ServerResponseListner<RemoveCartItemModel>() {
            @Override
            public void onDataSuccess(RemoveCartItemModel response, int code) {
                if (response!=null &&  response.getSuccess() && response.getData()!=null){
                    sendErrorMsgToActivity("cart_count",response.getData().getCartCount());
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.deleteItem(qt,id,s,"Token "+sessionManager.getUser().getData().getAuthToken());
    }

    private void callApiToAddQuantity(int qt, Integer id, String s) {
        DataService dataService = new DataService(context);
        dataService.setOnServerListener(new DataService.ServerResponseListner<CartModel>() {
            @Override
            public void onDataSuccess(CartModel response, int code) {
                if (response!=null &&  response.getSuccess() && response.getData()!=null){
                    sendErrorMsgToActivity("cart_count",response.getData().getCartCount());
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.additem(qt,id,s,"Token "+sessionManager.getUser().getData().getAuthToken());
    }


}

