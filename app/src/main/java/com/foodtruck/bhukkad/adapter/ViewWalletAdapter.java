package com.foodtruck.bhukkad.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.model.coupon.Offer;
import com.foodtruck.bhukkad.utils.SessionManager;
import com.foodtruck.bhukkad.utils.Validations;
import com.foodtruck.bhukkad.view.CartActivity;

import java.util.ArrayList;
import java.util.List;

public class ViewWalletAdapter extends RecyclerView.Adapter<ViewWalletAdapter.ViewPolicyDetailHolder>  {
    Activity myActivity;
    private LayoutInflater mInflater;
  List<String> data = new ArrayList<>();
    clickResponse clickResponse;
    private SessionManager sessionManager;
    private LocalBroadcastManager mgr;
    public ViewWalletAdapter(Activity myActivity, List<String> data) {
        this.myActivity = myActivity;
        this.data=data;
        mInflater = LayoutInflater.from(myActivity);
        sessionManager= new SessionManager(myActivity);


 }


    @NonNull
    @Override
    public ViewPolicyDetailHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= mInflater.inflate(R.layout.wallet_layout,parent,false);

        return new ViewPolicyDetailHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewPolicyDetailHolder viewHolder, final int position) {
     viewHolder.tvUserName.setText(data.get(position));
     viewHolder.tvPrice.setText(Validations.stringPrice("5"));
     viewHolder.tvStatus.setText("Pending");
    }


    @Override
    public int getItemCount() {


      return   data.size();

}



    public class ViewPolicyDetailHolder extends RecyclerView.ViewHolder {


        TextView tvUserName,tvPrice,tvStatus;
        public ViewPolicyDetailHolder(@NonNull View itemView) {
            super(itemView);
            tvUserName=itemView.findViewById(R.id.tvName);
            tvPrice=itemView.findViewById(R.id.tvPrice);
            tvStatus=itemView.findViewById(R.id.tvStatus);
        }
    }


public interface clickResponse{
        void clickOnCoupon(String code);
}




}

