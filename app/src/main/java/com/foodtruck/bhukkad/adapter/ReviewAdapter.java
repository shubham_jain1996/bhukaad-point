package com.foodtruck.bhukkad.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.model.delivery.MessageResponse;
import com.foodtruck.bhukkad.utils.SessionManager;
import com.foodtruck.bhukkad.utils.Validations;

import java.util.ArrayList;
import java.util.List;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ViewPolicyDetailHolder>  {
    Activity myActivity;
    private LayoutInflater mInflater;
  List<MessageResponse> data = new ArrayList<>();


    public ReviewAdapter(Activity myActivity, List<MessageResponse> data) {
        this.myActivity = myActivity;
        this.data=data;
        mInflater = LayoutInflater.from(myActivity);
 }


    @NonNull
    @Override
    public ViewPolicyDetailHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= mInflater.inflate(R.layout.item1,parent,false);

        return new ViewPolicyDetailHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewPolicyDetailHolder viewHolder, final int position) {
     viewHolder.tvTitle.setText(data.get(position).getMessage());
        //Toast.makeText(myActivity, "tesfsdsfsf", Toast.LENGTH_SHORT).show();

        if (data.get(position).isIselect()){
            viewHolder.mainLayout.setBackground(myActivity.getResources().getDrawable(R.drawable.curve_background_editext_orange));
            viewHolder.tvTitle.setTextColor(myActivity.getResources().getColor(R.color.white));
        }else {
            viewHolder.mainLayout.setBackground(myActivity.getResources().getDrawable(R.drawable.curve_background_editext_black));
            viewHolder.tvTitle.setTextColor(myActivity.getResources().getColor(R.color.black));
        }




    }


    @Override
    public int getItemCount() {


      return   data.size();

}



    public class ViewPolicyDetailHolder extends RecyclerView.ViewHolder {


        TextView tvTitle;
        RelativeLayout mainLayout;
        public ViewPolicyDetailHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle=itemView.findViewById(R.id.tvTitle);
            mainLayout=itemView.findViewById(R.id.mainLayout);

        }
    }


public interface clickResponse{
        void clickOnCoupon(String code);
}




}

