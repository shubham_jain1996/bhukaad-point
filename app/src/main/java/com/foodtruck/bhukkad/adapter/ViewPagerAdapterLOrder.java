package com.foodtruck.bhukkad.adapter;




import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.foodtruck.bhukkad.view.fragment.login.Login;
import com.foodtruck.bhukkad.view.fragment.login.Register;
import com.foodtruck.bhukkad.view.fragment.myorder.HistoryOrder;
import com.foodtruck.bhukkad.view.fragment.myorder.RunningOrder;

import java.util.ArrayList;
import java.util.List;


public  class ViewPagerAdapterLOrder extends FragmentPagerAdapter {
    public List<String> categoryname = new ArrayList<>();

    private FragmentManager mFragmentManager;
    private Fragment mFragmentAtPos0;

    public ViewPagerAdapterLOrder(FragmentManager manager, List<String> categoryname) {
        super(manager);
        this.categoryname= categoryname;
        mFragmentManager = manager;
    }
    @Override
    public int getItemPosition(Object object) {

            return POSITION_NONE;

    }
    @Override
    public Fragment getItem(int position) {
        if (position==0){
            return new RunningOrder();
        }else {
            return new HistoryOrder();
        }


    }

    @Override
    public int getCount() {
        return categoryname.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position==0){
            return "RUNNING";
        }else {
            return "HISTORY";
        }

    }


}
