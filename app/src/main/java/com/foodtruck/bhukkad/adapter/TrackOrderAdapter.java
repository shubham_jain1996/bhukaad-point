package com.foodtruck.bhukkad.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.model.delivery.DeliveryResponse;
import com.foodtruck.bhukkad.model.homeslider.SliderData;
import com.foodtruck.bhukkad.utils.SessionManager;
import com.foodtruck.bhukkad.utils.Validations;

import java.util.ArrayList;
import java.util.List;

public class TrackOrderAdapter extends RecyclerView.Adapter<TrackOrderAdapter.ViewPolicyDetailHolder>  {
    Activity myActivity;
    private LayoutInflater mInflater;
  List<DeliveryResponse> data = new ArrayList<>();

    public TrackOrderAdapter(Activity myActivity, List<DeliveryResponse> data) {
        this.myActivity = myActivity;
        this.data=data;
        mInflater = LayoutInflater.from(myActivity);



 }


    @NonNull
    @Override
    public ViewPolicyDetailHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= mInflater.inflate(R.layout.step_layout,parent,false);

        return new ViewPolicyDetailHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewPolicyDetailHolder viewHolder, final int position) {
            if (data.get(position).getStatus().equalsIgnoreCase("done")){
                viewHolder.imgStatus.setImageDrawable(myActivity.getResources().getDrawable(R.drawable.ic_check));
                viewHolder.lineView.setBackgroundColor(myActivity.getResources().getColor(R.color.green));
            }else if (data.get(position).getStatus().equalsIgnoreCase("pro")){
                viewHolder.imgStatus.setImageDrawable(myActivity.getResources().getDrawable(R.drawable.ic_ethics));
                viewHolder.lineView.setBackgroundColor(myActivity.getResources().getColor(R.color.green));
            }else {
                viewHolder.imgStatus.setImageDrawable(myActivity.getResources().getDrawable(R.drawable.ic_button_dot));
                viewHolder.lineView.setBackgroundColor(myActivity.getResources().getColor(R.color.black_73));
            }
            viewHolder.tvName.setText(data.get(position).getMessage());

            if (position==(data.size()-1)){
                viewHolder.lineView.setVisibility(View.GONE);
            }else {
                viewHolder.lineView.setVisibility(View.VISIBLE);
            }
    }


    @Override
    public int getItemCount() {


      return   data.size();

}



    public class ViewPolicyDetailHolder extends RecyclerView.ViewHolder {


        TextView tvTime,tvName,tvDescription;
        ImageView imgStatus;
        View lineView;
        public ViewPolicyDetailHolder(@NonNull View itemView) {
            super(itemView);
            tvTime=itemView.findViewById(R.id.tvTime);
            tvName=itemView.findViewById(R.id.tvName);
            tvDescription=itemView.findViewById(R.id.tvStatus);
            imgStatus=itemView.findViewById(R.id.img_status);
            lineView=itemView.findViewById(R.id.lineView);
        }
    }


public interface clickResponse{
        void clickOnCoupon(String code);
}




}

