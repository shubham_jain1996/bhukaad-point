package com.foodtruck.bhukkad.adapter;




import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.foodtruck.bhukkad.model.category.CategoryResponse;
import com.foodtruck.bhukkad.view.fragment.OtherTabFragment;
import com.foodtruck.bhukkad.view.fragment.login.Login;
import com.foodtruck.bhukkad.view.fragment.login.Register;

import java.util.ArrayList;
import java.util.List;


public  class ViewPagerAdapterLogin extends FragmentPagerAdapter {
    public List<String> categoryname = new ArrayList<>();

    private FragmentManager mFragmentManager;
    private Fragment mFragmentAtPos0;

    public ViewPagerAdapterLogin(FragmentManager manager, List<String> categoryname) {
        super(manager);
        this.categoryname= categoryname;
        mFragmentManager = manager;
    }
    @Override
    public int getItemPosition(Object object) {

            return POSITION_NONE;

    }
    @Override
    public Fragment getItem(int position) {
        if (position==0){
            return new Login();
        }else {
            return new Register();
        }


    }

    @Override
    public int getCount() {
        return categoryname.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position==0){
            return "LOGIN";
        }else {
            return "REGISTER";
        }

    }


}
