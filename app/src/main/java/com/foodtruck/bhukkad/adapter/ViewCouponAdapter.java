package com.foodtruck.bhukkad.adapter;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.model.cart.CartModel;
import com.foodtruck.bhukkad.model.cart.RemoveCartItemModel;
import com.foodtruck.bhukkad.model.coupon.Offer;
import com.foodtruck.bhukkad.model.menu.MenuResponse;
import com.foodtruck.bhukkad.network.DataService;
import com.foodtruck.bhukkad.utils.Const;
import com.foodtruck.bhukkad.utils.SessionManager;
import com.foodtruck.bhukkad.utils.Validations;
import com.foodtruck.bhukkad.view.CartActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ViewCouponAdapter extends RecyclerView.Adapter<ViewCouponAdapter.ViewPolicyDetailHolder>  {
    Activity myActivity;
    private LayoutInflater mInflater;
  List<Offer> data = new ArrayList<>();
    clickResponse clickResponse;
    private SessionManager sessionManager;
    private LocalBroadcastManager mgr;
    public ViewCouponAdapter(Activity myActivity, List<Offer> data, CartActivity cartActivity) {
        this.myActivity = myActivity;
        this.data=data;
        mInflater = LayoutInflater.from(myActivity);
        sessionManager= new SessionManager(myActivity);
        this.clickResponse=cartActivity;

 }


    @NonNull
    @Override
    public ViewPolicyDetailHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= mInflater.inflate(R.layout.coupon_layout,parent,false);

        return new ViewPolicyDetailHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewPolicyDetailHolder viewHolder, final int position) {
     viewHolder.tvdescription.setText(data.get(position).getName());
     viewHolder.tvname.setText("Max: "+Validations.stringPrice(data.get(position).getMaxDiscount()));
     viewHolder.tvTitle.setText(data.get(position).getDescription());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickResponse.clickOnCoupon(data.get(position).getName());
            }
        });
    }


    @Override
    public int getItemCount() {


      return   data.size();

}



    public class ViewPolicyDetailHolder extends RecyclerView.ViewHolder {


        TextView tvTitle,tvname,tvdescription;
        public ViewPolicyDetailHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle=itemView.findViewById(R.id.tvTitle);
            tvname=itemView.findViewById(R.id.tvname);
            tvdescription=itemView.findViewById(R.id.tvdescription);
        }
    }


public interface clickResponse{
        void clickOnCoupon(String code);
}




}

