package com.foodtruck.bhukkad.view.address;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.view.View;
import android.webkit.PermissionRequest;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.foodtruck.bhukkad.BuildConfig;
import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.adapter.ViewCouponAdapter;
import com.foodtruck.bhukkad.helper.Constants;
import com.foodtruck.bhukkad.helper.DataLoadingInterfaceHome;
import com.foodtruck.bhukkad.helper.SetDataInBackground;
import com.foodtruck.bhukkad.helper.Utility;
import com.foodtruck.bhukkad.model.wallet.WalletData;
import com.foodtruck.bhukkad.model.wallet.WalletResponse;
import com.foodtruck.bhukkad.network.DataService;
import com.foodtruck.bhukkad.utils.SessionManager;
import com.foodtruck.bhukkad.view.CartActivity;
import com.foodtruck.bhukkad.view.HomeActivity;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.navigation.NavigationView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.single.PermissionListener;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddAddres extends AppCompatActivity implements OnMapReadyCallback,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener,
        GoogleMap.OnMarkerClickListener, DataLoadingInterfaceHome {
    private GoogleMap mMap;
    private Map<String, Marker> markers;
    Marker marker, originMarker, destinationMarker;
    // bunch of location related apis
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;
    protected static final int REQUEST_CHECK_SETTINGS = 100;
    private LocationSettingsRequest.Builder builder;
    private double latitude;
    private double longitude;
    LatLng currentLocation;
    int addressId;

    private int currentLocationPicker = Constants.LOCATION_PICKER_PICKUP;
    private View bottomSheetView;
    BottomSheetDialog bottomSheetDialog;
    BottomSheetBehavior bottomSheetBehavior;
    String CurrentAddress;
    SessionManager sessionManager;


    @BindView(R.id.txt_location)
    TextView txtLocation;

    @BindView(R.id.btDetails)
    TextView btDetails;

    @BindView(R.id.tvChange)
    TextView tvChange;
    private String postal_code;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_addres);
        ButterKnife.bind(this);
        if (getIntent()!=null){
            addressId=getIntent().getIntExtra("id",0);
        }

        sessionManager = new SessionManager(this);
        this.markers = new HashMap<>();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        init();
    }
    private void init() {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();



        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location is received
                mCurrentLocation = locationResult.getLastLocation();
                getCurrentLocation();
            }
        };
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setSmallestDisplacement(50);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        // LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();



        startLocationButtonClick();


    }

    public void startLocationButtonClick() {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        startLocationUpdates();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if (response.isPermanentlyDenied()) {
                            openSettings();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(com.karumi.dexter.listener.PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }

                }).check();
    }

    private void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        //  Log.i(TAG, "All location settings are satisfied.");
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());
                        getCurrentLocation();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                                try {

                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(AddAddres.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    //  Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                //  Log.e(TAG, errorMessage);

                                Toast.makeText(AddAddres.this, errorMessage, Toast.LENGTH_LONG).show();
                        }

                        getCurrentLocation();
                    }
                });
    }

    private void getCurrentLocation() {
        if (mCurrentLocation != null) {
            mMap.clear();
            originMarker = null;
            destinationMarker = null;
            marker = null;
            longitude = mCurrentLocation.getLongitude();
            latitude = mCurrentLocation.getLatitude();
           // getBikeAvailability();
            moveMap();
        }

    }

    private void moveMap() {
        LatLng latLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(18));
        mMap.getUiSettings().setZoomControlsEnabled(false);

        currentLocation=latLng;
      //  LatLng coordinate = new LatLng(latitude, longitude);
        new SetDataInBackground(AddAddres.this, AddAddres.this).execute(latLng);
        if (getIntent()!=null && getIntent().getStringExtra("lat")!=null && getIntent().getStringExtra("long")!=null ){
            LatLng latLng1 = new LatLng(Double.parseDouble(getIntent().getStringExtra("lat")),Double.parseDouble(getIntent().getStringExtra("long")));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(18));
            mMap.getUiSettings().setZoomControlsEnabled(false);

            currentLocation=latLng;
            //  LatLng coordinate = new LatLng(latitude, longitude);
            new SetDataInBackground(AddAddres.this, AddAddres.this).execute(latLng1);
        }
        /*if (currentLocationPicker == Constants.LOCATION_PICKER_PICKUP) {
            String address = Utility.getCompleteAddressString(latLng.latitude, latLng.longitude, this);
          //  tvPickLocation.setText(address);
        } else {
            String address = Utility.getCompleteAddressString(latLng.latitude, latLng.longitude, this);
         //   tvDropLocation.setText(address);
        }*/


    }

    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package",
                BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void openAutocompleteActivity(int REQUEST_CODE_AUTOCOMPLETE) {
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME,Place.Field.LAT_LNG,Place.Field.ADDRESS);
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields)
                .setCountry("IN")
                .build(this);
        startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.ORIGIN_REQUEST_CODE_AUTOCOMPLETE) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                if (place != null) {
                    CurrentAddress=place.getAddress();
                    txtLocation.setText(CurrentAddress);
                    latitude=place.getLatLng().latitude;
                    longitude=place.getLatLng().longitude;
                    LatLng coordinate = new LatLng(latitude, longitude);
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(coordinate));
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(18));
                    mMap.getUiSettings().setZoomControlsEnabled(false);
                 //   new SetDataInBackground(AddAddres.this, AddAddres.this).execute(coordinate);
                }

            }


            }else if (requestCode==REQUEST_CHECK_SETTINGS){
                init();
            }
        }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }
    public void stopLocationUpdates() {
        mFusedLocationClient
                .removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                    }
                });
    }
    @Override
    public void onMapLongClick(LatLng latLng) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        latitude = marker.getPosition().latitude;
        longitude = marker.getPosition().longitude;
        LatLng coordinate = new LatLng(latitude, longitude);
        new SetDataInBackground(AddAddres.this, AddAddres.this).execute(coordinate);
        moveMap();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMapLongClickListener(this);
        googleMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                CameraPosition cameraPosition = googleMap.getCameraPosition();
                latitude = cameraPosition.target.latitude;
                longitude = cameraPosition.target.longitude;
                LatLng coordinate = new LatLng(latitude, longitude);
                new SetDataInBackground(AddAddres.this, AddAddres.this).execute(coordinate);
                // setAddressInTv(latitude,longitude);

            }
        });
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    private void initAddDeatailsSheet() {
        bottomSheetView = getLayoutInflater().inflate(R.layout.second_layout , null);
        bottomSheetDialog = new BottomSheetDialog(AddAddres.this);
        bottomSheetDialog.setContentView(bottomSheetView);
        TextView txtLocation1  = bottomSheetView.findViewById(R.id.txt_location);
        TextView tvChange = bottomSheetView.findViewById(R.id.tvChange);
        TextView btDetails = bottomSheetView.findViewById(R.id.btDetails);
        EditText etCompleteAddress = bottomSheetView.findViewById(R.id.etCompleteAddress);
        EditText etFloor = bottomSheetView.findViewById(R.id.etFloor);
        EditText etReach = bottomSheetView.findViewById(R.id.etReach);
        EditText etName = bottomSheetView.findViewById(R.id.etName);
        EditText etPhone = bottomSheetView.findViewById(R.id.etPhone);
        EditText etPincode = bottomSheetView.findViewById(R.id.etPinCode);
        postal_code= etPincode.getText().toString().trim();
        txtLocation1.setText(CurrentAddress);
        btDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etCompleteAddress.getText().toString().trim().isEmpty()) {
                    Toast.makeText(AddAddres.this, "Please enter Address", Toast.LENGTH_SHORT).show();
                } else if (etName.getText().toString().trim().isEmpty()) {
                    Toast.makeText(AddAddres.this, "Please enter Name", Toast.LENGTH_SHORT).show();
                } else if (etPhone.getText().toString().trim().isEmpty()) {
                    Toast.makeText(AddAddres.this, "Please enter phone no.", Toast.LENGTH_SHORT).show();
                } else {
                    if ( addressId <= 0) {
                        callApiToSaveAddress(etName.getText().toString().trim(),
                                etPhone.getText().toString().trim(), etCompleteAddress.getText().toString().trim(), etFloor.getText().toString(), etReach.getText().toString());
                    }else {
                        callApiToEditAddress(etName.getText().toString().trim(),
                                etPhone.getText().toString().trim(), etCompleteAddress.getText().toString().trim(), etFloor.getText().toString(), etReach.getText().toString());

                    }
                }
            }
        });
        tvChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Places.isInitialized()) {
                    Places.initialize(getApplicationContext(), getString(R.string.api_key), Locale.US);
                }
              openAutocompleteActivity(1);
            }
        });
     //   txtLocation.setText(CurrentAddress);
        bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
        bottomSheetBehavior.setBottomSheetCallback(bottomSheetCallback);

        bottomSheetDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
            }
        });

        bottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

            }
        });
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        bottomSheetDialog.show();

    }

    private void callApiToSaveAddress(String name, String phone, String address, String floor, String reach) {
        DataService dataService = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<WalletResponse>() {
            @Override
            public void onDataSuccess(WalletResponse response, int code) {
                if (response!=null && response.getSuccess() ){
                    Toast.makeText(AddAddres.this,"Address Added", Toast.LENGTH_SHORT).show();
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    onBackPressed();
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.callApiToSetAddress("Token "+sessionManager.getUser().getData().getAuthToken(),name,phone,CurrentAddress,address,floor, latitude,
                longitude,postal_code);


    }
    private void callApiToEditAddress(String name, String phone, String address, String floor, String reach) {
        DataService dataService = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<WalletResponse>() {
            @Override
            public void onDataSuccess(WalletResponse response, int code) {
                if (response!=null && response.getSuccess() ){
                    Toast.makeText(AddAddres.this,"Update Added", Toast.LENGTH_SHORT).show();
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    onBackPressed();
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.callApiToUpdateAddress("Token "+sessionManager.getUser().getData().getAuthToken(),name,phone,CurrentAddress,address,floor, latitude,
                longitude,postal_code,addressId);


    }
    @Override
    public void preExecute() {

    }

    @Override
    public void postExecute(String homeClasses) {
    CurrentAddress=homeClasses;
    txtLocation.setText(homeClasses);
    }
    BottomSheetBehavior.BottomSheetCallback bottomSheetCallback =
            new BottomSheetBehavior.BottomSheetCallback(){
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    switch (newState){
                        case BottomSheetBehavior.STATE_COLLAPSED:

                            break;
                        case BottomSheetBehavior.STATE_DRAGGING:

                            break;
                        case BottomSheetBehavior.STATE_EXPANDED:

                            break;
                        case BottomSheetBehavior.STATE_HIDDEN:

                            bottomSheetDialog.dismiss();
                            break;
                        case BottomSheetBehavior.STATE_SETTLING:

                            break;
                        default:

                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            };


    @OnClick(R.id.btDetails)
    void clickConfirmButton(){
        initAddDeatailsSheet();
    }

    @OnClick(R.id.tvChange)
    void clickSearchButton(){
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), getString(R.string.api_key), Locale.US);
        }
        openAutocompleteActivity(1);
    }
}