package com.foodtruck.bhukkad.view.fragment.login;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.model.register.FirebaseKeys;
import com.foodtruck.bhukkad.utils.SessionManager;
import com.foodtruck.bhukkad.utils.Validations;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Register extends Fragment {

@BindView(R.id.tv_username)
    EditText tvUserName;
@BindView(R.id.tv_name)
EditText tvName;

@BindView(R.id.tv_email)
EditText tvEmail;
@BindView(R.id.tv_password)
EditText tvPassword;
@BindView(R.id.tv_mobile)
EditText tvMovile;

@BindView(R.id.radio_g)
    RadioGroup gender;

@BindView(R.id.tv_Referral)
EditText tvReferral;

String genderString="M";

SessionManager sessionManager;
FirebaseKeys firebaseKeys;
String token="";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View view =inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this,view);
        sessionManager= new SessionManager(getActivity());
        gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.male:
                        genderString="M";
                        break;
                    case R.id.female:
                        genderString="F";
                        break;
                    case R.id.other:
                        genderString="O";
                        break;
                }
            }

        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (sessionManager.getGoogleUser()!=null){
            firebaseKeys = sessionManager.getGoogleUser();
            if (firebaseKeys!=null){
                tvEmail.setText(firebaseKeys.getEmail());
                tvName.setText(firebaseKeys.getName());
                token=firebaseKeys.getToken();
            }
           // sessionManager.saveGoogleUser(null);
        }
    }

    @OnClick(R.id.btn_redeem)
    void clickSubmitButton(){
        if (checkValidations()){
            Intent i = new Intent(getActivity(),PhoneVerification.class);
            i.putExtra("mobile", tvMovile.getText().toString().trim());
            i.putExtra("name", tvName.getText().toString().trim());
            i.putExtra("userName", tvUserName.getText().toString().trim());
            i.putExtra("gender",genderString);
            i.putExtra("email", tvEmail.getText().toString().trim());
            i.putExtra("password",tvPassword.getText().toString().trim());
            i.putExtra("referral",tvReferral.getText().toString().trim());
            i.putExtra("token",token);
            startActivity(i);
        }
    }


    private boolean checkValidations() {
        boolean isvalid=true;
        if (Validations.isEmpty(tvUserName.getText().toString().trim())){
            isvalid=false;
            Toast.makeText(getActivity(), "Please enter User Name", Toast.LENGTH_SHORT).show();
        } else if (Validations.isEmpty(tvName.getText().toString().trim())){
            isvalid=false;
            Toast.makeText(getActivity(), "Please enter name", Toast.LENGTH_SHORT).show();
        }else if (!Validations.isValidEmail(tvEmail.getText().toString().trim())){
            isvalid=false;
            Toast.makeText(getActivity(), "Please enter Valid Email", Toast.LENGTH_SHORT).show();
        }else if (!Validations.isValidMobile(tvMovile.getText().toString().trim())){
            isvalid=false;
            Toast.makeText(getActivity(), "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
        }else if (Validations.isEmpty(tvPassword.getText().toString().trim())){
            isvalid=false;
            Toast.makeText(getActivity(), "Please enter a valid password", Toast.LENGTH_SHORT).show();
        }

        return  isvalid;
    }
}