package com.foodtruck.bhukkad.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.adapter.CommonAdapter;
import com.foodtruck.bhukkad.model.menu.MenuResponse;
import com.foodtruck.bhukkad.network.DataService;
import com.foodtruck.bhukkad.utils.Const;
import com.foodtruck.bhukkad.utils.SessionManager;
import com.foodtruck.bhukkad.view.HomeActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.foodtruck.bhukkad.MainActivity.latitude;
import static com.foodtruck.bhukkad.MainActivity.longitude;


public class    OtherTabFragment extends Fragment implements  CommonAdapter.ClickMoreButton {

    private String categoryName;
    ///private List<HomeClass> topStoriesList=new ArrayList<>();
    private int position1=0;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.refreshPage)
    SwipeRefreshLayout refreshPage;
    @BindView(R.id.lout_no_data)
    LinearLayout loutNoData;



  /*  @BindView(R.id.progress_view)
    CircularProgressView prograssBar;
*/
    private LinearLayoutManager layoutManager;
    private CommonAdapter madapter;
    private int pageNO=1;
    private  int id;
    private boolean _areLecturesLoaded=false;
    SessionManager sessionManager ;
    List<List<MenuResponse.Datum>> data = new ArrayList<>();
   /* HomeClass homeModel;
    List<HomeClass> tempClass=new ArrayList<>();*/



    public static OtherTabFragment newInstance(String categoryName, int id) {
        OtherTabFragment fragment = new OtherTabFragment();

        Bundle args = new Bundle();
        args.putString("catName",categoryName);
        args.putInt("id",id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getActivity());

    }
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view!=null){
            return view;
        }
        Bundle bundle=getArguments();
        categoryName = bundle.getString("catName","");
        id= bundle.getInt("id");
        view = inflater.inflate(R.layout.fragment_temp, container, false);
        ButterKnife.bind(this,view);
        setUpSwipeRefresh();
        setUpRecyclerView();
     //   prograssBar.setVisibility(View.VISIBLE);

     //   getTopStoriesData(String.valueOf(id));
        getMenuData(String.valueOf(id));
        return view;
    }

    private void setUpSwipeRefresh() {
        refreshPage.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageNO=1;
             //   prograssBar.setVisibility(View.VISIBLE);
               // getTopStoriesData(String.valueOf(id));
                if (refreshPage.isRefreshing()){
                    refreshPage.setRefreshing(false);
                }
            }
        });

    }

    private void setUpRecyclerView() {
       // recyclerView.setVisibility(View.INVISIBLE);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        RecyclerView.LayoutManager verticalLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(verticalLayoutManager);

    }

   public void getMenuData(String Id) {
        DataService dataService = new DataService(getActivity());
        dataService.setOnServerListener(new DataService.ServerResponseListner<MenuResponse>() {
            @Override
            public void onDataSuccess(MenuResponse response, int code) {
                if (response != null && response.getSuccess() && response.getData().size()>0) {
                    data.addAll(response.getData());
                    madapter = new CommonAdapter(getActivity(),data);
                    recyclerView.setAdapter(madapter);
                    loutNoData.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);

                   // madapter.notifyDataSetChanged();
                    //new ConvertHtmlToString(com.multimedia.adomonline.view.mainActivity.fragment.OtherTabFragment.this).execute(response);

                }else{
                    loutNoData.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onDataFailiure() {
            }
        });
      // dataService.getMenuData(Id, String.valueOf(pageNO));
       if (sessionManager.getUser()!=null) {
           dataService.getMenuData("Token " + sessionManager.getUser().getData().getAuthToken(), Id, String.valueOf(latitude), String.valueOf(longitude));

       }else{
           dataService.getMenuData("", Id, String.valueOf(latitude), String.valueOf(longitude));
       }
    }

  /*  @Override

    public void preExecute() {
        prograssBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void postExecute(List<HomeClass> homeClasses) {
        if (pageNO==1){
            topStoriesList.clear();
        }
        topStoriesList.addAll(homeClasses);
        madapter.notifyDataSetChanged();
        prograssBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }
*/
    @Override
    public void clickMore() {
        pageNO++;
    //    prograssBar.setVisibility(View.VISIBLE);
     //   getTopStoriesData(String.valueOf(id));
    }

}

