package com.foodtruck.bhukkad.view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.adapter.SlidingImageAdapter;
import com.foodtruck.bhukkad.adapter.ViewCouponAdapter;
import com.foodtruck.bhukkad.adapter.ViewPagerAdapter;
import com.foodtruck.bhukkad.adapter.ViewReviewAdapter;
import com.foodtruck.bhukkad.customdailog.LbmDialog;
import com.foodtruck.bhukkad.helper.Constants;
import com.foodtruck.bhukkad.helper.Utility;
import com.foodtruck.bhukkad.model.category.CategoryResponse;
import com.foodtruck.bhukkad.model.homeslider.SliderData;
import com.foodtruck.bhukkad.model.homeslider.SliderImagesResponse;
import com.foodtruck.bhukkad.model.menu.MenuResponse;
import com.foodtruck.bhukkad.model.review.ReviewData;
import com.foodtruck.bhukkad.model.review.ReviewResponse;
import com.foodtruck.bhukkad.model.viewcart.CartViewResponse;
import com.foodtruck.bhukkad.model.viewcart.Data;
import com.foodtruck.bhukkad.network.DataService;
import com.foodtruck.bhukkad.utils.Const;
import com.foodtruck.bhukkad.utils.SessionManager;
import com.foodtruck.bhukkad.utils.Validations;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.foodtruck.bhukkad.MainActivity.latitude;
import static com.foodtruck.bhukkad.MainActivity.longitude;

public class HomeActivity extends AppCompatActivity {
/*@BindView(R.id.toolbar)
     toolbar;*/

@BindView(R.id.tabs)
    TabLayout tablayout;
@BindView(R.id.viewpager)
    ViewPager viewPagerImage;

@BindView(R.id.viewpagerFragment)
ViewPager viewPagerFragment;

@BindView(R.id.drawer_layout)
DrawerLayout drawerLayout;

@BindView(R.id.coordinateLayout)
CoordinatorLayout mainlayout;

@BindView(R.id.tvProfile)
TextView tvProfile;

@BindView(R.id.tvorder)
TextView tvOrder;

@BindView(R.id.tvwallet)
TextView tvWallet;

@BindView(R.id.tvRefer)
TextView tvRefer;

@BindView(R.id.tvLogout)
TextView tvLogout;

@BindView(R.id.tvProfileLine)
View tvProfileLine;
@BindView(R.id.tvOrderLine)
View tvOrderLine;
@BindView(R.id.tvWalletLine)
View tvWalletLine;
@BindView(R.id.tvReferLine)
View tvReferLine;
@BindView(R.id.tvLogotLine)
View tvLogotLine;

@BindView(R.id.name)
TextView name;

    @BindView(R.id.tv_email)
    TextView tvEmail;


    @BindView(R.id.imageView)
    ImageView imageView;

    @BindView(R.id.btLogin)
    Button btLogin;

    @BindView(R.id.ic_cart)
    RelativeLayout ic_cart;

    @BindView(R.id.ic_cart_black)
    RelativeLayout ic_cart_black;

    @BindView(R.id.tvLocation)
            TextView tvLocation;

    @BindView(R.id.tvLocation_black)
    TextView tvLocationBlack;

    TextView tvCount;
    TextView tvCountBlack;
    private DrawerLayout drawer;
    private NavigationView navigationView;

    RecyclerView rvReview;
    ViewReviewAdapter viewReviewAdapter;

    List<ReviewData> lstReview = new ArrayList<>();

    private ArrayList<String> permissiontoRequest;
    private ArrayList<String> permissiontoReject=new ArrayList<>();
    private ArrayList<String> permissions=new ArrayList<>();
    private static final int ALL_PERMISSION_RESULT = 1011;
    LocationManager locationManager;
    boolean GpsStatus ;
    List<CategoryResponse.Datum> categoryName = new ArrayList<>();
    private ViewPagerAdapter adapter;

    private LocalBroadcastManager mgr;
    SessionManager sessionManager;
    SlidingImageAdapter slidingImageAdapter;
    List<SliderData.Image> imageData = new ArrayList<>();
    @BindView(R.id.indicator)
    CirclePageIndicator indicator;
    private static int currentPage = 0;
    private Timer timer;
    private boolean doubleBackToExitPressedOnce=false;
    ProgressDialog progressDialog;
    BottomSheetDialog bottomSheetDialog;
    BottomSheetBehavior bottomSheetBehavior;
    private View bottomSheetView;

    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;

    @BindView(R.id.toolbar_benefit)
    LinearLayout commonHeaderLayout;

    @BindView(R.id.toolbar)
    RelativeLayout pagerLayout;


    private LbmDialog helpDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        helpDialog= new LbmDialog(this);
        helpDialog.setCancelable(false);
        helpDialog.show();
     /*   progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait..");
        progressDialog.setCancelable(false);
        progressDialog.show();*/
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        tvCount = ic_cart.findViewById(R.id.count);
        tvCountBlack = ic_cart_black.findViewById(R.id.count_black);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        setDataInView(navigationView);
        tvCount.setVisibility(View.INVISIBLE);
        tvCountBlack.setVisibility(View.INVISIBLE);
        GpsStatus();
        if (!GpsStatus){
            Intent in = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(in);
        }else {
            permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
            permissiontoRequest=permissiontoRequest(permissions);
            if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.M)
            {
                if (permissiontoRequest.size()>0){
                    requestPermissions(permissiontoRequest.toArray(new String[permissiontoRequest.size()]),ALL_PERMISSION_RESULT);
                }
            }
        /*    googleApiClient =new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();*/
        }
        setupAppBarlayout();
      //  startLocationUpdates();
        setupSlideImageAdapter();
        setupViewPager();

        callBroadcast();

        getSliderImages();
        if (latitude==0.0) {
            getLocation();
        }else {
            String address = Utility.getCompleteAddressString(latitude, longitude, HomeActivity.this);
            tvLocation.setText(address);
            tvLocationBlack.setText(address);
            callApiToGetCategoryData();
        }
        if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
           name.setText(sessionManager.getUser().getData().getUsername());
           tvEmail.setText(sessionManager.getUser().getData().getEmail());
           name.setVisibility(View.VISIBLE);
           tvEmail.setVisibility(View.VISIBLE);
           btLogin.setVisibility(View.GONE);
        }else {
            name.setVisibility(View.GONE);
            tvEmail.setVisibility(View.GONE);
            btLogin.setVisibility(View.VISIBLE);

        }
        if (sessionManager.getProfileImage(Constants.PROFILEIMAGE)!= null && !sessionManager.getProfileImage(Constants.PROFILEIMAGE).isEmpty()){
            Picasso.get().load(sessionManager.getProfileImage(Constants.PROFILEIMAGE)).into(imageView);
        }

        helpDialog.dismiss();
    }

    private void setupAppBarlayout() {
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                if (Math.abs(verticalOffset)-appBarLayout.getTotalScrollRange() == 0){
                    commonHeaderLayout.setAlpha(1);
                    commonHeaderLayout.setVisibility(View.VISIBLE);
                }else {
                    commonHeaderLayout.setAlpha(0);
                    commonHeaderLayout.setVisibility(View.GONE);
                }

                    //Expanded
                Log.d("Shubham", "123: "+Math.abs(verticalOffset));
                Log.d("Shubham", "3434: "+appBarLayout.getTotalScrollRange());




            }
        });


    }

    private void hideandshowLayout() {
        if (!sessionManager.getBooleanValue(Const.IS_LOGIN)) {
           tvProfile.setVisibility(View.GONE);
           tvLogout.setVisibility(View.GONE);
           tvOrder.setVisibility(View.GONE);
           tvRefer.setVisibility(View.GONE);
           tvWallet.setVisibility(View.GONE);
            tvProfileLine.setVisibility(View.GONE);
            tvLogotLine.setVisibility(View.GONE);
            tvOrderLine.setVisibility(View.GONE);
            tvReferLine.setVisibility(View.GONE);
            tvWalletLine.setVisibility(View.GONE);
        }else {
            tvProfile.setVisibility(View.VISIBLE);
            tvLogout.setVisibility(View.VISIBLE);
            tvOrder.setVisibility(View.VISIBLE);
            tvRefer.setVisibility(View.VISIBLE);
            tvWallet.setVisibility(View.VISIBLE);
            tvProfileLine.setVisibility(View.VISIBLE);
            tvLogotLine.setVisibility(View.VISIBLE);
            tvOrderLine.setVisibility(View.VISIBLE);
            tvReferLine.setVisibility(View.VISIBLE);
            tvWalletLine.setVisibility(View.VISIBLE);
        }
    }

    private void getSliderImages() {
        DataService dataService = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<SliderImagesResponse>() {
            @Override
            public void onDataSuccess(SliderImagesResponse response, int code) {
                if (response!=null && response.getSuccess() && response.getData()!=null && response.getData().getImages().size()>0){
                    imageData.addAll(response.getData().getImages());
                    slidingImageAdapter.notifyDataSetChanged();
                }else {
                    Toast.makeText(HomeActivity.this, "No Data", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.getSliderImages();


    }

    private void setupSlideImageAdapter() {
    slidingImageAdapter = new SlidingImageAdapter(this,imageData);
        viewPagerImage.setAdapter(slidingImageAdapter);
        indicator.setViewPager(viewPagerImage);
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {
            }
        });
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                viewPagerImage.post(new Runnable(){

                    @Override
                    public void run() {
                        if (currentPage==imageData.size()){
                            currentPage=0;
                        }
                        viewPagerImage.setCurrentItem(currentPage++, true);
                    }
                });
            }
        };
        timer = new Timer();
        timer.schedule(timerTask, 3000, 3000);

    }

    private void setupViewPager() {

        tablayout.setupWithViewPager(viewPagerFragment);
        tablayout.setTabMode(TabLayout.MODE_FIXED);
        adapter = new ViewPagerAdapter(getSupportFragmentManager(),categoryName);
        viewPagerFragment.setAdapter(adapter);
    }
    private void callBroadcast() {
        Intent intent = new Intent("attend");
        mgr = LocalBroadcastManager.getInstance(this);
        mgr.sendBroadcast(intent);
    }





    private void callApiToGetCategoryData() {
        if (helpDialog!=null){
          //  helpDialog.dismiss();
        }
        DataService dataService = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<CategoryResponse>() {
            @Override
            public void onDataSuccess(CategoryResponse response, int code) {
                if (response!=null && code==201){
                    if (response.getSuccess() && response.getData().size()>0){
                        categoryName.clear();
                        categoryName.addAll(response.getData());
                       // setCustomLayout(response.getData());

                    }
                    viewPagerFragment.invalidate();
                    adapter.notifyDataSetChanged();
                }


            }

            @Override
            public void onDataFailiure() {

            }
        });

        dataService.getCategory();


    }

    private void setCustomLayout(List<CategoryResponse.Datum> data) {
        for (int i = 0; i < data.size(); i++) {
            TextView customTab = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab_layout, null);//get custom view
            customTab.setText(data.get(i).getName());//set text over view
           // customTab.setCompoundDrawablesWithIntrinsicBounds(0, tabIcons[i], 0, 0);//set icon above the view
            TabLayout.Tab tab = tablayout.getTabAt(i);//get tab via position
            if (tab != null)
                tab.setCustomView(customTab);
        }
    }

    private void callApiToGetCartData() {
        DataService dataService = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<CartViewResponse>() {
            @Override
            public void onDataSuccess(CartViewResponse response, int code) {
                if (response!=null && response.getSuccess() && response.getData()!=null){
                    if (response.getData().getCartDetail().size()>0){
                        tvCount.setVisibility(View.VISIBLE);
                        tvCountBlack.setVisibility(View.VISIBLE);
                    tvCount.setText(""+response.getData().getCartDetail().size());
                    tvCountBlack.setText(""+response.getData().getCartDetail().size());
                    hideandshowLayout();
                    }else {
                        tvCount.setVisibility(View.INVISIBLE);
                        tvCountBlack.setVisibility(View.INVISIBLE);
                    }
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });
        if (sessionManager.getUser()!=null) {
            dataService.getCartData(String.valueOf(latitude), String.valueOf(longitude), "Token " + sessionManager.getUser().getData().getAuthToken());
        }else {
            dataService.getCartData(String.valueOf(latitude), String.valueOf(longitude), "");

        }

    }

  /*  @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }*/
    private void GpsStatus() {
        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }


    private ArrayList<String> permissiontoRequest(ArrayList<String> wantedPermission){
        ArrayList<String> result=new ArrayList<>();
        for (String perm : wantedPermission){
            if (!hasPermission(perm)){
                result.add(perm);
            }
        }
        return result;
    }

    private boolean hasPermission(String permmission) {
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.M)
        {
            return checkSelfPermission(permmission)== PackageManager.PERMISSION_GRANTED;
        }
        return true;
    }

    public void onStart(){
        super.onStart();
       /* if (googleApiClient!=null){
            googleApiClient.connect();
        }
*/
        mgr = LocalBroadcastManager.getInstance(this);
        mgr.registerReceiver(mMessageReceiver1, new IntentFilter("attend"));
    }

    public void onResume(){
        super.onResume();
        callApiToGetCartData();
    }

    public void onPause(){
        super.onPause();
        if (helpDialog!=null && helpDialog.isShowing()){
           // helpDialog.dismiss();
        }
       /* if (googleApiClient!=null&&googleApiClient.isConnected()){
//            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, (com.google.android.gms.location.LocationListener) this);
            googleApiClient.disconnect();
        }*/
    }

  /*  private boolean checkPlayServices() {
        GoogleApiAvailability googleApiAvailability=GoogleApiAvailability.getInstance();
        int ResultCode=googleApiAvailability.isGooglePlayServicesAvailable(this);
        if (ResultCode!= ConnectionResult.SUCCESS){
            if (googleApiAvailability.isUserResolvableError(ResultCode)){
                googleApiAvailability.getErrorDialog(this,ResultCode,PLAY_SERVICE_RESOLUTION_REQUEST);

            }else {
              finish();
            }
            return false;
        }
        return true;
    }*/

  /*  @Override
    public void onLocationChanged(Location location) {
        if (location!=null){

            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                //  currentlocation = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();

                // address.setText(currentlocation);

            } catch (IOException e) {
                e.printStackTrace();
            }
//            tvLocation.setText("Latitude:-"+location.getLatitude()+"Longitude:-"+location.getLongitude());
        }
    }
*/


 /*   public void onProviderDisabled(String s) {
        android.app.AlertDialog.Builder dialog=new android.app.AlertDialog.Builder(this);
        dialog.setTitle("Alert Location");
        dialog.setMessage("Please open your location");
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        });
        dialog.show();

    }*/

  /*  @Override
    public void onConnected(@Nullable Bundle bundle) {
        progressDialog.dismiss();
        if (ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION)!=PackageManager.PERMISSION_GRANTED&&ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION)!=PackageManager.PERMISSION_GRANTED){
            return;
        }
        locationn=LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (locationn!=null){
            latitude = locationn.getLatitude();
            longitude=locationn.getLongitude();
            String address = Utility.getCompleteAddressString(latitude, longitude, this);
            tvLocation.setText(address);
            callApiToGetCategoryData();
            hideandshowLayout();
         //   name.setText(""+latitude+"~~~"+longitude);
           // viewModel.callApiToGetMenuData(locationn);


        }
        startLocationUpdates();
    }
*/




   /* private void startLocationUpdates() {
        locationRequest=new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(fastest);

        if (ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION)!=PackageManager.PERMISSION_GRANTED&&ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION)!=PackageManager.PERMISSION_GRANTED){
            Toast.makeText(this, "You need to enable Permission to display location", Toast.LENGTH_SHORT).show();
        }

    }

*/

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case ALL_PERMISSION_RESULT:
                for (String perm : permissiontoRequest){
                    if (!hasPermission(perm)){
                        permissiontoReject.add(perm);
                    }
                }
                if (permissiontoReject.size()>0){
                    if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
                        if (shouldShowRequestPermissionRationale(permissiontoReject.get(0))){
                            new AlertDialog.Builder(this).
                                    setMessage("These Permission are mandatory to get location,you need to allow then.").
                                    setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
                                                requestPermissions(permissiontoReject.toArray(new String[permissiontoReject.size()]),ALL_PERMISSION_RESULT);
                                            }
                                        }
                                    }).
                                    setNegativeButton("Cancle",null).create().show();
                            return;
                        }
                    }
                }else {
                  /*  if (googleApiClient!=null){
                        googleApiClient.connect();
                    }*/
                }
                break;
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        if (mMessageReceiver1!=null){
            mgr.unregisterReceiver(mMessageReceiver1);
        }

    }

    private BroadcastReceiver mMessageReceiver1 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras()!=null && intent.getExtras().getString("session")!=null){

                switch (intent.getExtras().getString("session")){
                    case "open":
                        openBottomSheet();
                        break;
                    case "openCart":

                        break;
                    case "cart_count":
                        String count = intent.getExtras().getString("cartValue");
                        tvCount.setText(count);
                        tvCountBlack.setText(count);
                        if (Integer.parseInt(count)>0){
                          tvCount.setVisibility(View.VISIBLE);
                          tvCountBlack.setVisibility(View.VISIBLE);
                        }else {
                            tvCount.setVisibility(View.INVISIBLE);
                            tvCountBlack.setVisibility(View.INVISIBLE);
                        }
                        break;
                    case "details":
                        String title=intent.getExtras().getString("title");
                                String description=intent.getExtras().getString("description");
                            String image=intent.getExtras().getString("image");
                            String id=intent.getExtras().getString("id");
                            int price=intent.getExtras().getInt("price");
                            String rating=intent.getExtras().getString("rating");
                        bottomSheetDialog(title,description
                               ,image,id,price,rating
                               );
                        break;


                }
            }

        }
    };

    private void openBottomSheet() {
        Intent i = new Intent(this,LoginActivity.class);
        startActivity(i);
       /* LoginFragment bottomSheet = new LoginFragment();
        bottomSheet.show(getSupportFragmentManager(),
                "LoginSheet");*/

    }
    @OnClick(R.id.back_btn)
    void clickToOpenDrawer(){
        clickDrawr();

    }

    @OnClick(R.id.back_btn_black)
    void clickToOpenDrawerBlack(){
        clickDrawr();

    }

    public void clickDrawr() {
        drawerLayout.openDrawer(GravityCompat.START);
      //  drawerLayout.setRadius(GravityCompat.START,60);
    /*    drawerLayout.useCustomBehavior(GravityCompat.START);
        drawerLayout.setRadius(Gravity.START,25);*/
    }
    private void setDataInView(NavigationView navigationView) {
        if (navigationView == null) {
            return;
        }
        View headerView = navigationView.getHeaderView(0);
        CircleImageView sideMenuProfileImage = navigationView.findViewById(R.id.imageView);
        TextView sideMenuName = navigationView.findViewById(R.id.name);
    }
    @OnClick(R.id.ic_cart)
    void clickCartCode(){
        if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
            Intent i = new Intent(this,CartActivity.class);
            startActivity(i);
        }else {
            openBottomSheet();

        }

    }

    @OnClick(R.id.ic_cart_black)
    void clickCartCodeBlack(){
        if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
            Intent i = new Intent(this,CartActivity.class);
            startActivity(i);
        }else {
            openBottomSheet();

        }

    }

    private void showLogoutDialog() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.logout));
        builder.setMessage(getResources().getString(R.string.are_you_sure_you_want_to_logout));
        builder.setPositiveButton(getResources().getString(R.string.logout), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                logoutUser();

            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        android.app.AlertDialog alert = builder.create();
        alert.setCancelable(false);
        alert.show();
        alert.getButton(alert.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.green));
        alert.getButton(alert.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.black));


    }
    private void logoutUser() {
        drawerLayout.closeDrawer(Gravity.LEFT);
        sessionManager.clear();
        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
        startActivity(intent);
        finishAffinity();
    }

    @OnClick(R.id.tvLogout)
    void clickLogout(){
        showLogoutDialog();
    }

    @OnClick(R.id.tvorder)
    void clickOrder(){
        drawerLayout.closeDrawer(GravityCompat.START);
        Intent i = new Intent(HomeActivity.this,MyOrder.class);
        startActivity(i);
    }

    @Override
    public void onBackPressed() {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
            } else{
                doubleBackToExitPressedOnce = true;
                Snackbar snackbar = Snackbar
                        .make(mainlayout, getResources().getString(R.string.click_to_exit), Snackbar.LENGTH_LONG);
                snackbar.show();
                //  Toast.makeText(this, getResources().getString(R.string.click_Again_to_Exit), Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            }
        }

        @OnClick(R.id.tvRefer)
    void clickReferButton(){
            drawerLayout.closeDrawer(Gravity.LEFT);
            helpDialog.show();
            /*Create an ACTION_SEND Intent*/
            try {
                Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                String shareBody = "Join me on Bukhad Point, a Budget Friendly app for Ordering food. Enter my code "+sessionManager.getUser().getData().getUsername()+"  to earn ₹5 back on your first Order! https://play.google.com/store/apps/details?id=com.foodtruck.bhukkad";
                 intent.setType("text/plain");
                intent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.share_subject));
                intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(intent, getString(R.string.share_using)));
            }catch (Exception e){

            }
            helpDialog.dismiss();
        }


        @OnClick(R.id.tvwallet)
    void clickWallet(){
        drawerLayout.closeDrawer(Gravity.LEFT);
        Intent i = new Intent(HomeActivity.this,WalletActivity.class);
        startActivity(i);

        }

    @OnClick(R.id.tvAbout)
    void clickAbout(){
        drawerLayout.closeDrawer(Gravity.LEFT);
        Intent i = new Intent(HomeActivity.this,WebActivity.class);
        i.putExtra("title","About Us");
        i.putExtra("url","about-us/");
        startActivity(i);

    }

    @OnClick(R.id.tvPrivacy)
    void clickPrivacy(){
        drawerLayout.closeDrawer(Gravity.LEFT);
        Intent i = new Intent(HomeActivity.this,WebActivity.class);
        i.putExtra("title","Privacy & Policy");
        i.putExtra("url","privacy-policy/");
        startActivity(i);

    }

    @OnClick(R.id.tvterm)
    void clickTerm(){
        drawerLayout.closeDrawer(Gravity.LEFT);
        Intent i = new Intent(HomeActivity.this,WebActivity.class);
        i.putExtra("title","Terms & Conditions");
        i.putExtra("url","terms-condition/");
        startActivity(i);

    }

    public void bottomSheetDialog(String title, String description, String image, String id, int price, String rating){
        bottomSheetView = getLayoutInflater().inflate(R.layout.item_deatils_layout, null);
        bottomSheetDialog = new BottomSheetDialog(this,R.style.CustomBottomSheetDialog);
     //  bottomSheetDialog.getWindow().setBackgroundDrawableResource(R.drawable.roundcornerwhite);
        bottomSheetDialog.setContentView(bottomSheetView);
        ImageView imgFood=bottomSheetView.findViewById(R.id.imgFood);
        ImageView imgClose=bottomSheetView.findViewById(R.id.imgClose);
        TextView tvTitle=bottomSheetView.findViewById(R.id.tvTitle);
        TextView tvPrice=bottomSheetView.findViewById(R.id.tvPrice);
        TextView tvRating=bottomSheetView.findViewById(R.id.tvRating);
         rvReview= bottomSheetView.findViewById(R.id.rv_review);
         if (rating!=null) {
             tvRating.setText(rating+ " Star Rating");
         }else {
             tvRating.setText(" No Rating");
         }
         tvPrice.setText(Validations.stringPrice(price));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,RecyclerView.HORIZONTAL,false);
        rvReview.setLayoutManager(linearLayoutManager);
        viewReviewAdapter = new ViewReviewAdapter(this,lstReview);
        rvReview.setAdapter(viewReviewAdapter);
        TextView tvDescription=bottomSheetView.findViewById(R.id.tvDescription);
        tvTitle.setText(title);
        tvDescription.setText(description);
        if (image!= null) {
            Picasso.get().load("http://3.17.125.65"+image).into(imgFood);
        }
        bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
        bottomSheetBehavior.setBottomSheetCallback(bottomSheetCallback);

        bottomSheetDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {

            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });
        bottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

            }
        });
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        bottomSheetDialog.show();
        callApiToGetReviewData(id);
    }

    private void callApiToGetReviewData(String id) {
    DataService dataService = new DataService(this);
    dataService.setOnServerListener(new DataService.ServerResponseListner<ReviewResponse>() {
        @Override
        public void onDataSuccess(ReviewResponse response, int code) {
            if (response!=null && response.getSuccess()){
                if (response.getData().size()>0){
                    lstReview.clear();
                    lstReview.addAll(response.getData());
                    viewReviewAdapter.notifyDataSetChanged();
                }
            }
        }

        @Override
        public void onDataFailiure() {

        }
    });
    if (sessionManager.getUser()!=null && sessionManager.getUser().getData()!=null) {
        dataService.getReating("Token " + sessionManager.getUser().getData().getAuthToken(), id);
    }

    }


    BottomSheetBehavior.BottomSheetCallback bottomSheetCallback =
            new BottomSheetBehavior.BottomSheetCallback(){
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    switch (newState){
                        case BottomSheetBehavior.STATE_COLLAPSED:

                            break;
                        case BottomSheetBehavior.STATE_DRAGGING:

                            break;
                        case BottomSheetBehavior.STATE_EXPANDED:

                            break;
                        case BottomSheetBehavior.STATE_HIDDEN:

                            bottomSheetDialog.dismiss();
                            break;
                        case BottomSheetBehavior.STATE_SETTLING:

                            break;
                        default:

                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            };

    @OnClick(R.id.tvLocation)
    void clickLocation(){
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), getString(R.string.api_key), Locale.US);
        }
        openAutocompleteActivity(1);
    }
    @OnClick(R.id.tvLocation_black)
    void clickLocationBlack(){
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), getString(R.string.api_key), Locale.US);
        }
        openAutocompleteActivity(1);
    }
    private void openAutocompleteActivity(int REQUEST_CODE_AUTOCOMPLETE) {
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME,Place.Field.LAT_LNG,Place.Field.ADDRESS);
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields)
                .setCountry("IN")
                .build(this);
        startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.ORIGIN_REQUEST_CODE_AUTOCOMPLETE) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                if (place != null) {
                    String CurrentAddress = place.getAddress();
                    tvLocation.setText(CurrentAddress);
                    tvLocationBlack.setText(CurrentAddress);
                    latitude=place.getLatLng().latitude;
                    longitude=place.getLatLng().longitude;
                    recreate();
                    //callApiToGetCategoryData();
                    //   new SetDataInBackground(AddAddres.this, AddAddres.this).execute(coordinate);
                }

            }


        }
    }

    @OnClick(R.id.btLogin)
    void clickLogin(){
            openBottomSheet();



    }

    private void getLocation() {
        locationManager = (LocationManager) getSystemService( Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(true);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_MEDIUM);

        if (locationManager != null) {
            String provider = locationManager.getBestProvider(criteria, true);
            if (provider != null) {
                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                Location location = locationManager.getLastKnownLocation(provider);
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude=location.getLongitude();
                    String address = Utility.getCompleteAddressString(latitude, longitude, this);
                    tvLocation.setText(address);
                    tvLocationBlack.setText(address);
                    callApiToGetCategoryData();
                   // hideandshowLayout();
                   // updateWithNewLocation(location);
                    if (locationManager != null) {
                        locationManager.removeUpdates(locationListener);
                    }
                }else {
                    locationManager.requestLocationUpdates(provider, 1, 20, locationListener);
                }

                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500, 20, locationListener);
                }
                if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 500, 20, locationListener);
                }
            } else {
                if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 500, 20, locationListener);

                }
                else if (locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER)) {
                    locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 500, 20, locationListener);
                }
            }

        }
    }

    final android.location.LocationListener locationListener = new android.location.LocationListener() {
        public void onLocationChanged(Location location) {
          //  updateWithNewLocation(location);
            if (location!=null){
                latitude = location.getLatitude();
                longitude=location.getLongitude();
                String address = Utility.getCompleteAddressString(latitude, longitude, HomeActivity.this);
                tvLocation.setText(address);
                tvLocationBlack.setText(address);
                callApiToGetCategoryData();
                hideandshowLayout();
                if (locationManager != null) {
                    locationManager.removeUpdates(locationListener);
                }
            }

        }

        public void onProviderDisabled(String provider) {

         //  updateWithNewLocation(null);
        }

        public void onProviderEnabled(String provider) {

        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };

    @OnClick(R.id.img_current_location)
    void clickCurrentLocation(){
        getLocation();
        recreate();
    }

    @OnClick(R.id.img_current_location_black)
    void clickCurrentLocationBlack(){
        getLocation();
        recreate();
    }

    @OnClick(R.id.tvProfile)
    void clickProfile(){
        drawerLayout.closeDrawer(GravityCompat.START);
     Intent i  = new Intent(HomeActivity.this,EditProfile.class);
     startActivity(i);
    }


}
