package com.foodtruck.bhukkad.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.viewpager.widget.ViewPager;

import android.animation.Animator;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.baoyachi.stepview.VerticalStepView;
import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.adapter.ReviewAdapter;
import com.foodtruck.bhukkad.adapter.TrackOrderAdapter;
import com.foodtruck.bhukkad.adapter.ViewPagerTrackAdapter;
import com.foodtruck.bhukkad.customviews.dialog.CustomDialogBuilder;
import com.foodtruck.bhukkad.helper.RecyclerTouchListener;
import com.foodtruck.bhukkad.model.delivery.DeliveryResponse;
import com.foodtruck.bhukkad.model.delivery.MessageResponse;
import com.foodtruck.bhukkad.utils.FirebaseReferenceWS;
import com.foodtruck.bhukkad.utils.SessionManager;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TrackOrder extends AppCompatActivity {

    private DatabaseReference tripStatusObserver;
    private ValueEventListener tripStatusValueEventListener;
    CustomDialogBuilder customDialogBuilder;
    private String status;
    private String orderId;
    SessionManager sessionManager;

    @BindView(R.id.ht_scroll)
    VerticalStepView verticalStepView;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.rvStatus)
    RecyclerView rvStatus;

    List<DeliveryResponse> data = new ArrayList<>();
    DeliveryResponse deliveryResponse = new DeliveryResponse();
    MessageResponse messageResponse ;



    private List<String> stepsBeanList = new ArrayList<>();
    TrackOrderAdapter trackOrderAdapter;
    private AlertDialog dialogbox;
    List<MessageResponse> lstStringData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_order);
        ButterKnife.bind(this);
        tvTitle.setText("Track Order");
        sessionManager = new SessionManager(this);
        setString();

        orderId = String.valueOf(getIntent().getIntExtra("data",1));
        setUpList();
        setupRecyclerView();
        //setupViewPager();
      //  setUpVerticalStepView();
      //  customDialogBuilder = new CustomDialogBuilder(this);
        getRequestStatus();
      //  customDialogBuilder.showDeliveryDialogue();
    }

    private void setString() {
    lstStringData.add(new MessageResponse("Awesome",false));
    lstStringData.add(new MessageResponse("Clean",false));
    lstStringData.add(new MessageResponse("Punctual",false));
    lstStringData.add(new MessageResponse("Friendly Nature",false));
    lstStringData.add(new MessageResponse("Have great Skills",false));



    }

    private void setupRecyclerView() {
        LinearLayoutManager linearLayoutManager  = new LinearLayoutManager(this);
        rvStatus.setLayoutManager(linearLayoutManager);
         trackOrderAdapter = new TrackOrderAdapter(this,data);
        rvStatus.setAdapter(trackOrderAdapter);



    }

  /*  private void setupViewPager() {
        viewPager.setAdapter(new ViewPagerTrackAdapter(getSupportFragmentManager(),stepsBeanList));
        StepIndicator stepIndicator = findViewById(R.id.step_indicator);
        stepIndicator.setupWithViewPager(viewPager);
        stepIndicator.setEnabled(false);
    }*/

    private void setUpList() {
        deliveryResponse=new DeliveryResponse("Pending","done","");
        data.add(new DeliveryResponse("Pending","",""));
        data.add(new DeliveryResponse("Created","",""));
        data.add(new DeliveryResponse("Prepared","",""));
        data.add(new DeliveryResponse("On The Way","",""));
        data.add(new DeliveryResponse("Delivered","",""));
        //data.add(new DeliveryResponse("Cancelled", "",""));

    }

    private void setUpVerticalStepView(int step) {

        verticalStepView.setStepsViewIndicatorComplectingPosition(stepsBeanList.size() - step)//设置完成的步数
                .reverseDraw(false)//default is true
                .setStepViewTexts(stepsBeanList)//总步骤
                .setLinePaddingProportion(0.85f)//设置indicator线与线间距的比例系数
                .setStepsViewIndicatorCompletedLineColor(ContextCompat.getColor(this, android.R.color.black))//设置StepsViewIndicator完成线的颜色
                .setStepsViewIndicatorUnCompletedLineColor(ContextCompat.getColor(this, R.color.blue))//设置StepsViewIndicator未完成线的颜色
                .setStepViewComplectedTextColor(ContextCompat.getColor(this, android.R.color.black))//设置StepsView text完成线的颜色
                .setStepViewUnComplectedTextColor(ContextCompat.getColor(this, R.color.blue))//设置StepsView text未完成线的颜色
                .setStepsViewIndicatorCompleteIcon(ContextCompat.getDrawable(this, R.drawable.ic_check_dot))//设置StepsViewIndicator CompleteIcon
                .setStepsViewIndicatorDefaultIcon(ContextCompat.getDrawable(this, R.drawable.ic_button_dot))//设置StepsViewIndicator DefaultIcon
                .setStepsViewIndicatorAttentionIcon(ContextCompat.getDrawable(this, R.drawable.attention));//设置StepsViewIndicator AttentionIcon


    }

    public void getRequestStatus() {
        if (tripStatusObserver!=null){
            tripStatusObserver.removeEventListener(tripStatusValueEventListener);
        }
        tripStatusObserver = FirebaseReferenceWS.getI().getTripStatusReference(sessionManager.getUser().getData().getUsername(),orderId);
        tripStatusValueEventListener = tripStatusObserver.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // newRequestData.setEta(distance);
                if (dataSnapshot.getValue() != null) {
                    Map<String, Object> td = (HashMap<String, Object>) dataSnapshot.getValue();
                    status = String.valueOf(td.get("order_status"));

                    if (status.equals("CR")) {
                        orderCreated();

                    } else if (status.equals("PN")) {
                        orderPending();
                    } else if (status.equals("PR"))
                        orderPrepared();

                 else if (status.equals("OTW")) {
                     orderOutForDelivery();

                } else if (status.equals("DL")) {
                     orderDeliverd();
                        showRateDialog();

                   // customDialogBuilder.showDeliveryDialogue(2, TrackOrder.this);
                } else if (status.equals("CN")) {

                        //setUpVerticalStepView(1);
                  //  customDialogBuilder.showDeliveryDialogue(1, TrackOrder.this);

                }
                 trackOrderAdapter.notifyDataSetChanged();


                }
            };



            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("shubham", "onCancelled: "+databaseError.getMessage());
            }
        });

    }

    private void showRateDialog() {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            View viewInflated = LayoutInflater.from(this).inflate(R.layout.rate_layout, null);
            builder.setView(viewInflated);
            TextView btSkip = viewInflated.findViewById(R.id.tv_skip);
            btSkip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogbox.dismiss();
                }
            });

            RadioGroup radioGroup = viewInflated.findViewById(R.id.radio_g);
            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    dialogbox.dismiss();
                    showReviewDialog();
                }
            });
            dialogbox = builder.create();
            dialogbox.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialogbox.setCancelable(false);
            if (dialogbox!=null && dialogbox.isShowing()){
                dialogbox.dismiss();
            }
            dialogbox.show();






    }


    private void showReviewDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View viewInflated = LayoutInflater.from(this).inflate(R.layout.review_layout_given, null);
        builder.setView(viewInflated);
        ImageButton imgClose = viewInflated.findViewById(R.id.img_close);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogbox.dismiss();
            }
        });
        RecyclerView recyclerView  = viewInflated.findViewById(R.id.rvText);
        EditText edtReview  = viewInflated.findViewById(R.id.edtReview);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        ReviewAdapter reviewAdapter = new ReviewAdapter(this,lstStringData);
        recyclerView.setAdapter(reviewAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                for (MessageResponse messageResponse: lstStringData){
                    messageResponse.setIselect(false);
                }
                lstStringData.get(position).setIselect(true);
              /*  if (messageResponse==null){
                    messageResponse = new MessageResponse(lstStringData.get(position).getMessage(),true);
                }else {
                    messageResponse.setIselect(false);
                    messageResponse = new MessageResponse(lstStringData.get(position).getMessage(),true);
                }*/
                edtReview.setText(lstStringData.get(position).getMessage());
                reviewAdapter.notifyDataSetChanged();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        dialogbox = builder.create();
        dialogbox.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialogbox.setCancelable(false);
        if (dialogbox!=null && dialogbox.isShowing()){
            dialogbox.dismiss();
        }
        dialogbox.show();






    }

    private void orderDeliverd() {
        data.get(0).setStatus("done");
        data.get(1).setStatus("done");
        data.get(2).setStatus("done");
        data.get(3).setStatus("done");
        data.get(4).setStatus("done");
       // lottieAnimationView.setAnimation(R.raw.delivery_man_in_a_scooter_with_mask);

    }

    private void orderOutForDelivery() {
        data.get(0).setStatus("done");
        data.get(1).setStatus("done");
        data.get(2).setStatus("done");
        data.get(3).setStatus("done");
        data.get(4).setStatus("pro");
    //    lottieAnimationView.setAnimation(R.raw.delivery_man_in_a_scooter_with_mask);
    }

    private void orderPrepared() {
        data.get(0).setStatus("done");
        data.get(1).setStatus("done");
        data.get(2).setStatus("done");
        data.get(3).setStatus("pro");
     //   lottieAnimationView.setAnimation(R.raw.food_around_the_city_updated_for_ios);
    }

    private void orderPending() {
        data.get(0).setStatus("done");
        data.get(1).setStatus("pro");
     //   lottieAnimationView.setAnimation(R.raw.food_delivery);
    }

    private void orderCreated() {
        data.get(0).setStatus("done");
        data.get(1).setStatus("done");
        data.get(2).setStatus("pro");
       // lottieAnimationView.setAnimation(R.raw.food_delivery);
    }

    @OnClick(R.id.back_btn)
    void clickBackButton(){
        onBackPressed();
    }
}