package com.foodtruck.bhukkad.view.fragment.myorder;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.adapter.OrderHistoryAdapter;
import com.foodtruck.bhukkad.model.order.MyOrderResponse;
import com.foodtruck.bhukkad.model.order.MyOrderResponseList;
import com.foodtruck.bhukkad.network.DataService;
import com.foodtruck.bhukkad.utils.SessionManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HistoryOrder#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HistoryOrder extends Fragment {

    @BindView(R.id.tv_order_history)
    RecyclerView rvOrderHistory;

    OrderHistoryAdapter orderHistoryAdapter;
    private SessionManager sessionManager;

    List<MyOrderResponseList> data = new ArrayList<>();

    public static HistoryOrder newInstance(String param1, String param2) {
        HistoryOrder fragment = new HistoryOrder();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

      View view= inflater.inflate(R.layout.fragment_history_order, container, false);
        ButterKnife.bind(this,view);
        sessionManager = new SessionManager(getActivity());
        setUpRecyclerView();
        getDataFromServer();
      return view;

    }

    private void setUpRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvOrderHistory.setLayoutManager(linearLayoutManager);
        orderHistoryAdapter = new OrderHistoryAdapter(getActivity(),data);
        rvOrderHistory.setAdapter(orderHistoryAdapter);



    }

    private void getDataFromServer() {
        DataService dataService = new DataService(getActivity());
        dataService.setOnServerListener(new DataService.ServerResponseListner<MyOrderResponse>() {
            @Override
            public void onDataSuccess(MyOrderResponse response, int code) {
                if (response!=null && response.getSuccess()){
                    for (MyOrderResponseList data1: response.getData()){
                        if (data1.getStatus().equals("DL")){
                            data.add(data1);
                        }
                    }
                    orderHistoryAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });

        dataService.callApiToGetData("Token "+sessionManager.getUser().getData().getAuthToken());

    }
}