package com.foodtruck.bhukkad.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.helper.Constants;
import com.foodtruck.bhukkad.model.profile.ProfileData;
import com.foodtruck.bhukkad.model.profile.ProfileResponse;
import com.foodtruck.bhukkad.model.viewcart.Costing;
import com.foodtruck.bhukkad.model.viewcart.Data;
import com.foodtruck.bhukkad.network.DataService;
import com.foodtruck.bhukkad.utils.SessionManager;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class EditProfile extends AppCompatActivity {

    @BindView(R.id.tv_username)
    EditText tvUserName;
    @BindView(R.id.tv_name)
    EditText tvName;

    @BindView(R.id.tv_email)
    EditText tvEmail;

    @BindView(R.id.tv_mobile)
    EditText tvMovile;

    @BindView(R.id.radio_g)
    RadioGroup gender;



    @BindView(R.id.imageView)
    ImageView imageView;

    @BindView(R.id.tvTitle)
    TextView tvTitle;


    String genderString = "M";
    SessionManager sessionManager;
    private Uri mCropImageUri;
    Map<String, RequestBody> map = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        sessionManager = new SessionManager(this);
        ButterKnife.bind(this);
        tvTitle.setText("Edit Profile");
        if (sessionManager.getProfileImage(Constants.PROFILEIMAGE)!= null && !sessionManager.getProfileImage(Constants.PROFILEIMAGE).isEmpty()){
            Picasso.get().load(sessionManager.getProfileImage(Constants.PROFILEIMAGE)).into(imageView);
        }
        gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.male:
                        genderString = "M";
                        break;
                    case R.id.female:
                        genderString = "F";
                        break;
                    case R.id.other:
                        genderString = "O";
                        break;
                }
            }

        });
        getProfileData();

    }

    private void getProfileData() {
        DataService dataService = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<ProfileResponse>() {
            @Override
            public void onDataSuccess(ProfileResponse response, int code) {
                if (response != null && response.getSuccess()) {
                    if (response.getData() != null) {
                        setDataInView(response.getData());
                        if (response.getData().getProfileImage()!=null){
                            sessionManager.saveProfileImage(Constants.PROFILEIMAGE,response.getData().getProfileImage());
                        }

                    }
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.getProfileData("Token " + sessionManager.getUser().getData().getAuthToken());


    }

    private void setDataInView(ProfileData data) {
        tvUserName.setText(data.getUsername());
        tvName.setText(data.getDisplayName());
        tvEmail.setText(data.getEmail());
        tvMovile.setText(data.getMobileNumber());
        if (data.getProfileImage()!=null)
        {
            Picasso.get().load(data.getProfileImage()).into(imageView);
        }
    }

    @OnClick(R.id.imageView)
    void clickImageView() {
        CropImage.activity(mCropImageUri)
                .setGuidelines(CropImageView.Guidelines.OFF)
                .setCropMenuCropButtonTitle(getString(R.string.Save))
                .setAllowFlipping(false)
                .setAllowRotation(false)
                .setMultiTouchEnabled(true)
                .setActivityTitle("Select Image")
                .start(this);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK && result != null) {
                Uri resultUri = result.getUri();
                imageView.setImageURI(resultUri);
                File file = new File(resultUri.getPath());
                RequestBody requestFile =
                        RequestBody.create(MediaType.parse("image/*"), file);
                RequestBody id = RequestBody.create(MediaType.parse("text/plain"), genderString);

               // map.put("profile_image; filename=pp.jpeg", requestFile);
                map.put("profile_image\"; filename=\""+"pp.jpeg", requestFile);
               // map.put("profile_image", requestFile);
                map.put("gender", id);
                callApiToUploadImage();
              //  body = MultipartBody.Part.createFormData("user_profile", file.getName(), requestFile);
                //  picturePath=resultUri.getPath();

               /* if (picturePath != null) {
                    String filename = picturePath.substring(picturePath.lastIndexOf("/") + 1);
                    encodeandCompressImage();
                }*/


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }

    }

    private void callApiToUploadImage() {
        DataService dataService  = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<ProfileResponse>() {
            @Override
            public void onDataSuccess(ProfileResponse response, int code) {
                if (response!=null && response.getSuccess()){
                    getProfileData();
               /*         Toast.makeText(EditProfile.this, "Data Update Successfully", Toast.LENGTH_SHORT).show();
                        finish();*/
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });

        dataService.updateProfile("Token "+sessionManager.getUser().getData().getAuthToken(),map);


    }

    @OnClick(R.id.back_btn)
    void clickBackButton(){
        onBackPressed();
    }
}