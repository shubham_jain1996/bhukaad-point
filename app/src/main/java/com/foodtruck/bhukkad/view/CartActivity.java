package com.foodtruck.bhukkad.view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.arch.core.executor.DefaultTaskExecutor;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import android.animation.Animator;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.foodtruck.bhukkad.MainActivity;
import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.adapter.ViewCartAdapter;
import com.foodtruck.bhukkad.adapter.ViewCouponAdapter;
import com.foodtruck.bhukkad.helper.Constants;
import com.foodtruck.bhukkad.model.addres.AddressData;
import com.foodtruck.bhukkad.model.addres.AddressResponse;
import com.foodtruck.bhukkad.model.addres.DataAddress;
import com.foodtruck.bhukkad.model.cart.CartModel;
import com.foodtruck.bhukkad.model.cart.RemoveCartItemModel;
import com.foodtruck.bhukkad.model.coupon.CouponResponse;
import com.foodtruck.bhukkad.model.coupon.Offer;
import com.foodtruck.bhukkad.model.discount.DiscountResponse;
import com.foodtruck.bhukkad.model.viewcart.CartDetail;
import com.foodtruck.bhukkad.model.viewcart.CartViewResponse;
import com.foodtruck.bhukkad.model.viewcart.Costing;
import com.foodtruck.bhukkad.model.wallet.WalletResponse;
import com.foodtruck.bhukkad.network.DataService;
import com.foodtruck.bhukkad.utils.SessionManager;
import com.foodtruck.bhukkad.utils.Validations;
import com.foodtruck.bhukkad.view.address.AddAddres;
import com.foodtruck.bhukkad.view.address.ViewActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.foodtruck.bhukkad.MainActivity.latitude;
import static com.foodtruck.bhukkad.MainActivity.longitude;

public class CartActivity extends AppCompatActivity implements ViewCartAdapter.clickEvent, ViewCouponAdapter.clickResponse {

    @BindView(R.id.tvItemTotal)
    TextView tvItemTotal;
    @BindView(R.id.tvTaxes)
    TextView tvTaxes;
    @BindView(R.id.tvDeliveryCharges)
    TextView tvDeliveryCharges;
    @BindView(R.id.tvGrandTotal)
    TextView tvGrandTotal;
    @BindView(R.id.rv_food_list)
    RecyclerView rvFoodList;
    @BindView(R.id.tv_no_data)
    TextView tvNoData;
    @BindView(R.id.bottomLayout)
    CardView mainLayout;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.etCoupon)
    EditText etCoupon;
    @BindView(R.id.lvDiscount)
            LinearLayout lvDiscount;
    @BindView(R.id.tvDiscount)
            TextView tvDiscount;

    @BindView(R.id.tvWalletAmount)
            TextView tvWalletAmount;
    @BindView(R.id.tvAddWallet)
    TextView tvAddWallet;

    @BindView(R.id.lvWallet)
            LinearLayout lvWallet;

    @BindView(R.id.tvWalletPrice)
            TextView tvWalletPrice;
    Costing costing;

    @BindView(R.id.tvAddress)
            TextView tvAddress;

    @BindView(R.id.bt_order)
            Button btOrder;

    TextView tvError;




    List<CartDetail> cartData = new ArrayList<>();
    List<Offer> couponData = new ArrayList<>();
    ViewCartAdapter viewCartAdapter;
    SessionManager sessionManager;
    private View bottomSheetView;
    BottomSheetDialog bottomSheetDialog;
    BottomSheetBehavior bottomSheetBehavior;
    private RecyclerView rvListCoupon;
    ViewCouponAdapter viewCouponAdapter;
    String couponcode="";
    Integer walletAddAmount=0;
    private AlertDialog dialogbox;
    List<AddressData> dataAddresses = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        getDataForCoupon();


        bottomSheetView = getLayoutInflater().inflate(R.layout.bottomsheetdialog_layout, null);
        bottomSheetDialog = new BottomSheetDialog(CartActivity.this);
        bottomSheetDialog.setContentView(bottomSheetView);
         rvListCoupon  = bottomSheetView.findViewById(R.id.rv_list_coupon);
        tvError  = bottomSheetView.findViewById(R.id.tvError);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvListCoupon.setLayoutManager(linearLayoutManager);
        viewCouponAdapter = new ViewCouponAdapter(this,couponData,this);
        rvListCoupon.setAdapter(viewCouponAdapter);
        bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
        bottomSheetBehavior.setBottomSheetCallback(bottomSheetCallback);

        bottomSheetDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                viewCouponAdapter.notifyDataSetChanged();
            }
        });

        bottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

            }
        });
        tvTitle.setText("My Cart");
        setUpRecyclerView();
        callApiToGetCartData();
        callApiToGetWalletDetails();
        CallApiToGetAddress();

    }

    private void CallApiToGetAddress() {
    DataService dataService = new DataService(this);
    dataService.setOnServerListener(new DataService.ServerResponseListner<AddressResponse>() {
        @Override
        public void onDataSuccess(AddressResponse response, int code) {
            if (response!=null && response.getSuccess()){
                if (response.getData()!=null){
                    if (response.getData().getAddress().size()>0){
                        dataAddresses.clear();
                        dataAddresses.addAll(response.getData().getAddress());
                        String temVar=checkAddressStatus();
                        if (Integer.parseInt(temVar)<=0){
                            tvAddress.setText("Add Address");
                        }
                    }else {
                        tvAddress.setText("Add Address");
                    }
                }
            }
        }

        @Override
        public void onDataFailiure() {

        }
    });
    dataService.callApiToGetAddress("Token "+sessionManager.getUser().getData().getAuthToken(),latitude,longitude);

    }

    private String  checkAddressStatus() {
        for (AddressData data: dataAddresses){
            if (data.getValidStatus()){
                tvAddress.setText(data.getLine1());
                btOrder.setEnabled(true);
                return String.valueOf(data.getId());
            }
        }


    return String.valueOf("0");
    }

    private void getDataForCoupon() {
    DataService dataService = new DataService(this);
    dataService.setOnServerListener(new DataService.ServerResponseListner<CouponResponse>() {
        @Override
        public void onDataSuccess(CouponResponse response, int code) {
            if (response!=null && response.getData()!=null && response.getData().getOffers().size()>0){
            couponData.addAll(response.getData().getOffers());
            viewCouponAdapter.notifyDataSetChanged();
            tvError.setVisibility(View.INVISIBLE);
            }else {
                tvError.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onDataFailiure() {

        }
    });
    dataService.callApiToGetCoupon("Token "+sessionManager.getUser().getData().getAuthToken());

    }

    @OnClick(R.id.back_btn)
    void clickBackButton(){
        onBackPressed();
    }

    @OnClick(R.id.etCoupon)
    void clickCouponButton(){
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        bottomSheetDialog.show();
    }

    private void callApiToGetCartData() {
        DataService dataService = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<CartViewResponse>() {
            @Override
            public void onDataSuccess(CartViewResponse response, int code) {
            if (response!=null && response.getSuccess() && response.getData()!=null){
                if (response.getData().getCartDetail().size()>0){
                    cartData.clear();
                    cartData.addAll(response.getData().getCartDetail());
                    viewCartAdapter.notifyDataSetChanged();
                    costing=response.getData().getCosting();
                    setDataInView(response.getData().getCosting());
                    mainLayout.setVisibility(View.VISIBLE);
                    rvFoodList.setVisibility(View.VISIBLE);
                    tvNoData.setVisibility(View.INVISIBLE);
                }else {
                    mainLayout.setVisibility(View.INVISIBLE);
                    rvFoodList.setVisibility(View.INVISIBLE);
                    tvNoData.setVisibility(View.VISIBLE);
                }
            }
            }

            @Override
            public void onDataFailiure() {

            }
        });

        dataService.getCartData(String.valueOf(latitude),String.valueOf(longitude),"Token "+sessionManager.getUser().getData().getAuthToken());


    }

    private void setDataInView(Costing costing) {
    tvItemTotal.setText( Validations.stringPrice(costing.getSubTotal()));
    tvDeliveryCharges.setText(Validations.stringPrice(String.format("%.2f", costing.getShipping())));
    tvTaxes.setText(Validations.stringPrice(costing.getTax()));
    Double temp = costing.getTotal()-Double.parseDouble(String.valueOf(walletAddAmount));
    tvGrandTotal.setText(Validations.stringPrice(String.format("%.2f", temp)));
        if (walletAddAmount>0){
            lvWallet.setVisibility(View.VISIBLE);
        }else {
            lvWallet.setVisibility(View.GONE);
        }
        tvWalletPrice.setText("-"+Validations.stringPrice(walletAddAmount));
    }

    private void setUpRecyclerView() {
        LinearLayoutManager linearLayoutManager =new LinearLayoutManager(this);
        rvFoodList.setLayoutManager(linearLayoutManager);
        viewCartAdapter = new ViewCartAdapter(this,cartData,this);
        rvFoodList.setAdapter(viewCartAdapter);


    }
    private void callApiToDeleteQuantity(int qt, Integer id, String s) {
        DataService dataService = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<RemoveCartItemModel>() {
            @Override
            public void onDataSuccess(RemoveCartItemModel response, int code) {
                if (response!=null &&  response.getSuccess() && response.getData()!=null){
                    callApiToGetCartData();
                   // sendErrorMsgToActivity("cart_count",response.getData().getCartCount());
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.deleteItem(qt,id,s,"Token "+sessionManager.getUser().getData().getAuthToken());
    }

    private void callApiToAddQuantity(int qt, Integer id, String s) {
        DataService dataService = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<CartModel>() {
            @Override
            public void onDataSuccess(CartModel response, int code) {
                if (response!=null &&  response.getSuccess() && response.getData()!=null){
                    callApiToGetCartData();
                  //  sendErrorMsgToActivity("cart_count",response.getData().getCartCount());
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.additem(qt,id,s,"Token "+sessionManager.getUser().getData().getAuthToken());
    }

    @Override
    public void clickAddButton(int qty, int position, int price) {
        callApiToAddQuantity(qty,position,String.valueOf(price));

    }

    @Override
    public void clickDeleteButton(int qty, int position, int price) {
        callApiToDeleteQuantity(qty,position,String.valueOf(price));

    }

    @OnClick(R.id.bt_order)
    void clickOrderButton(){
        Intent i = new Intent(CartActivity.this,Payment.class);
        i.putExtra("coupon",couponcode);
        startActivity(i);
    }

    BottomSheetBehavior.BottomSheetCallback bottomSheetCallback =
            new BottomSheetBehavior.BottomSheetCallback(){
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    switch (newState){
                        case BottomSheetBehavior.STATE_COLLAPSED:

                            break;
                        case BottomSheetBehavior.STATE_DRAGGING:

                            break;
                        case BottomSheetBehavior.STATE_EXPANDED:

                            break;
                        case BottomSheetBehavior.STATE_HIDDEN:

                            bottomSheetDialog.dismiss();
                            break;
                        case BottomSheetBehavior.STATE_SETTLING:

                            break;
                        default:

                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            };

    @Override
    public void clickOnCoupon(String code) {
        couponcode=code;
        etCoupon.setText(code);
        bottomSheetBehavior.setHideable(true);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }

    @OnClick(R.id.tvApply)
    void clickCouponApply(){
        if (etCoupon.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "Please enter One coupon code", Toast.LENGTH_SHORT).show();
        }else {
            callApiToCheckCoupon();

        }
    }

    private void callApiToCheckCoupon() {
    DataService dataService= new DataService(this);
    dataService.setOnServerListener(new DataService.ServerResponseListner<DiscountResponse>() {
        @Override
        public void onDataSuccess(DiscountResponse response, int code) {
            if (response!=null && response.getSuccess()){
                etCoupon.setText(null);
                if (response.getData()!=null && response.getData().getCosting()!=null && response.getData().getCosting().getCouponStatus()){
                    lvDiscount.setVisibility(View.VISIBLE);
                    tvItemTotal.setText( Validations.stringPrice(response.getData().getCosting().getSubTotal()));
                    tvDeliveryCharges.setText(Validations.stringPrice(String.format("%.2f", response.getData().getCosting().getShipping())));
                    tvTaxes.setText(Validations.stringPrice(response.getData().getCosting().getTax()));
                    Double temp = costing.getTotal()-Double.parseDouble(String.valueOf(walletAddAmount));
                    tvGrandTotal.setText(Validations.stringPrice(String.format("%.2f",  temp)));
                    tvDiscount.setText("-"+Validations.stringPrice(response.getData().getCosting().getCouponDiscount()));
                    if (walletAddAmount>0){
                        lvWallet.setVisibility(View.VISIBLE);
                    }else {
                        lvWallet.setVisibility(View.GONE);
                    }
                    tvWalletPrice.setText("-"+Validations.stringPrice(walletAddAmount));
                }else{
                    Toast.makeText(CartActivity.this, "Apply not sccessfully", Toast.LENGTH_SHORT).show();
                    lvDiscount.setVisibility(View.GONE);
                }
            }
        }

        @Override
        public void onDataFailiure() {

        }
    });
    dataService.callApiToGetCouponStatus("Token "+sessionManager.getUser().getData().getAuthToken(),etCoupon.getText().toString());


    }
    private void callApiToGetWalletDetails() {
        DataService dataService = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<WalletResponse>() {
            @Override
            public void onDataSuccess(WalletResponse response, int code) {
                if (response!=null){
                    if (response.getData()!=null){
                        if (response.getData().getBalance()!=null){
                            tvWalletAmount.setText("Wallet "+Validations.stringPrice(response.getData().getBalance()));
                        }

                    }
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.callApiToGetWalletDetails("Token "+sessionManager.getUser().getData().getAuthToken());

    }

    @OnClick(R.id.tvAddWallet)
    void clickAddWallet(){
        String wallet = tvWalletAmount.getText().toString().trim().replace("Wallet \u20B9","").trim();
        double temp = Double.parseDouble(wallet);
        int walletPaisa=(int) temp;

        if (walletPaisa!=0){
            if (walletPaisa>0  ){
                callFunctionToShowAmountDialog(walletPaisa);
            }else {
                Toast.makeText(this, "No money Find please share our application to earn more points", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void callFunctionToShowAmountDialog(int wallet) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View viewInflated = LayoutInflater.from(this).inflate(R.layout.wallet_dialog, null);
        builder.setView(viewInflated);
       EditText edtAddAmount = viewInflated.findViewById(R.id.edt_addAmount);
            TextView btAdd= viewInflated.findViewById(R.id.bt_add);
            TextView tvError = viewInflated.findViewById(R.id.textError);
            btAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (edtAddAmount.getText().toString().isEmpty()){
                       tvError.setText("Please add amount");
                    }else if (Integer.parseInt(edtAddAmount.getText().toString().trim())>costing.getItemDiscount()){
                        tvError.setText("You cant not use amount more than "+costing.getItemDiscount());
                    }else if (Integer.parseInt(edtAddAmount.getText().toString().trim())>wallet){
                        tvError.setText("You dont have enough balance ");
                    }else {
                        walletAddAmount=Integer.parseInt(edtAddAmount.getText().toString().trim());
                        ;
                    }
                }
            });
            ImageButton imgClose = viewInflated.findViewById(R.id.imgClose);
            imgClose.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 dialogbox.dismiss();
             }
            });

        dialogbox = builder.create();
        dialogbox.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialogbox.setCancelable(false);
        if (dialogbox!=null && dialogbox.isShowing()){
            dialogbox.dismiss();
        }
        dialogbox.show();

        }

        @OnClick(R.id.imgAddress)
        void clickAddAddressScreen(){
        Intent i = new Intent(CartActivity.this, ViewActivity.class);
        //Intent i = new Intent(CartActivity.this, ViewActivity.class);
        startActivityForResult(i,2);
        }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 2) {
                String returnedResult = data.getData().toString();
              tvAddress.setText(returnedResult);

            }


        }
    }
}
