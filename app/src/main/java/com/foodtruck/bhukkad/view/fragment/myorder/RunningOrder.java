package com.foodtruck.bhukkad.view.fragment.myorder;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.adapter.RunningOrderAdapter;
import com.foodtruck.bhukkad.model.order.MyOrderResponse;
import com.foodtruck.bhukkad.model.order.MyOrderResponseList;
import com.foodtruck.bhukkad.network.DataService;
import com.foodtruck.bhukkad.utils.SessionManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RunningOrder#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RunningOrder extends Fragment {

@BindView(R.id.rv_running_order)
    RecyclerView rvRunningOrder;
ArrayList<MyOrderResponseList> data = new ArrayList<>();
SessionManager sessionManager;

RunningOrderAdapter runningOrderAdapter;
    public static RunningOrder newInstance(String param1, String param2) {
        RunningOrder fragment = new RunningOrder();
        Bundle args = new Bundle();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View  view = inflater.inflate(R.layout.fragment_running_order, container, false);
        ButterKnife.bind(this,view);
        sessionManager = new SessionManager(getActivity());
        setUpRecyclerView();
        getDataFromServer();


        return view;
    }

    private void getDataFromServer() {
        DataService dataService = new DataService(getActivity());
        dataService.setOnServerListener(new DataService.ServerResponseListner<MyOrderResponse>() {
            @Override
            public void onDataSuccess(MyOrderResponse response, int code) {
                if (response!=null && response.getSuccess()){
                    for (MyOrderResponseList data1: response.getData()){
                        if (!data1.getStatus().equals("DL") && !data1.getStatus().equals("CN")){
                            data.add(data1);
                        }
                    }
                    runningOrderAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });

        dataService.callApiToGetData("Token "+sessionManager.getUser().getData().getAuthToken());

    }


    private void setUpRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvRunningOrder.setLayoutManager(linearLayoutManager);
        runningOrderAdapter = new RunningOrderAdapter(getActivity(),data);
        rvRunningOrder.setAdapter(runningOrderAdapter);



    }
}