package com.foodtruck.bhukkad.view.fragment.login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.chaos.view.PinView;
import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.model.register.RegisterResponse;
import com.foodtruck.bhukkad.network.DataService;
import com.foodtruck.bhukkad.utils.Const;
import com.foodtruck.bhukkad.utils.SessionManager;
import com.foodtruck.bhukkad.utils.Validations;
import com.foodtruck.bhukkad.view.HomeActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class PhoneVerification extends AppCompatActivity {
    String mobileNumber;
    String name;
    String password;
    String email;
    String gender;
    String userName;
    String referral;
    String token="";
    private String mVerificationId;
    public SessionManager sessionManager = null;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    FirebaseAuth auth;
    String isVerify="N";
    PinView otpTextView;
    Button btnReedem;
    TextView tvSkip;
    TextView resendOtp;
    private CountDownTimer cTimer1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_verification);
        sessionManager = new SessionManager(this);
        mobileNumber=getIntent().getStringExtra("mobile");
        name=getIntent().getStringExtra("name");
        userName=getIntent().getStringExtra("userName");
        gender=getIntent().getStringExtra("gender");
        email=getIntent().getStringExtra("email");
        password=getIntent().getStringExtra("password");
        referral=getIntent().getStringExtra("referral");
        token=getIntent().getStringExtra("token");
        otpTextView= findViewById(R.id.otp_view);
        btnReedem=findViewById(R.id.btn_redeem);
        tvSkip=findViewById(R.id.tv_skip);
        resendOtp=findViewById(R.id.resendOtp);
        startTimer();
        btnReedem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidations()){
                    verifyVerificationCode(otpTextView.getText().toString());
                }
            }
        });

        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callApiToRegisterUser();
            }
        });

        resendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTimer();
                resendOtpRequest();
            }
        });



        sendVerificationCode(mobileNumber);
    }
    private void resendOtpRequest() {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                "+91"+mobileNumber,        // Phone number to verify
                1  ,               // Timeout duration
                TimeUnit.MINUTES,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                mResendToken);             // Force Resending Token from callbacks

    }

    private boolean checkValidations() {
        boolean isValid= true;
        if (Validations.isEmpty(otpTextView.getText().toString())){
            isValid=false;
            Toast.makeText(this, "Please enter OTP", Toast.LENGTH_SHORT).show();
        }

        return isValid;
    }

    private void sendVerificationCode(String mobile) {
        auth = FirebaseAuth.getInstance();
        PhoneAuthOptions options =
                PhoneAuthOptions.newBuilder(auth)
                        .setPhoneNumber("+91"+mobile)       // Phone number to verify
                        .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
                        .setActivity(this)                 // Activity (for callback binding)
                        .setCallbacks(mCallbacks)          // OnVerificationStateChangedCallbacks
                        .build();
        PhoneAuthProvider.verifyPhoneNumber(options);
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            //Getting the code sent by SMS
            String code = phoneAuthCredential.getSmsCode();
            if (code != null) {

                //verifying the code

            }
            //sometime the code is not detected automatically
            //in this case the code will be null
            //so user has to manually enter the code

        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Log.d("Shubham", "onVerificationFailed: " + e.getLocalizedMessage());
            Toast.makeText(PhoneVerification.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            mVerificationId = s;
            mResendToken = forceResendingToken;
        }
    };
    private void verifyVerificationCode(String otp) {
        //creating the credential
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, otp);

        //signing the user
        signInWithPhoneAuthCredential(credential);
    }
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        auth.signInWithCredential(credential)
                .addOnCompleteListener(PhoneVerification.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            isVerify="Y";
                            callApiToRegisterUser();


                        } else {

                            //verification unsuccessful.. display an error message

                            String message = "Somthing is wrong, we will fix it soon...";

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                message = "Invalid code entered...";
                            }

                            Snackbar snackbar = Snackbar.make(findViewById(R.id.parent), message, Snackbar.LENGTH_LONG);
                            snackbar.setAction("Dismiss", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            });
                            snackbar.show();
                        }
                    }
                });
    }

    private void callApiToRegisterUser() {
        DataService dataService =new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<RegisterResponse>() {
            @Override
            public void onDataSuccess(RegisterResponse response, int code) {
                if (response!=null && response.getSuccess()){
                    sessionManager.saveBooleanValue(Const.IS_LOGIN, true);
                    sessionManager.saveUser(response);
                    Intent i = new Intent(PhoneVerification.this, HomeActivity.class);
                    startActivity(i);
                   finishAffinity();
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.callApiToRegister(userName, email,password,password,mobileNumber,gender,name,isVerify,referral,sessionManager.getTokenValue("Token"),token);





    }
    void startTimer() {
        resendOtp.setClickable(false);
        cTimer1 = new CountDownTimer(60000, 1000) {
            public void onTick(long millisUntilFinished) {
                resendOtp.setText(""+(int) (millisUntilFinished / 1000));
                // resendOtp.setText(String.valueOf(millisUntilFinished/1000));

            }
            public void onFinish() {
                resendOtp.setClickable(true);
                resendOtp.setText(getResources().getString(R.string.resendOtp));
            }
        };
        cTimer1.start();
    }

    void cancelTimer() {
        if(cTimer1!=null) {
            cTimer1.cancel();
        }

    }
}