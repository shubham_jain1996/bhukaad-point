 package com.foodtruck.bhukkad.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.foodtruck.bhukkad.MainActivity;
import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.utils.SessionManager;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.foodtruck.bhukkad.MainActivity.latitude;
import static com.foodtruck.bhukkad.MainActivity.longitude;

 public class Payment extends AppCompatActivity {

    @BindView(R.id.mainWebView)
    WebView webviewMain;
    private ProgressDialog progressDialog;
    SessionManager sessionManager;
    boolean iscall=false;
    String coupon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ButterKnife.bind(this);
        coupon = getIntent().getStringExtra("coupon");

        sessionManager= new SessionManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        setUpWebView();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setUpWebView() {
            webviewMain.getSettings().setLoadsImagesAutomatically(true);
            webviewMain.getSettings().setLoadWithOverviewMode(true);
            webviewMain.getSettings().setSupportZoom(true);
            webviewMain.getSettings().setBuiltInZoomControls(true);
            webviewMain.getSettings().setDisplayZoomControls(false);
            webviewMain.getSettings().setJavaScriptEnabled(true);
            webviewMain.getSettings().setPluginState(WebSettings.PluginState.ON);
            webviewMain.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
            webviewMain.getSettings().setDomStorageEnabled(true);
        // webviewMain.setWebChromeClient(new UriChromeClient());
        //webviewMain.setWebViewClient(new UriWebViewClient());
            webviewMain.setWebViewClient(new WebViewClient());
            webviewMain.getSettings().setUserAgentString("Mozilla/5.0 (Linux; Android 4.1.1; Galaxy Nexus Build/JRO03C) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19");
        //+sessionManager.getUser().getData().getAuthToken()
            HashMap<String, String> headerMap = new HashMap<>();
            headerMap.put("Authorization", "Token "+sessionManager.getUser().getData().getAuthToken());
            if (coupon!=null && !coupon.isEmpty()) {
                webviewMain.loadUrl(" http:/3.17.125.65/menu/generate-order?lat=" + latitude + "&lng=" + longitude + "&coupon=" + coupon, headerMap);
            }else {
                webviewMain.loadUrl(" http:/3.17.125.65/menu/generate-order?lat="+latitude+"&lng="+longitude,headerMap);
            }
            webviewMain.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));


                }catch (Exception e){
                    Toast.makeText(Payment.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });




        if (Build.VERSION.SDK_INT >= 21) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(webviewMain, true);
        } else {
            CookieManager.getInstance().setAcceptCookie(true);
        }



        webviewMain.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                webviewMain.loadUrl(url);
                Log.d("Sahil", "shouldOverrideUrlLoading: "+url);
                if (url.contains("success") && !iscall){
                    showSuccessDialog();
                }else if(url.contains("cancel")&& !iscall){
                    showCancelDailog();
                }else if(url.contains("failure") && !iscall){
                    showFailurDailog();
                }
                return true;
                //  return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
            }

            @Nullable
            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
                if (url.contains("success") && !iscall){
                    showSuccessDialog();
                    iscall=true;
                }else if(url.contains("cancel")&& !iscall){
                    showCancelDailog();
                    iscall=true;
                }else if(url.contains("failure") && !iscall){
                    showFailurDailog();
                    iscall=true;
                }

                return super.shouldInterceptRequest(view, url);

            }

            @Override
            public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {

                super.doUpdateVisitedHistory(view, url, isReload);
            }

            @Override
            public void onPageCommitVisible(WebView view, String url) {
                super.onPageCommitVisible(view, url);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                progressDialog.show();
            }

            public void onPageFinished(WebView view, String url) {
                progressDialog.dismiss();



            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                progressDialog.dismiss();
            }

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                progressDialog.dismiss();
                final AlertDialog.Builder builder = new AlertDialog.Builder(Payment.this);
                builder.setMessage(R.string.notification_error_ssl_cert_invalid);
                builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        handler.proceed();
                    }
                });
                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        handler.cancel();
                    }
                });
                final AlertDialog dialog = builder.create();
                dialog.show();
            }
        });




        if (Build.VERSION.SDK_INT >= 21) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(webviewMain, true);
        } else {
            CookieManager.getInstance().setAcceptCookie(true);
        }




    }

    private void showFailurDailog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(Payment.this);
        builder.setMessage(R.string.payment_fail);
        builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
        builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Payment.super.onBackPressed();
            }
        });
        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showCancelDailog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(Payment.this);
        builder.setMessage(R.string.payment_cancel);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
               Payment.super.onBackPressed();
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showSuccessDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(Payment.this);
        builder.setMessage(R.string.payment_success);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(Payment.this,HomeActivity.class);
                startActivity(intent);
                finishAffinity();
            }
        });

        Payment.this.runOnUiThread(new Runnable() {
            public void run() {
                final AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(Payment.this);
        builder.setMessage(R.string.cancel_payment);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Payment.super.onBackPressed();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
        final AlertDialog dialog = builder.create();
        dialog.show();



    }
}