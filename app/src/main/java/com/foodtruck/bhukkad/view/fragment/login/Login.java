package com.foodtruck.bhukkad.view.fragment.login;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.model.register.FirebaseKeys;
import com.foodtruck.bhukkad.model.register.RegisterResponse;
import com.foodtruck.bhukkad.network.DataService;
import com.foodtruck.bhukkad.utils.Const;
import com.foodtruck.bhukkad.utils.SessionManager;
import com.foodtruck.bhukkad.view.ActivityResponse;
import com.foodtruck.bhukkad.view.HomeActivity;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;


public class Login extends Fragment {

@BindView(R.id.et_userName)
    TextView etUserName;

@BindView(R.id.et_password)
TextView etPassword;
    private SessionManager sessionManager;
    public static final int RC_SIGN_IN = 100;
    GoogleSignInClient mGoogleSignInClient;
    ActivityResponse activityResponse;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_login2, container, false);
        ButterKnife.bind(this,view);
        activityResponse = (ActivityResponse)getActivity();
        sessionManager = new SessionManager(getActivity());

     /*  GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
               .requestIdToken("482806336535-mgq9dp13vvjlojgqfdog31m7shj3vqus.apps.googleusercontent.com")
                .requestEmail()
                .build();*/
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
         mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);


        return view;
    }

    @OnClick(R.id.btn_redeem)
    void clickSubmitButton(){
        if (checkValidations()){
            callApiToLogin();
        }
    }

    private void callApiToLogin() {
        DataService dataService =new DataService(getActivity());
        dataService.setOnServerListener(new DataService.ServerResponseListner<RegisterResponse>() {
            @Override
            public void onDataSuccess(RegisterResponse response, int code) {
                if (response!=null && response.getSuccess()){
                    sessionManager.saveBooleanValue(Const.IS_LOGIN, true);
                    sessionManager.saveUser(response);
                    Intent i = new Intent(getActivity(), HomeActivity.class);
                    startActivity(i);
                    getActivity().finishAffinity();
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.callApiToLogin(etUserName.getText().toString().trim(),etPassword.getText().toString().trim(),sessionManager.getTokenValue("Token"));

    }

    private boolean checkValidations() {
        boolean isvalid=true;
        if (etUserName.getText().toString().trim().isEmpty()){
            Toast.makeText(getActivity(), "Please enter UserName", Toast.LENGTH_SHORT).show();
            isvalid=false;
        }else if (etPassword.getText().toString().trim().isEmpty()){
            Toast.makeText(getActivity(), "Please enter Password", Toast.LENGTH_SHORT).show();
            isvalid=false;
        }
        return isvalid;
    }

    @OnClick(R.id.bt_google)
    void clickGoogleLogin(){
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                handleSignInResult(task);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void handleSignInResult(Task<GoogleSignInAccount> completedTask) throws Exception {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if (account != null) {
                HashMap<String, Object> hashMap = new HashMap<>();
          /*      hashMap.put("user_email", account.getEmail());
                hashMap.put("full_name", account.getDisplayName());
                hashMap.put("login_type", Const.GOOGLE_LOGIN);
                hashMap.put("platform", "android");
                hashMap.put("user_name", Objects.requireNonNull(account.getEmail()).split("@")[0]);
                hashMap.put("identity", account.getId());*/
                callApiToGoogleLogin(account);

            }

        } catch (ApiException e) {

        }
    }
    private void callApiToGoogleLogin(GoogleSignInAccount account) {
        DataService dataService =new DataService(getActivity());
        dataService.setOnServerListener(new DataService.ServerResponseListner<RegisterResponse>() {
            @Override
            public void onDataSuccess(RegisterResponse response, int code) {
                if (response!=null ){
                    if (response.getSuccess()) {
                        sessionManager.saveBooleanValue(Const.IS_LOGIN, true);
                        sessionManager.saveUser(response);
                        Intent i = new Intent(getActivity(), HomeActivity.class);
                        startActivity(i);
                        getActivity().finishAffinity();
                    }else {
                        Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                        FirebaseKeys firebaseKeys = new FirebaseKeys(account.getDisplayName(),account.getEmail(),account.getIdToken());
                        sessionManager.saveGoogleUser(firebaseKeys);
                        if (activityResponse!=null){
                            activityResponse.changeFragment();
                        }
                    }
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.callApiToGoogleLogin(account.getEmail(), account.getDisplayName(),account.getIdToken(),sessionManager.getTokenValue("Token"));
    }
}