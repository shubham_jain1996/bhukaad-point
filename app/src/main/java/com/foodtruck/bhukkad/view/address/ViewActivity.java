package com.foodtruck.bhukkad.view.address;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.adapter.ViewAddressAdapter;
import com.foodtruck.bhukkad.model.addres.AddressData;
import com.foodtruck.bhukkad.model.addres.AddressResponse;
import com.foodtruck.bhukkad.network.DataService;
import com.foodtruck.bhukkad.utils.SessionManager;
import com.foodtruck.bhukkad.view.HomeActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.foodtruck.bhukkad.MainActivity.latitude;
import static com.foodtruck.bhukkad.MainActivity.longitude;

public class ViewActivity extends AppCompatActivity {

    @BindView(R.id.tvAddressList)
    RecyclerView rv_AddressList;

    ViewAddressAdapter viewAddressAdapter;
    List<AddressData> data = new ArrayList<>();
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
        sessionManager = new SessionManager(this);
        ButterKnife.bind(this);
        setUpRecyclerView();
        CallApiToGetAddress();
    }

    private void setUpRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rv_AddressList.setLayoutManager(linearLayoutManager);
        viewAddressAdapter= new ViewAddressAdapter(this,data);
        rv_AddressList.setAdapter(viewAddressAdapter);

    }

    @OnClick(R.id.tvAddAddress)
    void clickAddAddress(){
        Intent i = new Intent(ViewActivity.this,AddAddres.class);
        startActivity(i);
    }
    private void CallApiToGetAddress() {
        DataService dataService = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<AddressResponse>() {
            @Override
            public void onDataSuccess(AddressResponse response, int code) {
                if (response!=null && response.getSuccess()){
                    if (response.getData()!=null){
                        if (response.getData().getAddress().size()>0){
                            data.clear();
                            data.addAll(response.getData().getAddress());
                          viewAddressAdapter.notifyDataSetChanged();
                        }
                    }
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.callApiToGetAddress("Token "+sessionManager.getUser().getData().getAuthToken(), latitude,longitude);

    }
}