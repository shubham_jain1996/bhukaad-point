package com.foodtruck.bhukkad.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.TextView;

import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.adapter.ViewDetailAdapter;
import com.foodtruck.bhukkad.model.order.ItemResopnse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Orderdetails extends AppCompatActivity {

    List<ItemResopnse>data = new ArrayList<>();
    @BindView(R.id.rv_list)
    RecyclerView rv_list;


    @BindView(R.id.tvTitle)
    TextView tvTitle;

    ViewDetailAdapter viewDetailAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderdetails);
        ButterKnife.bind(this);
        tvTitle.setText("Order Details");
        data=getIntent().getParcelableArrayListExtra("data");
        setupRecyclerView();
    }

    private void setupRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rv_list.setLayoutManager(linearLayoutManager);
        viewDetailAdapter = new ViewDetailAdapter(this,data);
        rv_list.setAdapter(viewDetailAdapter);


    }

    @OnClick(R.id.back_btn)
    void clickBackButton(){
        onBackPressed();
    }
}