package com.foodtruck.bhukkad.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.widget.TextView;

import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.adapter.ViewPagerAdapter;
import com.foodtruck.bhukkad.adapter.ViewPagerAdapterLOrder;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyOrder extends AppCompatActivity {
    @BindView(R.id.viewpagerFragment)
    ViewPager viewPagerFragment;


    @BindView(R.id.tabs)
    TabLayout tablayout;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    List<String> data = new ArrayList<>();
    private ViewPagerAdapterLOrder adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_order);
        ButterKnife.bind(this);
        tvTitle.setText("My Order");
        data.add("Running");
        data.add("History");
        setupViewPager();
    }
    private void setupViewPager() {
        tablayout.setupWithViewPager(viewPagerFragment);
        tablayout.setTabMode(TabLayout.MODE_FIXED);
        adapter = new ViewPagerAdapterLOrder(getSupportFragmentManager(),data);
        viewPagerFragment.setAdapter(adapter);
    }

    @OnClick(R.id.back_btn)
    void clickBackButton(){
        onBackPressed();
    }
}