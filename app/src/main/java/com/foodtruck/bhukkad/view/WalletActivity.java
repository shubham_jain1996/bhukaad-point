package com.foodtruck.bhukkad.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.TextView;

import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.adapter.ViewWalletAdapter;
import com.foodtruck.bhukkad.model.wallet.WalletResponse;
import com.foodtruck.bhukkad.network.DataService;
import com.foodtruck.bhukkad.utils.SessionManager;
import com.foodtruck.bhukkad.utils.Validations;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WalletActivity extends AppCompatActivity {

    @BindView(R.id.tv_count)
    TextView tvCount;

    @BindView(R.id.tv_count_self)
    TextView tvCountSelf;

    @BindView(R.id.rvWallet)
    RecyclerView rvWallet;

    @BindView(R.id.tvTitle)
            TextView tvTitle;

    List<String>userListData = new ArrayList<>();
    ViewWalletAdapter viewWalletAdapter;
    SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        ButterKnife.bind(this);
        tvTitle.setText("My Wallet");
        sessionManager = new SessionManager(this);
        setUpRecyclerView();
        callApiToGetWalletDetails();

    }

    private void callApiToGetWalletDetails() {
        DataService dataService = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<WalletResponse>() {
            @Override
            public void onDataSuccess(WalletResponse response, int code) {
                if (response!=null){
                    if (response.getData()!=null){
                        if (response.getData().getBalance()!=null){
                            setDataInView(response.getData().getBalance(),response.getData().getPendingReferrals());
                        }
                        if (response.getData().getPendingReferrals().size()>0){
                            userListData.addAll(response.getData().getPendingReferrals());
                            viewWalletAdapter.notifyDataSetChanged();
                        }
                    }
                }
            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.callApiToGetWalletDetails("Token "+sessionManager.getUser().getData().getAuthToken());

    }

    private void setDataInView(Double balance, List<String> pendingReferrals) {
        tvCount.setText(Validations.prettyCount(balance));
        tvCountSelf.setText(Validations.prettyCount((pendingReferrals.size()*5)));
    }

    private void setUpRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvWallet.setLayoutManager(linearLayoutManager);
        viewWalletAdapter = new ViewWalletAdapter(this,userListData);
        rvWallet.setAdapter(viewWalletAdapter);


    }
    @OnClick(R.id.back_btn)
    void clickBAckButton(){
        onBackPressed();
    }
}