package com.foodtruck.bhukkad.view.fragment.login;

import android.app.Activity;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.adapter.ViewPagerAdapter;
import com.foodtruck.bhukkad.adapter.ViewPagerAdapterLogin;
import com.foodtruck.bhukkad.view.ActivityResponse;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends BottomSheetDialogFragment  {


    @BindView(R.id.tabs)
    TabLayout tablayout;

    @BindView(R.id.viewpagerFragment)
    ViewPager viewPagerFragment;

    List<String> categoryName = new ArrayList<>();
    private ViewPagerAdapterLogin adapter;


    public static LoginFragment newInstance(String param1, String param2) {
        LoginFragment fragment = new LoginFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       View view= inflater.inflate(R.layout.fragment_login, container, true);
        ButterKnife.bind(this,view);
        categoryName.add("Login");
        categoryName.add("Register");
        setupViewPager();
       return view;
    }
    private void setupViewPager() {
        tablayout.setupWithViewPager(viewPagerFragment);
        tablayout.setTabMode(TabLayout.MODE_FIXED);
        adapter = new ViewPagerAdapterLogin(getActivity().getSupportFragmentManager(),categoryName);
        viewPagerFragment.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }


}