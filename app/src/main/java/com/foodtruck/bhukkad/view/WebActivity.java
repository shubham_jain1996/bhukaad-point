package com.foodtruck.bhukkad.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.foodtruck.bhukkad.R;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WebActivity extends AppCompatActivity {

    @BindView(R.id.mainWebView)
    WebView webviewMain;
    @BindView(R.id.tvTitle)
    TextView tvTitle;

    String url;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        ButterKnife.bind(this);
        url="http:/3.17.125.65/"+getIntent().getStringExtra("url");
        tvTitle.setText(getIntent().getStringExtra("title"));
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        setUpWebView();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setUpWebView() {
        webviewMain.getSettings().setLoadsImagesAutomatically(true);
        webviewMain.getSettings().setLoadWithOverviewMode(true);
        webviewMain.getSettings().setSupportZoom(true);
        webviewMain.getSettings().setBuiltInZoomControls(true);
        webviewMain.getSettings().setDisplayZoomControls(false);
        webviewMain.getSettings().setJavaScriptEnabled(true);
        webviewMain.getSettings().setPluginState(WebSettings.PluginState.ON);
        webviewMain.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webviewMain.getSettings().setDomStorageEnabled(true);
        // webviewMain.setWebChromeClient(new UriChromeClient());
        //webviewMain.setWebViewClient(new UriWebViewClient());
        webviewMain.setWebViewClient(new WebViewClient());
        webviewMain.getSettings().setUserAgentString("Mozilla/5.0 (Linux; Android 4.1.1; Galaxy Nexus Build/JRO03C) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19");


            webviewMain.loadUrl(url);

        webviewMain.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));


                }catch (Exception e){
                    Toast.makeText(WebActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });




        if (Build.VERSION.SDK_INT >= 21) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(webviewMain, true);
        } else {
            CookieManager.getInstance().setAcceptCookie(true);
        }



        webviewMain.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                webviewMain.loadUrl(url);
                Log.d("Sahil", "shouldOverrideUrlLoading: "+url);

                return true;
                //  return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
            }

            @Nullable
            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {


                return super.shouldInterceptRequest(view, url);

            }

            @Override
            public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {

                super.doUpdateVisitedHistory(view, url, isReload);
            }

            @Override
            public void onPageCommitVisible(WebView view, String url) {
                super.onPageCommitVisible(view, url);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                progressDialog.show();
            }

            public void onPageFinished(WebView view, String url) {
                progressDialog.dismiss();



            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                progressDialog.dismiss();
            }

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                progressDialog.dismiss();
                final AlertDialog.Builder builder = new AlertDialog.Builder(WebActivity.this);
                builder.setMessage(R.string.notification_error_ssl_cert_invalid);
                builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        handler.proceed();
                    }
                });
                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        handler.cancel();
                    }
                });
                final AlertDialog dialog = builder.create();
                dialog.show();
            }
        });




        if (Build.VERSION.SDK_INT >= 21) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(webviewMain, true);
        } else {
            CookieManager.getInstance().setAcceptCookie(true);
        }




    }

    @OnClick(R.id.back_btn)
    void clickBackButton(){
        onBackPressed();
    }
}