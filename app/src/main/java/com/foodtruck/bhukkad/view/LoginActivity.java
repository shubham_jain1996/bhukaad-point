package com.foodtruck.bhukkad.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.adapter.ViewPagerAdapterLogin;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements ActivityResponse{
    @BindView(R.id.tabs)
    TabLayout tablayout;

    @BindView(R.id.viewpagerFragment)
    ViewPager viewPagerFragment;

    List<String> categoryName = new ArrayList<>();
    private ViewPagerAdapterLogin adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        categoryName.add("Login");
        categoryName.add("Register");
        setupViewPager();
    }
    private void setupViewPager() {
        tablayout.setupWithViewPager(viewPagerFragment);
        tablayout.setTabMode(TabLayout.MODE_FIXED);
        adapter = new ViewPagerAdapterLogin(getSupportFragmentManager(),categoryName);
        viewPagerFragment.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
    @Override
    public void changeFragment() {
        viewPagerFragment.setCurrentItem(1,true);
    }
}