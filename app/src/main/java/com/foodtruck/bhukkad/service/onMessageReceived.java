package com.foodtruck.bhukkad.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.foodtruck.bhukkad.R;
import com.foodtruck.bhukkad.view.Orderdetails;
import com.foodtruck.bhukkad.view.TrackOrder;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class onMessageReceived extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage.getData().size() > 0) {
            Map<String,String> data = remoteMessage.getData();
            Log.e("data", data.toString());
            String mTitle = data.get("title");
            String mMessage = data.get("message");
            String orderId = data.get("order_id");
            Intent intent;
            intent = new Intent(this, TrackOrder.class);
            intent.putExtra("data",orderId);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            pushNotification(intent,mTitle,mMessage);
        }

    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
       // setUpModel(s);
       // updateTokenOnServer();
    }

  /*  private void setUpModel(String s) {
    firebaseTokendata = new FirebaseTokendata(Application.shardPref.getString(Constants.Pref.userid,""),
            "passenger",s);
    firebaseSeverModel = new FirebaseSeverModel("1.1.1.1",Application.shardPref.getString(Constants.Pref.token,""),
            Application.shardPref.getString(Constants.Pref.email,""),firebaseTokendata);


    }

    private void updateTokenOnServer() {
        DataService dataService = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<FirebaseTokenResponse>() {
            @Override
            public void onDataSuccess(FirebaseTokenResponse response) {

            }

            @Override
            public void onDataFailiure() {

            }
        });
    dataService.saveFirebaseTokenOnServer(firebaseSeverModel);

    }*/
  private void pushNotification(Intent intent, String title, String message) {

      Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

      NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
      PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

      NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "alert_001")

              .setWhen(System.currentTimeMillis())
              .setContentTitle(title)
              .setContentText(message)
              .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
              .setContentIntent(pendingIntent)
              .setOngoing(false)
              .setSound(defaultSoundUri)
              .setPriority(NotificationCompat.PRIORITY_MAX)
              .setDefaults(Notification.DEFAULT_ALL)
              .setAutoCancel(false);

      if(Build.MANUFACTURER.equalsIgnoreCase("xiaomi")) {
          mBuilder.setSmallIcon(R.drawable.logo);
          mBuilder.setColorized(true);
          mBuilder.setColor(Color.parseColor("#f38000"));
          Log.e("MANUFACTURER", Build.MANUFACTURER);
      } else {
          mBuilder.setSmallIcon(R.drawable.logo);
      }

      if (mNotificationManager != null) {
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

              AudioAttributes attributes = new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).build();

              NotificationChannel channel = new NotificationChannel("alert_001",
                      "Alert",
                      NotificationManager.IMPORTANCE_HIGH);


              channel.setSound(defaultSoundUri, attributes);

              mNotificationManager.createNotificationChannel(channel);
              mBuilder.setChannelId(channel.getId());
          }
          mNotificationManager.notify(1, mBuilder.build());
      }
  }
}
